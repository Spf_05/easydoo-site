#!/bin/bash
npm --prefix themes/easydoo run prod
rsync -rzP --exclude=".DS_Store" --files-from="deploy_whitelist.txt" . getupp:/var/www/html/easydoo-site/