const { sassConfig } = require('./build/sass-options.js');
const mix = require('laravel-mix');
const glob = require('glob');
const nodeFs = require('fs');
const nodePath = require('path');
const config = require('./build/config')
const webpack = require('webpack');

require('laravel-mix-clean');
require('./build/copy/index.js');

// https://laravel.com/docs/8.x/mix#tailwindcss

mix.alias({
	NodeModules         : nodePath.resolve( __dirname, 'node_modules' ),
	AjaxFrameworkAssets : nodePath.resolve( __dirname, '..', '..', 'modules', 'system', 'assets' ),
	Config              : nodePath.resolve( __dirname, 'config_build' ),
	Utils               : nodePath.resolve( __dirname, 'src', 'utils' ),
	Components          : nodePath.resolve( __dirname, 'src', 'components' ),
});

// mix.alias({
//     '@': path.join(__dirname, 'resources/js')
// });

// mix
// 	.setPublicPath( publicFolder )
// 	.clean({
// 		cleanOnceBeforeBuildPatterns: [ `${ publicFolder }/*` ],
// 	})
// 	.options({
// 		processCssUrls: false
// 	})

// 	copyDirectories.forEach( item => mix.copyDirectory( item.from, item.to ) );
// 	jsArr.forEach( item => mix.js( item, 'js') );
// 	cssArr.forEach( item => mix.postCss( item, 'css') );
// 	extractArr.forEach( libName => {
// 		return mix.extract([ libName ], `${ vendorsFolder }/${ libName }/${ libName }.js`)
// 	});

// 	if ( mix.inProduction() ) {
// 		mix.version();
// 	} else {
// 		mix.browserSync({
// 			proxy	: process.env.APP_URL,
// 			host	: process.env.APP_URL,
// 			port	: process.env.BROWSER_SYNC_PORT || 8080,
// 			// proxy	: 'shopotam-catalog.local',
// 			// host	: 'home.max74.info',
// 			// port	: '8085',
// 			open	: 'external',
// 			browser	: 'google chrome',
// 			notify	: {
// 				styles: {
// 					top: '0',
// 				}
// 			},
// 			files	: [
// 				`./${ publicFolder }/**/*`,
// 				"./**/*.htm",
// 			]
// 		});
// 	}

mix.webpackConfig({
	plugins: [
		new webpack.DefinePlugin({
			__CONSTANTS__ : config,
		}),
	],
});

mix
    .clean({
        cleanOnceBeforeBuildPatterns: [ 'assets/*' ],
    })
    .setPublicPath('assets')
    .js('src/components/app.js', 'js')
    .js('src/components/quize.js', 'js')
    .sass('src/components/app.scss', 'css', sassConfig )
    .sass('src/components/quize.scss', 'css', sassConfig )
    .copyDirectory( 'src/assets', 'assets' )
    .CopyComponentsAssets('./src/components/**/*.(png|jpg|svg)', './assets/img/components')
    .vue({ version: 2 });

    if (mix.inProduction()) {
        mix.version();
    }

    mix.browserSync({
        proxy	: process.env.APP_URL,
        host	: process.env.APP_URL,
        browser	: 'google chrome',
        notify	: false,
        files	: [
            "./assets/css/*.css",
            "./**/*.htm",
            "./assets/js/*.js"
        ]
    });