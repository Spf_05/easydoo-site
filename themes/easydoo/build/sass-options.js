const sass      = require("node-sass");
const sassUtils = require('node-sass-utils')(sass);
const { screens, paths } = require('./config.js');
const moreSassUtils = require("node-sass-more-utils")(sass, sassUtils);

function getVars( varsObj ) {
    return Object.entries( varsObj ).reduce( (acc, [ varName, varVal ]) => acc += `$${ varName }: "${ varVal}";`, '' );
}

const vars = {
    ...paths
}

function getManifestFile( fileName ) {
    
}

function componentAssetPath( componentName, fileName ) {
    // const manifest = require('../assets/mix-manifest.json' );
    componentName = moreSassUtils.toJS( componentName );
    fileName = moreSassUtils.toJS( fileName );

    const path = `img/components/${ componentName }/${ fileName }`;
    return moreSassUtils.toSass( `/themes/easydoo/assets/${ path }` );

    // if ( manifest[ path ] ) {
    //     return moreSassUtils.toSass( `/themes/easydoo/assets/${ manifest[ path ] }` );
    // } else {
    //     return moreSassUtils.toSass( `/themes/easydoo/assets/${ path }` );
    // }
}

const sassConfig = {
    functions: {
        "getScreens()": () => sassUtils.castToSass( screens ),
        "mix( $fileName )": getManifestFile,
        "componentAssetPath( $componentName, $fileName )": componentAssetPath,
    },
    data: getVars( vars ),
}

module.exports	= {
    sassConfig
}