
const publicFolder	= 'assets';
const vendorsFolder	= 'vendors';

const extractArr = [
	'swiper',
	'alpinejs'
];

const copyDirectories = [
	{
		from	: 'src/assets',
		to		: 'assets',
	}, {
		from	: 'src/assets/img',
		to		: 'assets/img',
	},
];

const jsArr = glob
			.sync( './src/js/*.js' )
			.filter( file => nodePath.basename( file ).substr(0, 1) !== '_' );

const cssArr = glob
			.sync( './src/css/*.css' )
			.filter( file => nodePath.basename( file ).substr(0, 1) !== '_' );