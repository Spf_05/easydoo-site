const screens = {
    phoneSmall: 360,
    phoneLarge: 480,
    pad: 768,
    desktopSmall: 1000,
    desktopLarge: 1218,

    logosCarouselSwitch: 768,
};

const paths = {
    assetsPath: '/themes/easydoo/assets/',
    componentsImgPath: '/themes/easydoo/assets/img/components',
}

module.exports = {
    screens,
    paths
}