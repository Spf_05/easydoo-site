import myFetch from 'Components/price/fetch.js';

const submitFormHandle = e => {
    e.preventDefault();

    const $form = e.target;
    const redirectUrl = $form.dataset.redirect || '';
    const submitButtons = $form.querySelectorAll('[type="submit"]');
    submitButtons.forEach( $btn => $btn.disabled = true );

    $form.querySelectorAll( '[data-validation-error]' ).forEach( $el => $el.innerText = '' );

    const submitSuccess = data => {
        if( redirectUrl == '' ) return;
        location.href = redirectUrl;
    }

    const submitError = err => {
        if( err.name == 'ValidationError' ) {
            Object.entries( err.message ).forEach( ([ name, errors ]) => {
                const $el = $form.querySelector( `[data-validation-error="${ name }"]` );
                if( $el === null )
                    return;
                $el.innerText = errors[0];
            })
        } else {
            console.error( err.text );
        }
    }

    const submitFinally = () => {
        submitButtons.forEach( $btn => $btn.disabled = false );
    }

    myFetch( $form )
        .then( submitSuccess )
        .catch( submitError)
        .finally( submitFinally );
}

document.querySelectorAll('.js-price-simple').forEach( $form => {
    const $priceCode = $form.querySelector('.js-price-code');
    $form.querySelectorAll( '.js-option' ).forEach( $option => {
        $option.onchange = ({ target }) => $priceCode.value = target.dataset[ 'priceCode' ];
    });

    $form.onsubmit = submitFormHandle
});