function leftFillNum(num, targetLength) {
    return num.toString().padStart(targetLength, '0');
}

function initBlock( $root ) {
    const $hours = $root.querySelector('.js-hours');
    const $minutes = $root.querySelector('.js-minutes');
    const $seconds = $root.querySelector('.js-seconds');

    var date = new Date();
    var second = date.getSeconds();
    var minute = date.getMinutes();
    var hour = date.getHours();

    var leftHour = 23 - hour;
    var leftMinute = 59 - minute;
    var leftSeconds = 59 - second;

    var leftTime = (leftHour * 3600) + (leftMinute * 60) + leftSeconds;

    setInterval(updateTimer, 1000);

    function updateTimer() {
        var h = Math.floor(leftTime / 3600);
        var m = Math.floor((leftTime - (h * 3600)) / 60);
        var s = Math.floor(leftTime % 60);

        $hours.innerText = leftFillNum(h, 2);
        $minutes.innerText = leftFillNum(m,2);
        $seconds.innerText = leftFillNum(s+1, 2);

        leftTime--;
    }
}

document.querySelectorAll( '.js-block-countdown' ).forEach( initBlock );
// document.querySelectorAll( '.js-block-countdown' ).forEach( function($root ){
//     console.log('root1', $root);
//     initBlock( $root );
// } );