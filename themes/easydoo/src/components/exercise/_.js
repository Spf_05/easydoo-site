document.querySelectorAll( '.js-exercise-tabs' ).forEach( $root => {
    $root.querySelectorAll( '.js-tab' ).forEach( $tab => {
        $tab.onclick = e => {
            $panel = $root.querySelector( `#${ $tab.dataset.control }` );

            if(!$panel)
                return;

            $root.querySelectorAll( '.js-panel' ).forEach( $panel => $panel.classList.remove( 'exercise__panel_active' ) );
            $root.querySelectorAll( '.js-tab' ).forEach( $panel => $panel.classList.remove( 'exercise__tab_active' ) );

            $tab.classList.add('exercise__tab_active');
            $panel.classList.add('exercise__panel_active');
        }
    });
})