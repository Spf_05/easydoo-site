import Alpine from 'alpinejs'

window.Alpine = Alpine;


const addSerializedItems = (arrSerialize, name, value) => arrSerialize.push(`${(name)}=${(value)}`);

const formSerialize = form => {
    const arr = [];
    const { elements } = form;

    for (let i = 0; i < elements.length; i += 1) {
      const input = elements[i];
      const {
        name, type, checked, value,
      } = input;

      const arrUnSerializedElmsType = ['file', 'reset', 'submit', 'button'];

      if (name && type.indexOf(arrUnSerializedElmsType) === -1) {
        if (type === 'select-multiple') {
          const selected = [...input.options].filter(k => k.selected);

          selected.forEach(j => this.addSerializedItems(arr, name, selected[j]));
        } else if ((type !== 'checkbox' && type !== 'radio') || checked) {
            addSerializedItems(arr, name, value);
        }
      }
    }

    return arr.join('&');
}

const quize = data => {
    return {
        currentSlide : 0,
        slides: [],
        nextDisabled: false,
        nextBtnText: 'Начать',
        quizeId: null,
        finish: false,
        storePath: '/storage/app/media',
        phoneVerified : false,
        phoneVerifiedCode : '',
        phoneVerifiedCodeError : '',

        async init() {
            const res = await fetch(`/api/quize/${ data.id }`);
            const resData = await res.json();

            let slides = resData.slides;

            this.quizeId = resData.id;

            slides.forEach( slide => {
                if( [ 'form' ].includes( slide._group ) ) {
                    slide.fields.forEach( field => {
                        field.required = field.required == '0' ? false : true;
                        field.value = '';
                    })
                }

                if( [ 'phone' ].includes( slide._group ) ) {
                    slide.checkPhone = false;
                    slide.code = '';
                    slide.verified = false;

                    slide.fields = [
                        {
                            error: '',
                            name : 'phone',
                            label: 'Телефон',
                            required : true,
                            value : '',
                        }
                    ];
                }

                if( [ 'checkbox', 'radio' ].includes( slide._group ) ) {
                    slide.values = [];
                    slide.fields.forEach( field => field.checked = false )
                }
            });

            this.slides = slides;

            this.$watch('slides', (value, oldValue) => this.updateNextDisabled() );
            this.$watch('currentSlide', (value, oldValue) => this.updateNextDisabled() );
        },
        updateNextDisabled() {

            if( this.currentSlide == 0 ) {
                this.nextBtnText = 'Начать'
            }

            if( this.currentSlide > 0 ) {
                this.nextBtnText = 'Далее'
            }

            if( this.currentSlide == this.slides.length - 1 ) {
                this.nextBtnText = 'Закончить'
            }

            const slide = this.slides[ this.currentSlide ];

            if( slide._group == 'text' ) {
                return this.nextDisabled = false;
            }

            if( slide._group == 'phone' ) {
                return this.nextDisabled = !this.phoneVerified;
            }

            if( slide._group == 'video' ) {
                return this.nextDisabled = false;
            }

            if( [ 'phone' ].includes( slide._group ) ) {
                let disabled = false;

                slide.fields.forEach( field => {
                    if( field.required == true && field.value == '' ) {
                        disabled = true;
                        return;
                    }
                })

                return this.nextDisabled = disabled;
            }

            if( [ 'form' ].includes( slide._group ) ) {
                let disabled = false;

                slide.fields.forEach( field => {
                    if( field.required == true && field.value == '' ) {
                        disabled = true;
                        return;
                    }
                })

                return this.nextDisabled = disabled;
            }

            if( [ 'checkbox', 'radio' ].includes( slide._group ) ) {
                this.nextDisabled = slide.values.length ? false : true;
            }
        },
        playVideo(e){
            const btn = e.currentTarget;
            btn.previousElementSibling.play();
            btn.remove();
        },
        nextSlide() {
            this.currentSlide++;
        },
        prevSlide() {
            this.currentSlide--;
        },
        requestVerifyPhone( slide ) {
            slide.fields[0].error = '';

            const val = slide.fields[0].value;
            const phoneRe = /^\+?7(9\d{9})$/;

            if( !phoneRe.test( val ) ) {
                slide.fields[0].error = 'Телефон должен начинаться с +7 и быть без пробелов +79999999999';
                return;
            }

            slide.checkPhone = true;
        },
        verifyPhone() {
            this.phoneVerifiedCodeError = '';

            const phoneRe = /d{4}$/;

            if( phoneRe.test( this.phoneVerifiedCode ) ){
                this.phoneVerifiedCodeError = 'Код должен быть 4 цифры';
                return;
            }

            if( this.phoneVerifiedCode != '1234' ) {
                this.phoneVerifiedCodeError = 'Код не верный';
                return;
            }

            this.phoneVerified = true;
            this.nextDisabled = false;
        },
        inputRadioLabel: (( slide,field ) => {
            return {
                'for': `${ slide._id }-${ slide.name }-${ field.value }`,
            }
        }),
        inputRadioAttr: (( slide,field ) => {
            // x-model="field.value"
            return {
                'type': `${ slide._group }`,
                'value': `${ field.value }`,
                'id': `${ slide._id }-${ slide.name }-${ field.value }`,
                'name': `${ slide.name }[]`,
                // ['x-model']: field.value,
            }
        }),
        btnNextSlide: function() {
            return {
                async ['@click']() {
                    if( this.currentSlide != this.slides.length - 1 ) {
                        this.currentSlide++;
                        const $root = this.$root;

                        this.$nextTick(() => {
                            $root.scrollIntoView({ behavior: 'smooth' });
                        });
                        return;
                    }

                    let body = {
                        id : this.quizeId,
                        data : {}
                    };

                    this.slides.forEach( slide => {

                        if( ![ 'phone', 'form', 'checkbox', 'radio' ].includes( slide._group ) ) {
                            return;
                        }

                        const slideId = slide._id;

                        body.data[ slideId ] = {
                            'type' : slide._group,
                            'fields' : {},
                        }

                        if( [ 'phone', 'form' ].includes( slide._group ) ) {
                            slide.fields.forEach( field => {
                                body.data[ slideId ].fields[ field.name ] = {
                                    label: field.label,
                                    value: field.value,
                                }
                            })
                        }

                        if( [ 'checkbox', 'radio' ].includes( slide._group ) ) {
                            let values = slide.fields
                                            .map( field => {
                                                if( slide.values.includes( field.value ) ) {
                                                    return field.label
                                                }
                                            })
                                            .filter( item => item !== undefined )

                                            body.data[ slideId ].fields[ slide.name ] = {
                                ids: slide.values,
                                values: values,
                            };
                        }
                    });

                    const options = {
                        'method' : 'POST',
                        'body'   : JSON.stringify( body ),
                        'headers' : {
                            'Content-Type' : 'application/json',
                            'x-october-request-handler' : 'onQuize',
                            'x-requested-with' : 'XMLHttpRequest',
                        }
                    }

                    try {
                        const rawResponse = await fetch( window.location.href, options );

                        if ( rawResponse.status !== 200 || rawResponse.ok === false ) {
                            throw rawResponse;
                        }

                        const respData	= await rawResponse.json();
                        console.log( respData );
                        this.finish = true;
                    } catch (err) {
                        console.log( err );
                    }
                }
            }
        }
    }
}

Alpine.data('quize', quize )
    
Alpine.start()

// (()=>{
//     const $app = document.querySelector( '.js-quiz' );

//     if( !$app )
//         return;

//     const $slides = $app.querySelectorAll( '[data-slide]' );
//     let currentSlide = 1;

//     const playVideo = e => {
//         const $btn = e.currentTarget;
//         const $video = $btn.previousElementSibling;
//         $btn.addEventListener( 'click', () => {
//             $video.play();
//             $btn.remove();
//         });
//     }

//     $app.querySelectorAll( '.js-video-play' ).forEach( $btn => $btn.addEventListener( 'click', playVideo ) );

//     const nextSlide = () => {
//         currentSlide++;
//         $slides.forEach( $slide => $slide.style.display = 'none' );
//         $nextSlide = $app.querySelector( `[data-slide="${ currentSlide }"]` );
//         $nextSlide.style.display = 'flex';

//         $app.querySelectorAll( 'video' ).forEach( $el => $el.pause() );
//     }

//     $app.querySelectorAll( '.js-next' ).forEach( $btn => $btn.addEventListener( 'click', nextSlide ) );
// })()