document.querySelectorAll( '.js-tabs3' ).forEach( $root => {
    $root.querySelectorAll( '.js-tab' ).forEach( $tab => {

        $tab.onclick = e => {
           const $panel = $root.querySelector( `#${ $tab.dataset.control }` );

            if(!$panel)
                return;;

            $root.querySelectorAll( '.js-panel' ).forEach( $panel => $panel.classList.remove( 'sign-up-now__tabs-content_show' ));
            $root.querySelectorAll( '.js-tab' ).forEach( $tab => $tab.parentElement.classList.remove( 'sign-up-now__tabs-nav_active' ) );

            $tab.parentElement.classList.add('sign-up-now__tabs-nav_active');
            $panel.classList.add('sign-up-now__tabs-content_show');
        }
    });
});