import domFocus from 'dom-focus-lock';
import { disableScroll, enableScroll } from 'Utils/scroll-lock';

(() => {
    const $menu = document.querySelector( '.js-main-menu' );
    const $btnOpen = document.querySelector( '.js-btn-open-main-menu' );

    if( !$menu || !$btnOpen )
        return;

    function escHandler( e ) {
        if( e.key == 'Escape' )
            closeMenu();
    }

    function openMenu() {
        $menu.classList.add( 'header-menu_open' );
        domFocus.on( $menu );
        $menu.addEventListener( 'keyup', escHandler, true );
        $btnOpen.setAttribute( 'aria-expanded', true );
        disableScroll();
    }

    function closeMenu() {
        $menu.classList.remove( 'header-menu_open' );
        domFocus.off( $menu );
        $menu.removeEventListener( 'keyup', escHandler, true );
        $btnOpen.setAttribute( 'aria-expanded', false );
        enableScroll();
    }

    document.querySelectorAll( '.js-btn-main-menu-toggle' ).forEach( $btn => {
        $btn.onclick = e => {
            if( $menu.classList.contains( 'header-menu_open' ) ) {
                closeMenu();
            } else {
                openMenu();
            }
        }
    })
})();