
import Swiper, { Pagination } from 'swiper';

(() => {
    const initBlock = $root => {
        const $carousel = $root.querySelector( '.js-carousel' );
        let swiper		= null;

        const renderCustom = (swiper, current, total) => {
            let templ = '';

            if( total == 1 )
                return '';

                for (let i = 1; i < total + 1; i++) {
                const crntClass = i == current ? ' swiper-pagination-bullet-active' : '';
                templ += `<div class="swiper-pagination-bullet${ crntClass }"></div>`;
            }

            return templ;
        }

        const pagination = {
            el				: '.js-swiper-pagination',
            type			: 'custom',
            clickable		: true,
            renderCustom	: renderCustom,
        }


        const breakpoints = {
            768: {
                slidesPerView: 2,
            },
            1218: {
                slidesPerView: 3,
            }
        }

        const options = {
            modules         : [ Pagination ],
            slidesPerView   : 'auto',
            spaceBetween    : 30,
            slideClass      : 'js-slide',
            wrapperClass    : 'js-wrapper',
            loop            : false,
            autoHeight      : true,
            breakpoints     : breakpoints,
        // pagination		: pagination
        }


        if ( swiper !== null || $carousel === null )
            return;

        swiper = new Swiper( $carousel, options );
        $root.classList.add( 'blog-anons-list_carousel-inited' );
    }

    document.querySelectorAll( '.js-blog-anons' ).forEach( initBlock );
})();