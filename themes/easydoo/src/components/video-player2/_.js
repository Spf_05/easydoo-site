document.querySelectorAll('[data-selector="video-player2"]').forEach( $root => {
    const $player = $root.querySelector('[data-selector="player"]');
    const $volume = $root.querySelector('[data-selector="volume"]');

    $volume.onclick = () => {
        $player.muted = false;
        $player.volume = 1;
        $player.currentTime = 0;
        $volume.remove();
    }
});