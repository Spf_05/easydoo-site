import Swiper, { Navigation } from 'swiper';

const initCarousel = $root => {
    const $carousel = $root.querySelector( '.js-carousel' );

    if ( $carousel === null )
        return;

    let $btnPrev = null;
    let $btnNext = null;

    let swiper = null;

    const options = {
        modules         : [],
        slidesPerView   : 'auto',
        spaceBetween    : 20,
        slideClass      : 'js-slide',
        wrapperClass    : 'js-wrapper',
        loop            : false,
    }

    function init() {
        if( swiper !== null ) {
            destroy();
        }

        swiper = new Swiper( $carousel, options );
        $root.classList.add( 'logos_inited' );
    }

    function destroy() {
        swiper.destroy();
        swiper = null;
        $root.classList.remove( 'logos_inited' );

        // if( $pagination !== null ) {
        //     $pagination.innerHtml = '';
        //     $pagination.style.setProperty( 'display', 'none' );
        // }
    }

    function xinit( windowWidth ) {
        if( swiper === null && windowWidth < __CONSTANTS__.screens.logosCarouselSwitch ) {
            init();
        }

        if( swiper !== null && windowWidth > __CONSTANTS__.screens.logosCarouselSwitch ) {
            destroy();
        }
    }

    window.addEventListener( 'optimizedResize', ({ detail : windowWidth }) => {
        xinit( windowWidth );
    });

    xinit( window.innerWidth );
}

document.querySelectorAll( '.js-logos' ).forEach( initCarousel );