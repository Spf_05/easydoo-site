document.querySelectorAll( '.js-bt2-time-left' ).forEach( $el => {
    let minutes = $el.dataset.minutes;
    let timerLeft = minutes * 60;
    let timerId = null;

    function showSeconds() {
        $el.innerText = new Date(timerLeft * 1000).toISOString().substr(11, 8);
    }

    timerId = setInterval( () => {
        timerLeft--;
        showSeconds();

        if( timerLeft == 0 ) {
            clearInterval( timerId );
        }
    }, 1000);
})