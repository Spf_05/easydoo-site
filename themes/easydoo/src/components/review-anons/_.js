
import Swiper, { Pagination } from 'swiper';

(() => {
    const initWindowWidth = windowWidth => windowWidth < 1000 ? true : false;

    const initBlock = $root => {
        const $carousel = $root.querySelector( '.js-carousel' );
        let swiper		= null;

        const renderCustom = (swiper, current, total) => {
            let templ = '';

            if( total == 1 )
                return '';

                for (let i = 1; i < total + 1; i++) {
                const crntClass = i == current ? ' swiper-pagination-bullet-active' : '';
                templ += `<div class="swiper-pagination-bullet${ crntClass }"></div>`;
            }

            return templ;
        }

        const pagination = {
            el				: '.js-swiper-pagination',
            type			: 'custom',
            clickable		: true,
            renderCustom	: renderCustom,
        }

        const options = {
            modules         : [ Pagination ],
            slidesPerView   : 'auto',
            spaceBetween    : 10,
            slideClass      : 'js-slide',
            wrapperClass    : 'js-wrapper',
            loop            : false,
            autoHeight      : true,
            // pagination		: pagination
        }

        const initCarousel = () => {
            if ( swiper !== null || $carousel === null )
                return;

            swiper = new Swiper( $carousel, options );
            $root.classList.add( 'review-anons_carousel-inited' );
        }

        const destroyCarousel = () => {
            if ( swiper === null )
                return;

            swiper.destroy();
            swiper = null;
            $root.classList.remove( 'review-anons_carousel-inited' );
        }

        window.addEventListener( 'optimizedResize', e => {
            initWindowWidth( e.detail )
                ? initCarousel()
                : destroyCarousel();
        });

        if( initWindowWidth( window.innerWidth ) )
            initCarousel();
    }

    document.querySelectorAll( '.js-carousel-review' ).forEach( initBlock );
})();