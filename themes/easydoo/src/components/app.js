import { updateVh, throttle } from 'Utils/helpers.js';

function init() {
    window.addEventListener( 'resize', updateVh );
    updateVh();

    throttle('resize', 'optimizedResize');
    throttle('scroll', 'optimizedScroll');

    const $header = document.querySelector('.js-header');

    document.querySelectorAll( '[data-scroll]' ).forEach( $el => {
        $el.onclick = () => {
            const $target = document.querySelector( $el.dataset.scroll );
            if ( !$target ) return;
            const headerHeight = $header ? $header.clientHeight : 0;
            const scrollGap = 20;
            const scrollTop = $target.offsetTop - ( headerHeight + scrollGap );
            window.scrollTo({ top: scrollTop, behavior: 'smooth' });
        };
    });
}

init();

require( './header-menu/_.js' );
require( './faq/_.js' );
require( './exercise/_.js' );
require( './review-anons/_.js' );
require( './blog-anons-list/_.js' );
require( './form-search/_.js' );
require( './form-filter/_.js' );
require( './custom-select/_.js' );
require( './price/_.js' );
require( './sign-up-now/_.js' );
require( './authors-list/_.js' );
require( './price-simple/_.js' );
require( './br2/_.js' );
require( './request-video/_.js' );
require( './logos/_.js' );
require( './quiz/_.js' );
require( './countdown/_.js' );
require( './video-player2/_.js' );
// require( './blog-list-layout/_.js' );