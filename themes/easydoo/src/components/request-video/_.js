import myFetch from 'Components/price/fetch.js';

let messanger = null;
let url = null;

const submitFormHandle = e => {
    e.preventDefault();

    const $form = e.target;
    const submitButtons = $form.querySelectorAll('[type="submit"]');
    submitButtons.forEach( $btn => $btn.disabled = true );

    $form.querySelectorAll( '[data-validation-error]' ).forEach( $el => $el.innerText = '' );
    const redirectUrl = $form.dataset.redirect || '';

    const submitSuccess = data => {
        if( data.redirect !== undefined ) {
            const $link = document.createElement('a');
            $link.href = data.redirect;
            $link.style.left = '-9000px';
            document.body.appendChild( $link );
            $link.click();
            $link.remove();
        }

        if( redirectUrl != '' ) {
            location.href = redirectUrl;
        }
    }

    const submitError = err => {
        if( err.name == 'ValidationError' ) {
            Object.entries( err.message ).forEach( ([ name, errors ]) => {
                const $el = $form.querySelector( `[data-validation-error="${ name }"]` );
                if( $el === null )
                    return;
                $el.innerText = errors[0];
            })
        } else {
            console.error( err.text );
        }
    }

    const submitFinally = () => {
        submitButtons.forEach( $btn => $btn.disabled = false );
    }

    myFetch( $form, [ { name : 'data[messanger]', value : messanger } ] )
        .then( submitSuccess )
        .catch( submitError)
        .finally( submitFinally );
}

document.querySelectorAll('.js-request-video').forEach( $form => {
    $form.querySelectorAll('[type="submit"]').forEach( $btn => {
        $btn.onclick = () => messanger = $btn.dataset.messanger;
    });
});

document.querySelectorAll('.js-request-video').forEach( $form => $form.onsubmit = submitFormHandle );