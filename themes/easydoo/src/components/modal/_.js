import focusLock from 'dom-focus-lock';
import { disableScroll, enableScroll } from 'Utils/scroll-lock';

// ToDo
// prevent duble create
// first param selector or body
// return focus to call object
// close on esc

/**
    transitionTime: 300,
    openOnCreate: true,
    destroyOnClose: true,
    onBeforeCreate : noop,
    onAfterCreate : noop,
    onBeforeOpen : func,
    onAfterOpen : func,
    onBeforeClose : func,
    onAfterClose : func,
    onBeforeDestroy : func,
    onAfterDestroy : func,
    params: obj,
    rootClass: '',
    modal: false,
 */

const delay = time => new Promise( resolve => setInterval( resolve, time ) );

function _createModal( opts ) {
    const modal = document.createElement('div');
    modal.classList.add( 'modal' );
    if( opts.rootClass !== '' )
        modal.className += ` ${ opts.rootClass }`;

    modal.insertAdjacentHTML( 'afterbegin', `
        <div class="modal__overlay" ${ !opts.modal ? 'data-close="true"' : '' }></div>
        <div class="popup popup_size_s modal__window" data-content>
        </div>
        `);
        // <div class="popup__content" data-content></div>

    const $content = document.querySelector( opts.selector );
    if( $content ) {
        if( $content.tagName.toLowerCase() == 'template' ) {
            modal.querySelector('[data-content]').appendChild( $content.content.cloneNode(true) );
        } else {
            modal.querySelector('[data-content]').appendChild( $content.cloneNode(true) );
        }
    }

    document.body.appendChild( modal );
    modal.style.setProperty('--transitionTime', `${ opts.transitionTime }ms` );

    return modal;
}

export default opts => {
    const defOpts = {
        transitionTime: 300,
        openOnCreate: true,
        destroyOnClose: true,
        rootClass: '',
        modal: false,
        params: {}
    }

    opts = {...defOpts, ...opts };
    let state = 'creating';

    let modal = {
        open,
        close,
        params: opts.params,
    }

    if (typeof opts.onBeforeCreate === 'function') {
        opts.onBeforeCreate({modal});
    }

    const $modal = _createModal(opts);
    state = 'created';

    if (typeof opts.onAfterCreate === 'function') {
        opts.onAfterCreate({ modal, dom : $modal });
    }

    const doDestroy = () => {
        return new Promise( resolve => {
            state = 'destroing';

            if (typeof opts.onBeforeDestroy === 'function') {
                opts.onBeforeDestroy($modal);
            }

            $modal.parentNode.removeChild($modal)
            $modal.removeEventListener('click', listener, true);
            state = 'destroyed';

            if (typeof opts.onAfterDestroy === 'function') {
                opts.onAfterDestroy();
            }

            resolve();
        })
    }

    const destroy = () => {
        return new Promise( ( resolve, reject ) => {
            if ( ![ 'created', 'closed', 'opened' ].includes( state ) ) {
                reject();
            }

            if( state == 'opened' ) {
                close().then( doDestroy ).then( resolve );
                return;
            }

            doDestroy().then( resolve );
        });
    }

    function open() {
        return new Promise( (resolve, reject) => {
            if ( ![ 'created', 'closed' ].includes( state ) ) {
                reject();
            }

            state = 'opening';

            if (typeof opts.onBeforeOpen === 'function') {
                opts.onBeforeOpen({ modal, dom : $modal });
            }

            disableScroll();
            delay( 0 ).then( () => $modal.classList.add('modal_open') );

            focusLock.on( $modal );

            delay( opts.transitionTime ).then( () => {
                if (typeof opts.onAfterOpen === 'function') {
                    opts.onAfterOpen({ modal, dom : $modal })
                }

                state = 'opened';
                resolve();
            });
        });
    }

    function close() {
        return new Promise( (resolve, reject ) => {
            if ( state != 'opened' ) {
                reject()
            }

            state = 'closing';
            $modal.classList.remove('modal_open');
            $modal.classList.add('modal_hide');

            if (typeof opts.onBeforeClose === 'function') {
                opts.onBeforeClose({ modal, dom : $modal });
            }

            delay( opts.transitionTime ).then( () => {
                enableScroll();
                $modal.classList.remove('modal_hide');
                state = 'closed';
                focusLock.off( $modal );

                if (typeof opts.onAfterClose === 'function') {
                    opts.onAfterClose({ modal, dom : $modal })
                }

                if( opts.destroyOnClose )
                    destroy();

                resolve();
            });
        })
    }

    const listener = event => {
        if ( 'close' in event.target.dataset ) {
            modal.close();
        }
    }

    $modal.addEventListener('click', listener, true);

    if( opts.openOnCreate )
        modal.open();

    $modal.modal = modal;

    return {...modal, ...{
        destroy,
        setContent(html) {
            $modal.querySelector('[data-content]').innerHTML = html;
        }
    }}
}