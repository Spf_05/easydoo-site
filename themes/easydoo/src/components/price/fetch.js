import { Notifications } from 'Components/Notifications/_';
function ValidationError(message) {
    this.name = 'ValidationError';
    this.message = message || 'Сообщение по умолчанию';
    this.stack = (new Error()).stack;
}

ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

const addSerializedItems = (arrSerialize, name, value) => arrSerialize.push(`${(name)}=${(value)}`);

const formSerialize = form => {
    const arr = [];
    const { elements } = form;

    for (let i = 0; i < elements.length; i += 1) {
      const input = elements[i];
      const {
        name, type, checked, value,
      } = input;

      const arrUnSerializedElmsType = ['file', 'reset', 'submit', 'button'];

      if (name && type.indexOf(arrUnSerializedElmsType) === -1) {
        if (type === 'select-multiple') {
          const selected = [...input.options].filter(k => k.selected);

          selected.forEach(j => this.addSerializedItems(arr, name, selected[j]));
        } else if ((type !== 'checkbox' && type !== 'radio') || checked) {
            addSerializedItems(arr, name, value);
        }
      }
    }

    return arr.join('&');
}

export default ( $form, appendData = [] ) => {
    return new Promise( async (resolve, reject) => {
        const endpoint = new URL ($form.action || window.location.href );
        const formData = new FormData( $form );
        const method = $form.method;
        const enctype = $form.enctype;

        appendData.forEach( ({ name, value }) => formData.append(name, value) );

        let options = {
            'method' : method,
            'headers' : {
                'Accept' : 'application/json',
            }
        };

        if ( method === 'get' ) {
            endpoint.search = new URLSearchParams( formData );
        } else {
            const octoberAjax = $form.dataset.requestx;

            if( octoberAjax !== undefined ) {
                options.headers[ 'x-october-request-handler' ] = octoberAjax;
                options.headers[ 'x-requested-with' ] = 'XMLHttpRequest';
            }

            if ( enctype === 'multipart/form-data' ) {
                options.body = formData;
            } else {
                // options.body = JSON.stringify ( Object.fromEntries( formData.entries() ) );
                // options.headers[ 'Content-Type' ] = 'application/json';
                options.headers[ 'Content-Type' ] = 'application/x-www-form-urlencoded';
                let data = formSerialize( $form );
                appendData.forEach( ({ name, value }) => data += `&${ name }=${ value }` );
                options.body = data;
            }
        }

        try {
            const rawResponse	= await fetch( endpoint, options );
            if ( rawResponse.status !== 200 || rawResponse.ok === false ) {
                throw rawResponse;
            }

            const respData	= await rawResponse.json();

            if( respData.X_OCTOBER_REDIRECT !== undefined )
                window.location	= respData.X_OCTOBER_REDIRECT;

            if( respData.text !== undefined ) {
                Notifications.make({ text :respData.text });
            }

            resolve( respData );
        } catch ( err ) {
            const makeValidationError = async () => {
                const json = await err.json();
                return new ValidationError( json['X_OCTOBER_ERROR_FIELDS'] );
            }

            const rejectError = err.status === 406
                            ? await makeValidationError()
                            : new Error( await err.text() );

            reject( rejectError );
        }
    });
}