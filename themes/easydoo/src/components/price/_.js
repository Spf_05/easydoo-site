document.querySelectorAll( '.js-tabs2' ).forEach( $root => {
    $root.querySelectorAll( '.js-tab' ).forEach( $tab => {
        $tab.onclick = e => {
           const $panel = $root.querySelector( `#${ $tab.dataset.control }` );

            if(!$panel)
                return;;

            $root.querySelectorAll( '.js-panel' ).forEach( $panel => $panel.classList.remove( 'price__tabs-content_show' ));
            $root.querySelectorAll( '.js-tab' ).forEach( $tab => $tab.parentElement.classList.remove( 'price__tabs-nav_active' ) );

            $tab.parentElement.classList.add('price__tabs-nav_active');
            $panel.classList.add('price__tabs-content_show');
        }
    });
});

import Swiper, { Pagination } from 'swiper';
import { getCssProperty } from 'Utils/helpers.js';

const initCarousel = $root => {
	const $carousel = $root.querySelector( '.js-carousel' );
	const $pagination = $root.querySelector( '.js-pagination' );

	if ( $carousel === null )
		return;

	let swiper = null;
	const getCarouselCssProperty = propName => parseFloat( getCssProperty($root, propName ) );
	let slidesCount = getCarouselCssProperty( '--carouselSlidesCount' );
	// const slidesGap = getCarouselCssProperty( '--carouselSlidesGap' );
    const slidesGap = 30;

	const options = {
		modules         : [ Pagination ],
		slidesPerView   : 4,
		spaceBetween    : slidesGap,
		slideClass      : 'js-slide',
		wrapperClass    : 'js-wrapper',
		loop            : false,
	}

	const renderCustom = (swiper, current, total) => {
		let templ = '';

		if( total == 1 )
			return '';

		for (let i = 1; i < total + 1; i++) {
			const crntClass = i == current ? ' carousel-pagination__item_active' : '';
			const crntAtte = i == current ? 'aria-current="true"' : '';
			templ += `<li class="carousel-pagination__item${ crntClass }">
				<button
					${ crntAtte }
					class="carousel-pagination__btn"
					type="button"
					aria-label="Слайд ${ i } из ${ total }"
					data-index="${ i - 1 }"
				></button>
			</li>`;
		}

		return templ;
	}

	const pagination = {
		el				: $pagination,
		type			: 'custom',
		clickable		: true,
		renderCustom	: renderCustom,
	}

	if ( $pagination !== null ) {
		options.pagination = pagination;

		$pagination.onclick = e => {
			const index = e.target.dataset.index;
			if( swiper === null || index === undefined)
				return;
	
				swiper.slideTo( index );
		}
	}

	function init() {
		if( swiper !== null ) {
			destroy();
		}

		if( $pagination !== null ) {
			$pagination.style.removeProperty( 'display' );
		}

		options.slidesPerView = slidesCount;
		swiper = new Swiper( $carousel, options );
		$root.classList.add( 'price_inited' );
	}

	function destroy() {
		swiper.destroy();
		$root.classList.remove( 'price_inited' );

		if( $pagination !== null ) {
			$pagination.innerHtml = '';
			$pagination.style.setProperty( 'display', 'none' );
		}
	}

	window.addEventListener( 'optimizedResize', e => {
		const newSlidesCount = getCarouselCssProperty( '--carouselSlidesCount' );
		if( slidesCount === newSlidesCount )
			return;

		slidesCount = newSlidesCount;
		init();
	});

	init();
}

document.querySelectorAll( '.js-price' ).forEach( initCarousel );


import modal from 'Components/modal/_.js';
import myFetch from 'Components/price/fetch.js';

const modalAfterCreate = ({ modal, dom }) => {
    const submitFormHandle = e => {
        e.preventDefault();

        const $form = e.target;
        const $submitButton = $form.querySelector('[type="submit"]');
        const redirectUrl = $form.dataset.redirect || '';

        if( $submitButton )
            $submitButton.disabled = true;

        const appendData = Object.entries( modal.params ).map( ([field, value ]) => {
            return { 'name' : field, 'value' : value }
        });

        $form.querySelectorAll( '[data-validation-error]' ).forEach( $el => $el.innerText = '' );

        const submitSuccess = data => {
            modal.close();
            if( redirectUrl == '' ) return;
            location.href = redirectUrl;
        }

        const submitError = err => {
            if( err.name == 'ValidationError' ) {
                Object.entries( err.message ).forEach( ([ name, errors ]) => {
                    const $el = $form.querySelector( `[data-validation-error="${ name }"]` );
                    if( $el === null )
                        return;
                    $el.innerText = errors[0];
                })
            } else {
                console.error( err.text );
            }
        }

        const submitFinally = () => {
            if( $submitButton )
                $submitButton.disabled = false;
        }

        myFetch( $form, appendData )
            .then( submitSuccess )
            .catch( submitError)
            .finally( submitFinally );
    }

    const $form = dom.querySelector('form');
    $form.onsubmit = submitFormHandle;
}

document.querySelectorAll( '.js-price-modal' ).forEach( $btn => {
    $btn.onclick = () => modal({
        modal: true,
        selector: $btn.dataset.id,
        rootClass: 'modal-form',
        onAfterCreate: modalAfterCreate,
    });
});

(()=>{
    const submitFormHandle = e => {
        e.preventDefault();

        const $form = e.target;
        const $submitButton = $form.querySelector('[type="submit"]');

        if( $submitButton )
            $submitButton.disabled = true;

        $form.querySelectorAll( '[data-validation-error]' ).forEach( $el => $el.innerText = '' );

        const submitSuccess = data => {
            $form.insertAdjacentHTML( 'beforebegin', '<p class="request-success">Ваше сообщение отправлено. Наши менеджеры ответят в ближайшее время</p>' );
            $form.remove();
        }

        const submitError = err => {
            if( err.name == 'ValidationError' ) {
                Object.entries( err.message ).forEach( ([ name, errors ]) => {
                    const $el = $form.querySelector( `[data-validation-error="${ name }"]` );
                    if( $el === null )
                        return;
                    $el.innerText = errors[0];
                })
            } else {
                console.log( err.text );
            }
        }

        const submitFinally = () => {
            if( $submitButton )
                $submitButton.disabled = false;
        }

        myFetch( $form )
            .then( submitSuccess )
            .catch( submitError)
            .finally( submitFinally );
    }

    const $form = document.querySelector('.js-request-test-form');
    if( !$form )
        return;
    $form.onsubmit = submitFormHandle;
})();