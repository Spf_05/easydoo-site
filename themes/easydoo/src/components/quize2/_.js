import Alpine from 'alpinejs'
import SlideTypeForm from './SlideTypeForm';
import SlideTypePhone from './SlideTypePhone';
import SlideTypeFinish from './SlideTypeFinish';
import SlideTypeFinish2 from './SlideTypeFinish2';
import SlideTypeText from './SlideTypeText';
import SlideTypeVideo from './SlideTypeVideo';
import SlideTypeIntro1 from './SlideTypeIntro1';
import SlideTypeIntro2 from './SlideTypeIntro2';

window.Alpine = Alpine;

const addSerializedItems = (arrSerialize, name, value) => arrSerialize.push(`${(name)}=${(value)}`);

const formSerialize = form => {
    const arr = [];
    const { elements } = form;

    for (let i = 0; i < elements.length; i += 1) {
      const input = elements[i];
      const {
        name, type, checked, value,
      } = input;

      const arrUnSerializedElmsType = ['file', 'reset', 'submit', 'button'];

      if (name && type.indexOf(arrUnSerializedElmsType) === -1) {
        if (type === 'select-multiple') {
          const selected = [...input.options].filter(k => k.selected);

          selected.forEach(j => this.addSerializedItems(arr, name, selected[j]));
        } else if ((type !== 'checkbox' && type !== 'radio') || checked) {
            addSerializedItems(arr, name, value);
        }
      }
    }

    return arr.join('&');
}

const quize = data => {
    return {
        currentSlide : 0,
        slides: [],
        nextDisabled: false,
        nextBtnText: 'Начать',
        quizeId: null,
        quizeTitle : '',
        redirect : '',
        root: null,

        async init() {
            const res = await fetch(`/api/quize2/${ data.id }`,{ cache: "no-cache" });
            const resData = await res.json();

            this.quizeId = resData.id;
            this.quizeTitle = resData.title;
            this.root = this.$root;

            resData.slides.forEach( slide => {
                let instance = null;

                switch ( slide._group ) {
                    case 'form':
                        instance = new SlideTypeForm( slide, this );
                        break;
                    case 'phone':
                        instance = new SlideTypePhone( slide, this );
                        break;
                    case 'finish':
                        instance = new SlideTypeFinish( slide, this );
                        break;
                    case 'finish2':
                        instance = new SlideTypeFinish2( slide, this );
                        break;
                    case 'text':
                        instance = new SlideTypeText( slide, this );
                        break;
                    case 'video':
                        instance = new SlideTypeVideo( slide, this );
                        break;
                    case 'intro1':
                        instance = new SlideTypeIntro1( slide, this );
                        break;
                    case 'intro2':
                        instance = new SlideTypeIntro2( slide, this );
                        break;
                }

                if( instance !== null )
                    this.slides.push( instance );
            });

            this.slide = this.slides[0];

            this.$watch('slides', (value, oldValue) => this.updateNextDisabled() );
            this.$watch('currentSlide', (value, oldValue) => this.updateNextDisabled() );
            this.updateNextDisabled();
        },
        updateNextDisabled() {
            const slide = this.slides[ this.currentSlide ];
            slide.show();
            this.nextDisabled = slide.canNext();

            if( this.currentSlide == 0 ) {
                this.nextBtnText = 'Начать'
            }

            if( this.currentSlide > 0 ) {
                this.nextBtnText = 'Далее'
            }

            if( this.currentSlide == this.slides.length - 1 ) {
                this.nextBtnText = 'Отправить'
            }
        },
        async submitData() {
            let body = {
                id      : this.quizeId,
                title   : this.quizeTitle,
                data    : []
            };

            this.slides.forEach( slide => {
                if( slide.isStored === false ) return;
                const slideData = slide.getStoreData();
                body.data.push( slideData );
            });

            const options = {
                'method' : 'POST',
                'body'   : JSON.stringify( body ),
                'headers' : {
                    'Content-Type' : 'application/json',
                    'x-october-request-handler' : 'onQuize2',
                    'x-requested-with' : 'XMLHttpRequest',
                }
            }

            try {
                const rawResponse = await fetch( window.location.href, options );

                if ( rawResponse.status !== 200 || rawResponse.ok === false ) {
                    throw rawResponse;
                }

                const resData	= await rawResponse.json();
                console.log('resData', resData );
                
                this.redirect	= resData.result;
            } catch (err) {
                console.log( err );
            }
        },
        nextSlide() {
            this.currentSlide++;

            this.$nextTick(() => {
                this.root.scrollIntoView({ behavior: 'smooth' });
            });

            if( this.currentSlide == this.slides.length - 1 ) {
                this.submitData();
            }
        },
        prevSlide() {
            this.currentSlide--;
        },
        btnNextSlide() {
            return {
                ['x-text']() {
                    return this.nextBtnText;
                },
                async ['@click']() {
                    this.nextSlide();
                }
            }
        },

        /* helpers */
        getStorePath( file ) {
            return `/storage/app/media${ file }`;
        },
        priceFormat(price) {
            return `${ price }₽`
        },
        getPercent(newPrice, oldPrice) {
            const percent = Math.round( (newPrice-oldPrice) / (oldPrice/100) );
            return `${ percent }%`
        },
        nl2br( str ) {
            return str.replace(/([^>])\n/g, '$1<br/>');
        },
        formSlideModificator() {
            const slide = this.slides[ this.currentSlide ];
            if ( slide._group == 'form' && slide.data.fields.length ) {
                const fieldsType = slide.data.fields[0]._group;
                return `common-block_${ fieldsType }`;
            }

            return '';
        },
    }
}

Alpine.data('quize2', quize )
Alpine.start()