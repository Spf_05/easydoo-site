import Alpine from 'alpinejs'

window.Alpine = Alpine;

const addSerializedItems = (arrSerialize, name, value) => arrSerialize.push(`${(name)}=${(value)}`);

const formSerialize = form => {
    const arr = [];
    const { elements } = form;

    for (let i = 0; i < elements.length; i += 1) {
      const input = elements[i];
      const {
        name, type, checked, value,
      } = input;

      const arrUnSerializedElmsType = ['file', 'reset', 'submit', 'button'];

      if (name && type.indexOf(arrUnSerializedElmsType) === -1) {
        if (type === 'select-multiple') {
          const selected = [...input.options].filter(k => k.selected);

          selected.forEach(j => this.addSerializedItems(arr, name, selected[j]));
        } else if ((type !== 'checkbox' && type !== 'radio') || checked) {
            addSerializedItems(arr, name, value);
        }
      }
    }

    return arr.join('&');
}

class Slidex {
    constructor( slide ) {
        this.slide = slide;
        this.prepare();
        console.log('dddd');
    }

    prepare() {

    }
}

class xFinish2 extends Slidex {
    prepare() {
        // this.timerText = '';
        // this.timerLeft = this.slide.minutes * 60;

        // this.timerId = setInterval( () => {
        //     this.timerLeft--;

        //     if( this.timerLeft == 0 ) {
        //         clearInterval( this.timerId );
        //     }
        // }, 1000);
    }

    showSeconds() {
        return new Date(this.timerLeft * 1000).toISOString().substr(11, 8);
    }
}

class xPhone extends Slidex {
    prepare() {
        this.slide.checkPhone = false;
        this.slide.code = '';
        this.slide.verified = false;
        this.slide.checkCode = false;
        this.slide.codeConfirmed = false;

        this.slide.fields = [
            {
                error: '',
                name : 'phone',
                label: 'Телефон',
                required : true,
                value : '',
            }
        ];
    }
}

const quize = data => {
    return {
        currentSlide : 0,
        slides: [],
        nextDisabled: false,
        nextBtnText: 'Начать',
        quizeId: null,
        phoneVerified : false,
        phoneVerifiedCode : '',
        phoneVerifiedCodeError : '',
        quizeTitle : '',
        // timerLeft : null,
        timerId : null,
        timerText: '',
        redirect : '',

        async init() {
            const res = await fetch(`/api/quize2/${ data.id }`,{ cache: "no-cache" });
            const resData = await res.json();

            let slides = resData.slides;
            this.quizeId = resData.id;
            this.quizeTitle = resData.title;

            slides.forEach( slide => {
                if( [ 'form' ].includes( slide._group ) ) {
                    if( slide.fields.length == 0 )
                        return;

                    const fieldsType = slide.fields[0]._group;

                    if( fieldsType == 'input' ) {
                        slide.fields[0].inputs.forEach( field => {
                            field.required = field.required == '0' ? false : true;
                            field.value = '';
                        })
                    }

                    if( [ 'checkbox', 'radio' ].includes( fieldsType ) ) {
                        slide.values = [];
                        slide.fields[0].inputs.forEach( field => field.checked = false )
                    }
                }

                if( [ 'phone' ].includes( slide._group ) ) {
                    slide.instance = new xPhone( slide );
                }

                if( [ 'finish2' ].includes( slide._group ) ) {
                    slide.instance = new xFinish2( slide );

                    // this.timerText = '';
                    // this.timerLeft = slide.minutes * 60;

                    // this.timerId = setInterval( () => {
                    //     this.timerLeft--;

                    //     if( this.timerLeft == 0 ) {
                    //         clearInterval( this.timerId );
                    //     }
                    // }, 1000);
                }
            });

            this.slides = slides;

            this.$watch('slides', (value, oldValue) => this.updateNextDisabled() );
            this.$watch('currentSlide', (value, oldValue) => this.updateNextDisabled() );
            this.updateNextDisabled();
        },
        getInputId( name, idx ) {
            return `input-${ name }-${ idx }`;
        },
        getStorePath( file ) {
            return `/storage/app/media${ file }`;
        },
        updateNextDisabled() {
            const slide = this.slides[ this.currentSlide ];
            const slideType = slide._group;

            if( this.currentSlide == 0 ) {
                this.nextBtnText = 'Начать'
            }

            if( this.currentSlide > 0 ) {
                this.nextBtnText = 'Далее'
            }

            if( this.currentSlide == this.slides.length - 1 ) {
                this.nextBtnText = 'Отправить'
            }

            if( slideType == 'text' ) {
                return this.nextDisabled = false;
            }

            if( slideType == 'phone' ) {
                return this.nextDisabled = !this.phoneVerified;
            }

            if( slideType == 'video' ) {
                return this.nextDisabled = false;
            }

            if( [ 'phone' ].includes( slideType ) ) {
                let disabled = false;

                slide.fields.forEach( field => {
                    if( field.required == true && field.value == '' ) {
                        disabled = true;
                        return;
                    }
                })

                return this.nextDisabled = disabled;
            }

            if( [ 'form' ].includes( slideType ) ) {
                let disabled = false;
                const fieldsType = slide.fields[0]._group;

                if( fieldsType == 'input' ) {
                    slide.fields[0].inputs.forEach( field => {
                        if( field.required == true && field.value == '' ) {
                            disabled = true;
                            return;
                        }
                    })
                }

                if( [ 'checkbox', 'radio' ].includes( fieldsType ) ) {
                    this.nextDisabled = slide.values.length ? false : true;
                    return;
                }

                return this.nextDisabled = disabled;
            }
        },
        playVideo(e){
            const btn = e.currentTarget;
            btn.previousElementSibling.play();
            btn.remove();
        },
        prevSlide() {
            this.currentSlide--;
        },

        inputRadioLabel: (( slide,field ) => {
            return {
                'for': `${ slide._id }-${ slide.name }-${ field.value }`,
            }
        }),
        inputRadioAttr: (( slide,field ) => {
            // x-model="field.value"
            return {
                'type': `${ slide._group }`,
                'value': `${ field.value }`,
                'id': `${ slide._id }-${ slide.name }-${ field.value }`,
                'name': `${ slide.name }[]`,
                // ['x-model']: field.value,
            }
        }),
        async submitData() {
            let body = {
                id : this.quizeId,
                title : this.quizeTitle,
                data : []
            };

            this.slides.forEach( slide => {
                if( ![ 'phone', 'form' ].includes( slide._group ) ) {
                    return;
                }

                const slideId = slide._id;

                if( slide.fields.length == 0 )
                    return;

                let slideData = {
                    'id' : slideId,
                    'title' : slide.title,
                    'type' : slide._group == 'phone' ? 'phone' : slide.fields[0]._group,
                    'fields': {}
                }

                if( [ 'phone' ].includes( slide._group ) ) {
                    slideData.fields[ 'phone' ] = {
                        label: 'Телефон',
                        value: slide.fields[0].value,
                        name: slide.fields[0].name,
                    }
                }

                if( [ 'form' ].includes( slide._group ) ) {
                    const fieldsType = slide.fields[0]._group;
                    const inputs = slide.fields[0].inputs;

                    if( [ 'input' ].includes( fieldsType ) ) {
                        inputs.forEach( field => {
                            slideData.fields[ field.name ] = {
                                label: field.label,
                                value: field.value,
                                name: field.name,
                            }
                        });
                    }

                    if( [ 'checkbox', 'radio' ].includes( fieldsType ) ) {
                        const name = slide.fields[0].name;

                        const arr = inputs.map( inp => {
                            let checked = false;

                            if ( fieldsType == 'checkbox' ) {
                                const checkedIdx = slide.values.findIndex( val => val == inp.value );
                                checked = checkedIdx === -1 ? false : true
                            } else {
                                checked = slide.values == inp.value
                            }

                            return {
                                value: inp.value,
                                label: inp.label,
                                checked: checked,
                                name: name,
                            };
                        });

                        slideData.fields[ slide.fields[0].name ] = arr;
                    }
                }

                body.data.push(slideData );
            });

            const options = {
                'method' : 'POST',
                'body'   : JSON.stringify( body ),
                'headers' : {
                    'Content-Type' : 'application/json',
                    'x-october-request-handler' : 'onQuize2',
                    'x-requested-with' : 'XMLHttpRequest',
                }
            }

            try {
                const rawResponse = await fetch( window.location.href, options );

                if ( rawResponse.status !== 200 || rawResponse.ok === false ) {
                    throw rawResponse;
                }

                const resData	= await rawResponse.json();
                this.redirect	= resData.result;
            } catch (err) {
                console.log( err );
            }
        },
        nextSlide() {
            this.currentSlide++;
            const $root = this.$root;

            this.$nextTick(() => {
                $root.scrollIntoView({ behavior: 'smooth' });
            });

            if( this.currentSlide == this.slides.length - 1 ) {
                this.submitData();
            }
        },
        btnNextSlide: function() {
            return {
                ['x-text']() {
                    return this.nextBtnText;
                },
                async ['@click']() {
                    this.nextSlide();
                }
            }
        },



        /* ToDo переписать все к херам! */
        checkCode( phone, code ) {
            this.slide.errorCode = '';

            const requestOptions = {
                method  : 'POST',
                headers : { 'Content-Type': 'application/json' },
                body    : JSON.stringify({ phone, code })
            };
        
            const endpoint = `/api/auth/login`;
        
            return fetch(endpoint, requestOptions)
                .then(res => res.json() )
                .then(user => {
                    if (user.token) {
                        localStorage.setItem('user', JSON.stringify(user));
                    }
                    return user;
                });
        },
        inputCode(e) {
            const $el = e.currentTarget;
            const elIdx = parseInt( $el.dataset.idx );

            // todo paste clipboard

            e.preventDefault();

            if(e.which != 8 && isNaN(String.fromCharCode(e.which))){
                return;
            }

            $el.value = e.key[0];

            const codes = Array.from(Array(4))
                                .map( (x, idx ) => {
                                    return this.$refs[ `input${ idx + 1 }` ].value
                                });

            if( elIdx < 4 )
                this.$refs[ `input${ elIdx + 1 }` ].focus();

            const code = codes.join('');

            const fail = () => {
                this.slide.errorCode = 'Код не верный';
                Array.from(Array(4))
                    .forEach( (x, idx ) => this.$refs[ `input${ idx + 1 }` ].value = '' );

                this.$refs[ 'input1' ].focus();
            }

            if( code.length == 4 ) {
                this.checkCode( this.slide.phone, code )
                    .then( res => {
                        if( res.data == null ) {
                            fail()
                        } else {
                            // this.slide.checkCode = false;
                            // this.slide.codeConfirmed = true;

                            this.nextSlide();
                        }
                    })
                    .catch( err => {
                        fail();
                    });
            }
        },
        formSlideModificator() {
            const slide = this.slides[ this.currentSlide ];
            if ( slide._group == 'form' && slide.fields.length ) {
                const fieldsType = slide.fields[0]._group;
                return `common-block_${ fieldsType }`;
            }

            return '';
        },
        clearTimer() {
            this.slide.errorCode = '';
            this.slide.codeConfirmed = false;
            this.slide.checkCode = false;
            clearInterval( this.slide.timerId );
        },
        setConfirmTimer( time ) {
            this.slide.leftSeconds = time;
            this.slide.codeComment = `осталось время ${ this.slide.leftSeconds } сек.`;

            this.slide.timerId = setInterval( () => {
                this.slide.codeComment = `осталось время ${ this.slide.leftSeconds } сек.`;
                this.slide.leftSeconds--;

                if( this.slide.leftSeconds == 0 ) {
                    this.clearTimer();
                }
            }, 1000 );

            this.slide.checkCode = true;
        },
        getCode( phone ) {
            const requestOptions = {
                method  : 'POST',
                headers : { 'Content-Type': 'application/json' },
                body    : JSON.stringify({ phone })
            };

            const endpoint = `/api/auth/get-code`;

            return new Promise( (resolve, reject) => {
                try {
                    fetch(endpoint, requestOptions)
                        .then(res => res.json() )
                        .then(data => {
                            if( data.status === false ) {
                                reject( data.message )
                            }

                            resolve( data.data );
                        } )
                        .catch( err => reject( err.message ));
                } catch ( err ) {
                    reject( err.message );
                }
            });
        },
        priceFormat(price) {
            return `${ price }₽`
        },
        getPercent(newPrice, oldPrice) {
            const percent = Math.round( (newPrice-oldPrice) / (oldPrice/100) );
            return `${ percent }%`
        },
        nl2br( str ) {
            return str.replace(/([^>])\n/g, '$1<br/>');
        },
        async requestVerifyPhone() {
            this.slide.fields[0].error = '';
            const phone = this.slide.fields[0].value;

            try {
                const { time } = await this.getCode( phone );
                this.setConfirmTimer( time );
                this.slide.phone = phone;
                this.slide.checkCode = true;
            } catch ( err ) {
                this.slide.fields[0].error = err;
            }
        },
    }
}

Alpine.data('quize2', quize )
Alpine.start()