import SlideDefault from "./SlideDefault";
import GroupRadio from "./fields-groups/GroupRadio";
import GroupCheckbox from "./fields-groups/GroupCheckbox";
import GroupInput from "./fields-groups/GroupInput";

export default class extends SlideDefault {
    isStored = true;

    prepare() {
        if( this.data.fields.length == 0 )
            return;

        const fieldData = this.data.fields[0];
        this.groupType = fieldData._group;

        switch ( fieldData._group ) {
            case 'input':
                this.instance = new GroupInput( fieldData );
                break;
            case 'checkbox':
                this.instance = new GroupCheckbox( fieldData );
                break;
            case 'radio':
                this.instance = new GroupRadio( fieldData );
                break;
        }
    }

    canNext() {
        return this.instance.canNext();
    }

    getStoreData() {
        const slideData = {
            'id'        : this.data._id,
            'title'     : this.data.title,
            'type'      : this.groupType,
            'fields'    : this.instance.getStoreData(),
        }

        return slideData;
    }
}
