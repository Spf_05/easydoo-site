import GroupDefault from './GroupDefault';

export default class extends GroupDefault {
    prepare() {
        this.values = [];
        this.inputs = [];

        this.fieldData.inputs.forEach( ( field, idx ) => {
            this.inputs.push({
                type    : 'radio',
                checked : false,
                id      : this._generateId( this.fieldData.name, idx ),
                name    : `${ this.fieldData.name }[]`,
                icon    : field.icon,
                label   : field.label,
                value   : field.value,
            });
        });
    }

    canNext() {
        return this.values.length ? false : true;
    }

    getStoreData() {
        const fieldsData = {};
        const name = this.fieldData.name;

        const arr = this.inputs.map( inp => {
            return {
                value   : inp.value,
                label   : inp.label,
                checked : this.values == inp.value,
                name    : name,
            };
        });

        fieldsData[ name ] = arr;

        return fieldsData;
    }
}