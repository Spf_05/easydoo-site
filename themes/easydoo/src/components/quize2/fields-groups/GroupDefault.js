export default class {
    constructor( fieldData ) {
        this.fieldData  = fieldData;
        this.prepare();
    }

    prepare() {
    }

    canNext() {
        
    }

    getStoreData() {

    }

    _generateId( name, idx ) {
        return `input-${ name }-${ idx }`;
    }
}