import GroupDefault from './GroupDefault';
import Iodine from '@caneara/iodine';

const valid = new Iodine();

valid.rule('phone', value => {
    const re = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/
    value = value.replace(/\s/g, '');
    return re.test( value );
});

valid.setErrorMessage( 'phone', 'Поле должно быть телефоном' );
valid.setErrorMessage( 'email', 'Поле должно быть email' );
valid.setErrorMessage( 'required', 'Поле обязательное' );
valid.setErrorMessage( 'numeric', 'Поле должно быть числом' );
valid.setErrorMessage( 'maxLength', 'Поле должно быть не более [PARAM] символов' );
valid.setErrorMessage( 'minLength', 'Поле должно быть не менее [PARAM] символов' );
valid.setErrorMessage( 'max', 'Поле должно быть меньше или равно [PARAM]' );
valid.setErrorMessage( 'min', 'Поле должно быть больше или равно [PARAM]' );

function checkValid() {
    let errors = [];
    const res = valid.assert( this.value, this.rules );

    if( res.valid === false ) {
        this.error = res.error;
        errors.push( res.error );
    } else {
        this.error = '';
    }

    return errors;
}

function setRules( field, defData ) {
    let rules = [];

    if( field.required === true ) {
        rules.push('required');
    } else {
        rules.push('optional');
    }

    if( [ 'number', 'text' ].includes( field.type ) ) {
        if( defData.maxValueActive == 1 )
            rules.push(`max:${ defData.maxValue }`);

        if( defData.minValueActive == 1 )
            rules.push(`min:${ defData.minValue }`);

        if( defData.minLengthActive == 1 )
            rules.push(`minLength:${ defData.minLength }`);

        if( defData.maxLengthActive == 1 )
            rules.push(`maxLength:${ defData.maxLength }`);
    }

    if( field.type == 'number' )
        rules.push('numeric');

    if( field.type == 'email' )
        rules.push('email');

    if( field.type == 'tel' )
        rules.push('phone');

    return rules;
}

function setType( type ) {
    switch (type) {
        case 'number':
            return 'number';
        case 'email':
            return 'email';
        case 'tel':
            return 'tel';
        case 'tel':
            return 'tel';
        default:
            return 'text';
    }
}

export default class extends GroupDefault {
    prepare() {
        this.inputs     = [];

        this.fieldData.inputs.forEach( ( field, idx ) => {
            const inputData = {
                required    : field.required == '0' ? false : true,
                id          : this._generateId( this.fieldData.name, idx ),
                name        : field.name,
                label       : field.label,
                type        : setType( field.type ),
                fieldType   : field.type,
                error       : '',
                value       : '',
                rules       : [],
                changed     : false,
                checkValid  : checkValid
            }

            inputData.rules = setRules( inputData, field );

            this.inputs.push( inputData );
        })
    }

    canNext() {
        let errors = [];

        this.inputs.forEach( field => {
            errors = [...errors, ...field.checkValid() ];
        });

        return errors.length > 0;
    }

    getStoreData() {
        let fieldsData = {};

        this.inputs.forEach( field => {
            fieldsData[ field.name ] = {
                label       : field.label,
                value       : field.value,
                name        : field.name,
                fieldType   : field.fieldType,
            }
        });

        return fieldsData;
    }
}