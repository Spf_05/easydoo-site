import SlideDefault from "./SlideDefault";

export default class extends SlideDefault {
    canNext() {
        return false;
    }

    playVideo({ target : $btn }) {
        $btn.previousElementSibling.play();
        $btn.remove();
    }
}