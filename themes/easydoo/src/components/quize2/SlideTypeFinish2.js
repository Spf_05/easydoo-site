import SlideDefault from "./SlideDefault";

export default class extends SlideDefault {
    prepare() {
        this.data.timerText = '';
        this.data.timerLeft = this.data.minutes * 60;
    }

    onShow() {
        this._startTimer();
    }

    onHide() {
        this._stopTimer();
    }

    showSeconds() {
        return new Date(this.data.timerLeft * 1000).toISOString().substr(11, 8);
    }

    playVideo({ target : $btn }) {
        $btn.previousElementSibling.play();
        $btn.remove();
    }

    _startTimer() {
        this.timerId = setInterval( () => {
            this.data.timerLeft--;
            if( this.data.timerLeft == 0 ) this._stopTimer();
        }, 1000);
    }

    _stopTimer() {
        clearInterval( this.timerId );
        this.data.timerLeft = this.data.minutes * 60;
    }
}