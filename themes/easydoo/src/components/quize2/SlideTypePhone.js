import SlideDefault from "./SlideDefault";

export default class extends SlideDefault {
    isStored = true;

    prepare() {
        this.data.checkPhone = false;
        this.data.code = '';
        this.data.verified = false;
        this.data.checkCode = false;
        this.data.codeConfirmed = false;
        this.data.errorCode = '';

        this.data.leftSeconds = 0;
        this.data.codeComment = '';
        this.timerId = null;
        this.OTPInputs = [];

        this.codeKeyPressHandler = this.codeKeyPressHandler.bind( this );

        this.data.fields = [
            {
                error: '',
                name : 'phone',
                label: 'Телефон',
                required : true,
                value : '',
            }
        ];
    }

    canNext() {
        return !this.data.phoneVerified;
    }

    getStoreData() {
        return {
            'id'        : this.data._id,
            'title'     : this.data.title,
            'type'      : 'phone',
            'fields'    : {
                'phone' : {
                    label   : 'Телефон',
                    value   : this.data.fields[0].value,
                    name    : this.data.fields[0].name,
                }
            }
        }
    }

    codeKeyPressHandler(e) {
        e.preventDefault();
        const $el = e.currentTarget;
        const elIdx = parseInt( $el.dataset.idx );

        // todo paste clipboard

        if(e.which != 8 && isNaN(String.fromCharCode(e.which))){
            return;
        }

        $el.value = e.key[0];

        const code = Array.from( this.OTPInputs )
                        .reduce( ( code, $input ) => code += $input.value, '' );

        if( e.which == 8 ) {
            elIdx--;
            if( elIdx < 0 ) elIdx = 0;
            this.OTPInputs[ elIdx ].focus();
        } else {
            if( elIdx < 4 )
                this.OTPInputs[ elIdx ].focus();
        }

        const fail = () => {
            this.data.errorCode = 'Код не верный';
            Array.from( this.OTPInputs ).forEach( $input => $input.value = '' );
            this.OTPInputs[ 0 ].focus();
        }

        if( code.length == 4 ) {
            this._checkCode( this.data.phone, code )
                .then( res => {
                    if( res.data == null ) {
                        fail()
                    } else {
                        // this.data.checkCode = false;
                        this.data.codeConfirmed = true;
                        this.data.phoneVerified = true;
                        this.data.checkPhone = true;
                        this.quize.nextSlide();
                    }
                })
                .catch( err => {
                    fail();
                });
        }
    }

    async requestVerifyPhone() {
        this.data.fields[0].error = '';
        const phone = this.data.fields[0].value;

        try {
            const { time } = await this._fetchOTP( phone );
            this._setConfirmTimer( time );
            this.data.phone = phone;
            this.data.checkCode = true;
            setTimeout(() => this.OTPInputs = document.querySelectorAll('[data-selector="otp-input"]'), 0);
        } catch ( err ) {
            this.data.fields[0].error = err;
        }
    }

    _checkCode( phone, code ) {
        this.data.errorCode = '';

        const requestOptions = {
            method  : 'POST',
            headers : { 'Content-Type': 'application/json' },
            body    : JSON.stringify({ phone, code })
        };

        const endpoint = `/api/auth/login`;

        return fetch(endpoint, requestOptions)
            .then(res => res.json() )
            .then(user => {
                if (user.token) {
                    localStorage.setItem('user', JSON.stringify(user));
                }
                return user;
            });
    }

    _clearTimer() {
        this.data.errorCode = '';
        this.data.codeConfirmed = false;
        this.data.checkCode = false;
        clearInterval( this.timerId );
    }

    _setConfirmTimer( time ) {
        this.data.leftSeconds = parseInt( time );
        this.data.codeComment = `осталось время ${ this.data.leftSeconds } сек.`;

        this.timerId = setInterval( () => {
            this.data.codeComment = `осталось время ${ this.data.leftSeconds } сек.`;
            this.data.leftSeconds--;

            if( this.data.leftSeconds == 0 ) {
                this._clearTimer();
            }
        }, 1000 );

        this.data.checkCode = true;
    }

    _fetchOTP( phone ) {
        const requestOptions = {
            method  : 'POST',
            headers : { 'Content-Type': 'application/json' },
            body    : JSON.stringify({ phone })
        };

        const endpoint = `/api/auth/get-code`;

        return new Promise( (resolve, reject) => {
            try {
                fetch(endpoint, requestOptions)
                    .then(res => res.json() )
                    .then(data => {
                        if( data.status === false ) {
                            reject( data.message )
                        }

                        resolve( data.data );
                    } )
                    .catch( err => reject( err.message ));
            } catch ( err ) {
                reject( err.message );
            }
        });
    }
}