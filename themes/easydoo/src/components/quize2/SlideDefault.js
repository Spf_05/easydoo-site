export default class {
    isStored = false;

    constructor( slide, quize ) {
        this.data = slide;
        this.quize = quize;
        this._group = slide._group;
        this.prepare();
        this.isShow = false;
    }

    prepare() {

    }

    canNext() {

    }

    show() {
        if( this.isShow === true ) return;
        this.isShow = true;
        this.onShow();
    }

    hide() {
        if( this.isShow === false ) return;
        this.isShow = false;
        this.onHide();
    }

    onShow() {
    }

    onHide() {
    }
}