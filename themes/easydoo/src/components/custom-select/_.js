const init = $root => {
    $toggle = $root.querySelector( '.js-toggle' );
    $label = $root.querySelector( '.js-label' );
    $dropdown = $root.querySelector( '.js-dropdown' );

    const setLabel = text => $label.innerText = text.trim();

    $toggle.onclick = e => $root.classList.toggle( 'custom-select_open' );

    let labelText = 'Выберите значение';
    $selectedRadio = $root.querySelector( 'input[type="radio"]:checked + .js-item-label' );

    if( $selectedRadio !== null )
        labelText = $selectedRadio.innerText;

    setLabel( labelText );

    $root.addEventListener( 'change', e => {
        const $itemLabel = e.target.parentNode.querySelector( '.js-item-label' );
        setLabel( $itemLabel !== null ? $itemLabel.innerText : '' );
        document.body.dispatchEvent( new CustomEvent( 'form-filter' ) );
    });

    document.body.addEventListener( 'click', e => {
        if ( e.target === $toggle )
            return;
        $root.classList.remove( 'custom-select_open' )
    });
}

document.querySelectorAll( '.js-custom-select' ).forEach( init );