import Swiper, { Navigation } from 'swiper';

const makeNavBtn = type => {
    let $btn = document.createElement('button');
    $btn.type = 'button';
    $btn.setAttribute( 'class', `authors-list__navs authors-list__navs_${ type }` );

    const icon = type == 'prev' ? 'arrow-left' : 'arrow-right';

    $btn.innerHTML = `
    <svg xmlns="http://www.w3.org/2000/svg" class="button-secondary__icon" role="presentation" width="35" height="24">
        <use xlink:href="/themes/easydoo/assets/img/components/sprites/icons.svg#${ icon }"></use>
    </svg>
    `

    return $btn;
}

const initCarousel = $root => {
    const $carousel = $root.querySelector( '.js-carousel' );
    const $navsWrapper = $root.querySelector( '.js-navs' );

    if ( $carousel === null )
        return;

    let $btnPrev = null;
    let $btnNext = null;

    if( $navsWrapper !== null ) {
        $btnPrev = makeNavBtn( 'prev' );
        $btnNext = makeNavBtn( 'next' );

        $navsWrapper.appendChild( $btnPrev );
        $navsWrapper.appendChild( $btnNext );
    }

    let swiper = null;

    const options = {
        modules         : [ Navigation ],
        slidesPerView   : 'auto',
        spaceBetween    : 20,
        slideClass      : 'js-slide',
        wrapperClass    : 'js-wrapper',
        loop            : false,
        navigation: {
            nextEl: $btnNext,
            prevEl: $btnPrev,
            disabledClass: 'authors-list__navs_disabled',
        },
        breakpoints: {
            1000: {
                slidesPerView: 3,
            },
            1218: {
                slidesPerView: 3,
                spaceBetween: 30,
            }
        }
    }

    function init() {
        if( swiper !== null ) {
            destroy();
        }

        swiper = new Swiper( $carousel, options );
        $root.classList.add( 'authors-list_inited' );
    }

    init();
}

document.querySelectorAll( '.js-authors-list' ).forEach( initCarousel );