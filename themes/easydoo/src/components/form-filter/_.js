(()=>{
    const $form = document.querySelector( '.js-form-filter' );

    if( !$form )
        return;

    document.body.addEventListener( 'form-filter', () => {
        window.history.replaceState( '', '', '?' + $( '#filter-form' ).serialize() );
    
        $( '#filter-form' ).request( 'ProgramsList::onFilter', {
            loading : window.$.oc.stripeLoadIndicator,
            update  : {
                'programs-list/list' : '#programs-list'
            },
            success: data => {
                const $part = document.querySelector( '#programs-list' )
                if( $part && data[ 'programs-list/list' ] ) {
                    $part.innerHTML = data[ 'programs-list/list' ];
                }
                closeForm();
                // window.history.replaceState( '', '', '?' + data.url );
            }
        })
    });
    
    const toggleForm = () => document.body.classList.toggle( 'filters-open' );
    const closeForm = () => document.body.classList.remove( 'filters-open' );
    
    document.querySelectorAll( '.js-toggle-filters' ).forEach( $btn => $btn.onclick = toggleForm );
    
    $form.addEventListener( 'submit', e => {
        document.body.dispatchEvent( new CustomEvent( 'form-filter' ) );
        e.preventDefault();
    });
    
    $form.addEventListener( 'change', e => {
        if( window.innerWidth > 1000 ) {
            document.body.dispatchEvent( new CustomEvent( 'form-filter' ) );
        }
    });
})();