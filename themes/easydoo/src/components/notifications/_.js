import facon from 'facon';
// import { makeIcon } from 'AtlasCommon/icons-sprite/_.js';

const makeIcon = () => '';

const typeClasses = {
    'danger' : 'notifications__item_danger',
    'success' : 'notifications__item_success',
    'warning' : 'notifications__item_warning',
}

export const notificationTypes = Object.keys( typeClasses );

window.Notifications = Notifications;
export class Notifications {
    static makeRoot() {
        if ( this.root )
            return;

        const $root = facon`<div ref="root" class="notifications"></div>`;

        $root.collect({ to : this });
        document.body.appendChild( $root );
    }

    static make( prop ) {
        this.makeRoot();

        let timerId = null;

        const defProp = {
            text : '',
            icon : null,
            link : null,
            delay : 4000,
        }

        const setHideTimer = () => {
            timerId = setTimeout( () => {
                item.classList.remove( 'notifications__item_show' );
                setTimeout( () => item.remove(), 300 );
            }, prop.delay );
        }

        prop = { ...defProp, ...prop };

        const typeClass = typeClasses[ prop.type ] || typeClasses[ 'success' ];

        const itemNode = facon`
            <div role="alert" aria-live="assertive" aria-atomic="true" aria-atomic="true" ref="item" class="notifications__item ${ typeClass }">
                <div ref="colIcon" class="notifications__col-icon">
                    ${ makeIcon({ width: 24, height: 24, icon: prop.icon, css: 'notifications__icon' }) }
                </div>

                <div class="notifications__col-text">${ prop.text }</div>

                <div ref="colLink" class="notifications__col-link">
                    <a class="btn btn_size_s btn_default notifications__link" href="${ prop.link && prop.link.url ? prop.link.url : '' }">${ prop.link && prop.link.text ? prop.link.text : '' }</a>
                </div>
            </div>
        `

        const { item, colIcon, colLink } = itemNode.collect();

        if( !prop.icon )
            colIcon.remove();

        if( !prop.link )
            colLink.remove();

        item.onmouseenter = e => clearTimeout( timerId );
        item.onmouseleave = e => setHideTimer();
        setTimeout(() => item.classList.add( 'notifications__item_show' ), 0 );
        setHideTimer();

        this.root.appendChild( itemNode );
    }
}