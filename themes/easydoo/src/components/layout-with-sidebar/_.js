document.querySelectorAll( '.js-blog-categories' ).forEach( $btn => {
    $btn.onclick = e => {
        $sidebar = document.querySelector( '.js-sidebar' );
        if( !$sidebar )
            return;

        $sidebar.classList.toggle( 'blog-list-layout__area-sidebar_show' );
    }
});