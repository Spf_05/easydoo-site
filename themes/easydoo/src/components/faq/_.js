document.querySelectorAll( '.js-faq' ).forEach( $root => {
    $root.querySelectorAll( '.js-item' ).forEach( $item => {
        const $label = $item.querySelector( '.js-label' );
        const $content = $item.querySelector( '.js-content' );

        if( !$label || !$content )
            return;

        $label.onclick = e => {
            const cl = 'faq__item_open';
            const isOpen = $item.classList.contains( cl );

            if( isOpen ) {
                $content.removeAttribute( 'style' );
                $item.classList.remove( cl );
            } else {
                $content.style.maxHeight = `${ $content.scrollHeight }px`;
                $item.classList.add( cl );
            }
        };
    });
})