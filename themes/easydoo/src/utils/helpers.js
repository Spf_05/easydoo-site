// https://frontend-stuff.com/blog/debounce-in-javascript/
const debounce = (func, wait, immediate) => {
	let timeout;
	return function executedFunction() {
		const context = this, args = arguments;
		const later = () => {
			timeout = null;
			if ( !immediate ) func.apply( context, args );
		};
		const callNow = immediate && !timeout;
		clearTimeout( timeout );
		timeout = setTimeout( later, wait );
		if (callNow) func.apply(context, args);
	};
};

const throttle = (type, name, obj) => {
    obj = obj || window;
    let running = false;
    const func = () => {
        if (running) { return; }
        running = true;
        requestAnimationFrame(() => {
            let payload = { detail: null };

            if (type == 'resize') {
                payload.detail = window.innerWidth;
            }

            if (type == 'scroll') {
                payload.detail = window.scrollY;
            }

            obj.dispatchEvent(new CustomEvent(name, payload));
            running = false;
        });
    };
    obj.addEventListener(type, func);
};

const optimizedResize = debounce( () => window.dispatchEvent( new CustomEvent( 'optimizedResize', { detail : window.innerWidth } ) ), 250);

const getCssProperty = ( $el, propertyName ) => getComputedStyle( $el ).getPropertyValue( propertyName );

const isInViewport = el => {
    if( !el )
        return;
    const rect = el.getBoundingClientRect();
    const innerHeight = (window.innerHeight || document.documentElement.clientHeight);
    return ( rect.top >= 0 && rect.top <= innerHeight ) || ( rect.bottom >=0 && rect.bottom <= innerHeight );
}

const initInView = ( $el, fn ) => {
	const scrollHandler = (e) => {
		if( !isInViewport( $el ) )
			return;
	
		window.removeEventListener('DOMContentLoaded', scrollHandler, false);
		window.removeEventListener('load', scrollHandler, false);
		window.removeEventListener('scroll', scrollHandler, false);
		window.removeEventListener('resize', scrollHandler, false);

		fn();
	}
	
	window.addEventListener('DOMContentLoaded', scrollHandler, false);
	window.addEventListener('load', scrollHandler, false);
	window.addEventListener('scroll', scrollHandler, false);
	window.addEventListener('resize', scrollHandler, false);
}

const updateVh = () => {
	const vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty( '--vh', `${ vh }px` );
}

export {
	debounce,
	optimizedResize,
	getCssProperty,
	isInViewport,
	initInView,
	updateVh,
    throttle
}