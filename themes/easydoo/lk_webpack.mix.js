const { sassConfig } = require('./build/sass-options.js');
const mix = require('laravel-mix');
const glob = require('glob');
const nodeFs = require('fs');
const nodePath = require('path');
const config = require('./build/config')
const webpack = require('webpack');

mix.alias({
	'@'         : nodePath.join(__dirname, 'cp'),
	// AjaxFrameworkAssets : nodePath.resolve( __dirname, '..', '..', 'modules', 'system', 'assets' ),
	// Config              : nodePath.resolve( __dirname, 'config_build' ),
	// Utils               : nodePath.resolve( __dirname, 'src', 'utils' ),
	// Components          : nodePath.resolve( __dirname, 'src', 'components' ),
});

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules(?!\/foundation-sites)|bower_components/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: Config.babel()
                    }
                ]
            }
        ]
    },
	plugins: [
	],
	optimization: {
	},
	output: {
		publicPath: '/themes/easydoo/assets/cp/',
	}
});

let themePath = 'themes/easydoo/assets/';

mix.options({
    processCssUrls: false,
    publicPath: '/themes/easydoo/assets/cp/',
    fileLoaderDirs: {
        images: '/images',
    }
});

mix.copy('cp/assets/img', 'assets/cp/img');

mix
    .setPublicPath('assets/cp')
    .js('cp/main.js', 'js')
    .sass('cp/assets/sass/main.scss', 'css')

    .vue({
        extractStyles : 'css/app.css',
        version: 2
    });

    if (mix.inProduction()) {
        mix.version();
    }

    mix.browserSync({
        proxy	: process.env.APP_URL,
        host	: process.env.APP_URL,
        browser	: 'google chrome',
        notify	: false,
        files	: [
            "./assets/css/*.css",
            "./**/*.htm",
            "./assets/js/*.js"
        ]
    });