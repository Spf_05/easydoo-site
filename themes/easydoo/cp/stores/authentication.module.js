import { userService } from '@/services';
import router from '@/router.js';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
    ? { status: { loggedIn: true }, user }
    : { status: {}, user: null };

export const authentication = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ dispatch, commit }, { phone, code }) {
            commit('loginRequest', { phone });
            userService.login(phone, code)
                .then(
                    user => {
                        commit('loginSuccess', user);
                        router.push({ name : 'dashboard' });
                    },
                    error => {
                        console.log('fail', error );
                        commit('loginFailure', error);
                        // dispatch('alert/error', error, { root: true });
                    }
                );
        },
        logout({ commit }) {
            userService.logout();
            commit('logout');
            router.push({ name : 'login' });
        }
    },
    mutations: {
        loginRequest(state, user) {
            state.status = { loggingIn: true };
            state.user = user;
        },
        loginSuccess(state, user) {
            state.status = { loggedIn: true };
            state.user = user;
        },
        loginFailure(state) {
            state.status = {};
            state.user = null;
        },
        logout(state) {
            state.status = {};
            state.user = null;
        }
    }
}