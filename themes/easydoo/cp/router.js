import Vue from 'vue'
import VueRouter from 'vue-router';
// import { store } from '@/stores/index.js';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import(/* webpackChunkName: 'login' */ './pages/public/Login')
    }, {
        path: '/dashboard',
        name: 'dashboard',
        meta: { auth: true },
        component: () => import(/* webpackChunkName: 'dashboard' */ './pages/account/Dashboard')
    }, {
        path: '/about',
        name: 'about',
        meta: { auth: true },
        component: () => import(/* webpackChunkName: 'about' */ './pages/account/About')
    }, {
        path: '/profile',
        name: 'profile',
        meta: { auth: true },
        component: () => import(/* webpackChunkName: 'profile' */ './pages/account/Profile')
    },
    {
        path: '*',
        name: '404',
        component: () => import(/* webpackChunkName: '404' */ './pages/public/404')
    }
]

const router = new VueRouter({
    base: "/account",
    linkActiveClass: 'active',
    routes,
    mode: 'history'
});

router.beforeEach((to, from, next) => {
    const loggedIn = localStorage.getItem('user');
    // if( this.$store.state.authentication.status.loggedIn === true )

    if( to.name == 'login' && loggedIn ) {
        return next({ name: 'dashboard' })
    }

    if (to.meta.auth === true && !loggedIn ) {
        return next({ name: 'login' })
    }

    next()
});

export default router