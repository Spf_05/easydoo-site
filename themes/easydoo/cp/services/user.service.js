// import config from 'config';
import { authHeader } from '@/helpers';

export const userService = {
    login,
    logout,
    getCode
};

function getCode( phone ) {
    const requestOptions = {
        method  : 'POST',
        headers : { 'Content-Type': 'application/json' },
        body    : JSON.stringify({ phone })
    };

    const endpoint = `/api/auth/get-code`;

    return new Promise( (resolve, reject) => {
        try {
            fetch(endpoint, requestOptions)
                .then(res => res.json() )
                .then(data => {
                    if( data.status === false ) {
                        reject( data.message )
                    }

                    resolve( data.data );
                } )
                .catch( err => reject( err.message ));
        } catch ( err ) {
            reject( err.message );
        }
    });
}

function login( phone, code ) {
    const requestOptions = {
        method  : 'POST',
        headers : { 'Content-Type': 'application/json' },
        body    : JSON.stringify({ phone, code })
    };

    const endpoint = `/api/auth/login`;

    return fetch(endpoint, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
            }

            return user;
        });
}

function logout() {
    localStorage.removeItem('user');
}

// function getAll() {
//     const requestOptions = {
//         method: 'GET',
//         headers: authHeader()
//     };

//     return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
// }

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.status === false) || response.statusText;
            return Promise.reject(error);
        }

        return data.data;
    });
}