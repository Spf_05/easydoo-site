import Vue from 'vue'

const data = [
    {
        id: 'profile',
        access: ['registered', 'candidate', 'expert'],
        icon: 'iconsminds-profile',
        label: 'Аккаунт',
        to: '/account/profile'
    },
    {
        id: 'courses',
        access: ['registered'],
        icon: 'iconsminds-box-with-folders',
        label: 'Программа',
        to: '/account/courses'
    },
    {
        id: 'experts',
        access: ['registered'],
        icon: 'iconsminds-male-female',
        label: 'Эксперты',
        to: '/account/experts'
    },
    {
        id: 'clients',
        access: ['expert'],
        icon: 'iconsminds-male-female',
        label: 'Клиенты',
        to: '/account/clients'
    },
    {
        id: 'exercises',
        access: ['candidate', 'expert'],
        icon: 'iconsminds-aerobics',
        label: 'Упражнения',
        to: '/account/exercises'
    },
    {
        id: 'messenger',
        access: ['registered', 'candidate', 'expert'],
        icon: 'iconsminds-speach-bubbles',
        label: 'Сообщения',
        to: '/account/messenger'
    },
];

const v = new Vue();
if ( v.$auth.user().groups.includes( 'expert' ) && v.$auth.user().groups.includes( 'tester' ) ) {
    data.push({
        id: 'poll',
        access: ['registered', 'expert'],
        icon: 'simple-icon-question',
        label: 'Опросы',
        to: '/account/polls'
    })
}

export default data;
