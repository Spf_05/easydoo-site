const data = [
    {
        id: 'home',
        assets: 'all',
        icon: 'iconsminds-home',
        label: 'Главная',
        to: '/'
    },
    {
        id: 'experts',
        assets: 'onlyAuth',
        icon: 'iconsminds-stethoscope',
        label: 'Эксперты',
        to: '/experts'
    },
    {
        id: 'blog',
        assets: 'all',
        icon: 'iconsminds-newspaper',
        label: 'Блог',
        to: '/blog'
    }
];
export default data;
