export const defaultMenuType = 'menu-sub-hidden' // 'menu-default', 'menu-sub-hidden', 'menu-hidden'
export const searchPath = '#'
export const subHiddenBreakpoint = 1440
export const menuHiddenBreakpoint = 768
