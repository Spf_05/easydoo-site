import Vue from 'vue';
import App from './App.vue';

import Breadcrumb from './components/Common/Breadcrumb';

import router from './router';
import './bootstrap';
import { store } from '@/stores';
import Colxx from './components/Common/Colxx';
// import './assets/sass/main.scss';

Vue.router = router;

Vue.component('breadcrumb', Breadcrumb);
Vue.component('b-colxx', Colxx);

new Vue({
    router,
    store,
    render: h => h( App )
}).$mount('#app');