<?php namespace Ed\Blog;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
    public function registerComponents(){}
    public function registerSettings(){}
    public $require = ['ReaZzon.Editor'];
}
