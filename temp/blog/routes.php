<?php

// use Ed\Blog\Models\Post;
// use Illuminate\Http\JsonResponse;
use October\Rain\Support\Collection;

Route::post( '/api/relation-upload', [function(){
	$sessionKey = post('sessionKey');
	$relationName = 'images';
	$model = new \Ed\Blog\Models\Post;
	$relationDefinition = $model->getRelationDefinition( $relationName );
	$fileModel = $relationDefinition[0];
	$isPublic = array_get($relationDefinition, 'public', true);

	$file = new $fileModel;
	$file->data = Input::file('file');
	$file->is_public = $isPublic;
	$file->save();

	$model->{ $relationName }()->add( $file, $sessionKey );

	trace_log( $sessionKey );
	// $file = $this->decorateFileAttributes($file);

	return Response::json($file, 200);
}])->middleware( 'web' );

Route::post( '/api/get-relation', [function(){
	$sessionKey = post('sessionKey', null );
	$modelId = post('modelId', null );
	$relationName = 'images';
	$model = new \Ed\Blog\Models\Post;

	if ( $modelId === null ) {
		$list = $model
					->{ $relationName }()
					->withDeferred($sessionKey)
					->orderBy('id', 'desc')
					->get();
	} else {
		$defferedItems = $model
					->{ $relationName }()
					->withDeferred($sessionKey)
					->orderBy('id', 'desc')
					->get();

		$res = \Ed\Blog\Models\Post::whereId($modelId)
									->with( $relationName )
									->first();

		$list = $defferedItems->merge( $res->images );
	}

	return Response::json( $list, 200);
}])->middleware( 'web' );


// public function getFileList()
// {
// 	if (!is_string($this->attribute)) {
// 		throw new ApplicationException(sprintf('Attribute name must be a string, %s was passed.', gettype($this->attribute)));
// 	}

// 	/*
// 		* Use deferred bindings
// 		*/
// 	if ($sessionKey = $this->getSessionKey()) {
// 		$list = $deferredQuery = $this->model
// 			->{$this->attribute}()
// 			->withDeferred($sessionKey)
// 			->orderBy('id', 'desc')
// 			->get();
// 	}
// 	else {
// 		$list = $this->model
// 			->{$this->attribute}()
// 			->orderBy('id', 'desc')
// 			->get();
// 	}

// 	if (!$list) {
// 		$list = new Collection;
// 	}

// 	/*
// 		* Decorate each file with thumb
// 		*/
// 	$list->each(function ($file) {
// 		$this->decorateFileAttributes($file);
// 	});

// 	return $list;
// }


// public function onRemoveAttachment() {
// 	if (!$fileId = post('file_id')) {
// 		return;
// 	}

// 	/*
// 	 * Use deferred bindings
// 	 */
// 	if ($sessionKey = $this->getSessionKey()) {
// 		$file = $this->model
// 			->{$this->attribute}()
// 			->withDeferred($sessionKey)
// 			->find($fileId);
// 	}
// 	else {
// 		$file = $this->model
// 			->{$this->attribute}()
// 			->find($fileId);
// 	}

// 	if ($file) {
// 		$this->model->{$this->attribute}()->remove($file, $this->getSessionKey());
// 	}
// }