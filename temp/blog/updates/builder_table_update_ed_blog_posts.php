<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdBlogPosts extends Migration
{
    public function up()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->integer('category_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
}
