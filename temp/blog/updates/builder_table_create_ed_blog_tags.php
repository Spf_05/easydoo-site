<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdBlogTags extends Migration
{
    public function up()
    {
        Schema::create('ed_blog_tags', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_blog_tags');
    }
}
