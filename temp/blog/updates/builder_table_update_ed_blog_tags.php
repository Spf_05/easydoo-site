<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdBlogTags extends Migration
{
    public function up()
    {
        Schema::table('ed_blog_tags', function($table)
        {
            $table->string('slug');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->integer('sort_order');
            $table->text('text')->nullable();
            $table->boolean('is_published')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('ed_blog_tags', function($table)
        {
            $table->dropColumn('slug');
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_keywords');
            $table->dropColumn('sort_order');
            $table->dropColumn('text');
            $table->dropColumn('is_published');
        });
    }
}
