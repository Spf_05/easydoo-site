<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdBlogTags2 extends Migration
{
    public function up()
    {
        Schema::table('ed_blog_tags', function($table)
        {
            $table->integer('sort_order')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ed_blog_tags', function($table)
        {
            $table->integer('sort_order')->default(null)->change();
        });
    }
}
