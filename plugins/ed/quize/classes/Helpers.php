<?php namespace Ed\Quize\Classes;

class Helpers {
	public static function quizesList() {
		return \Ed\Quize\Models\Quize::select( 'id', 'title' )
				->get()
				->keyBy('id')
				->pluck( 'title', 'id', )
				->toArray();
	}
}