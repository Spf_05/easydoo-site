<?php

Route::get( '/api/quize/{id}', [function($id){
    return \Ed\Quize\Models\Quize::whereId($id)->published()->first();
}])->middleware( 'web' );