<?php namespace Ed\Quize\Models;

use Model;

class Answer extends Model {
    use \October\Rain\Database\Traits\Validation;
    public $table = 'ed_quize_answers';
    public $rules = [];
    protected $jsonable = ['data'];

    public $belongsTo = [
        'quize' => \Ed\Quize\Models\Quize::class,
    ];
}
