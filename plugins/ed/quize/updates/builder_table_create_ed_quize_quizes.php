<?php namespace Ed\Quize\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdQuizeQuizes extends Migration
{
    public function up()
    {
        Schema::create('ed_quize_quizes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('slides')->nullable();
            $table->boolean('published')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_quize_quizes');
    }
}
