<?php namespace Ed\Quize\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdQuizeAnswers extends Migration
{
    public function up()
    {
        Schema::create('ed_quize_answers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('quize_id');
            $table->text('data')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_quize_answers');
    }
}
