<?php namespace Ed\Quize\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdQuizeQuizes extends Migration
{
    public function up()
    {
        Schema::table('ed_quize_quizes', function($table)
        {
            $table->string('title');
        });
    }
    
    public function down()
    {
        Schema::table('ed_quize_quizes', function($table)
        {
            $table->dropColumn('title');
        });
    }
}
