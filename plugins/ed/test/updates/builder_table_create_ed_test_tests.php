<?php namespace Ed\Test\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdTestTests extends Migration
{
    public function up()
    {
        Schema::create('ed_test_tests', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_test_tests');
    }
}
