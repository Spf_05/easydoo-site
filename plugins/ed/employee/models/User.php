<?php namespace Ed\Employee\Models;

use Model;
use Cms\Classes\Page;

class User extends Model {
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    public $table = 'ed_employee_users';
    protected $slugs = [ 'slug' => [ 'id', 'name' ] ];
    protected $jsonable = [ 'text' ];
    protected $modelPage = 'employee/user';

    public static $types = [
        '1' => 'Автор программ',
        '2' => 'Соавтор программ',
        '3' => 'Куратор',
    ];

    public $implement = [
        'Diz.EditorJSThemeBlocks.Behaviors.ConvertToHtml'
    ];

    public $rules = [
        'name' => 'required',
    ];

    public $attachOne	= [
        'cover' => 'System\Models\File',
    ];

    public function getTextHtmlAttribute() {
        return $this->convertJsonToHtml($this->text);
    }

    public function getTypeNameAttribute() {
        return self::$types[ $this->type_id ] ?? ' - ';
    }

    public function scopePublished( $query ) {
        return $query
                    ->where( 'is_published', 1 );
    }

    public function scopeOrder( $query ) {
        return $query->orderBy( 'date', 'DESC' );
    }

    public function getUrl( $slug = null ) {
        return Page::url( $this->modelPage, [ 'slug' => $slug ?? $this->slug ] );
    }

    public function beforeSave() {
        if ( $this->slug == '' ) {
            $this->slug = null;
            $this->slugAttributes();
        }
    }

    public function afterCreate() {
        $this->slug	= null;
        $this->slugAttributes();
        $this->save();
    }

    public function getTypeIdOptions() {
        return self::$types;
    }
}
