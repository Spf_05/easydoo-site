<?php namespace Ed\Employee\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdEmployeeUsers extends Migration
{
    public function up()
    {
        Schema::table('ed_employee_users', function($table)
        {
            $table->renameColumn('type', 'type_id');
        });
    }
    
    public function down()
    {
        Schema::table('ed_employee_users', function($table)
        {
            $table->renameColumn('type_id', 'type');
        });
    }
}
