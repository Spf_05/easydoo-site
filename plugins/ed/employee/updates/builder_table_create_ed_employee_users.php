<?php namespace Ed\Employee\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdEmployeeUsers extends Migration
{
    public function up()
    {
        Schema::create('ed_employee_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->boolean('is_published')->default(0);
            $table->integer('sort_order')->default(0);
            $table->string('name');
            $table->string('slug');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->smallInteger('type');
            $table->string('specialization')->nullable();
            $table->text('text')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_employee_users');
    }
}
