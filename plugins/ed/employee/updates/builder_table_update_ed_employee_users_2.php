<?php namespace Ed\Employee\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdEmployeeUsers2 extends Migration
{
    public function up()
    {
        Schema::table('ed_employee_users', function($table)
        {
            $table->text('specialization')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ed_employee_users', function($table)
        {
            $table->string('specialization', 255)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
