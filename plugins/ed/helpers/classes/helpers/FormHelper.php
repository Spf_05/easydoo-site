<?php namespace Ed\Helpers\Classes\Helpers;

class FormHelper {
	public static function vgapOptions() {
		return [
			'none'	=> "- не задан -",
			's'		=> "Маленький",
			'm'		=> "Средний",
			'l'		=> "Большой",
		];
	}
}