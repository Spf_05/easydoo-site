<?php namespace Ed\Helpers\Classes\Helpers;

class Slug {
    public static function getSlugId( string $slug ) {
        $arr	= explode( '-', $slug, 2 );
        return is_numeric( $arr[0] )
            ? [ 'id' => $arr[0], 'slug' => $arr[1] ]
            : false;
    }
}