<?php namespace Ed\Helpers\Classes\Events;

use Event;
use Art\Form\Models\Item as itemModel;
use Art\Payment\Classes\PaymentStatus;

class TelegramNotif {
	public function subscribe() {
		Event::listen( 'art.form.success', function ( $item ) {
            $text = [];

            $text[] = 'Имя: ' . $item->name;

            if( $item->data AND array_key_exists('type', $item->data ) ) {
                $text[] = 'Тип: ' . $item->data['type'];
            }

            if( $item->contacts AND array_key_exists('phone', $item->contacts ) ) {
                $text[] = 'Телефон: ' . $item->contacts[ 'phone' ];
            }

            if( $item->contacts AND array_key_exists('mail', $item->contacts ) ) {
                $text[] = 'E-mail: ' . $item->contacts[ 'mail' ];
            }

			\Diz\Telegram\Models\Telegram::send([
				'title'	=> 'Сообщение с сайта "' . itemModel::$types[ $item->type ] . '"',
				'text'	=> implode("\r\n", $text),
				'group'	=> 'admins'
			]);
		});

		Event::listen( 'art.payment.update', function ( $model ) {
			\Diz\Telegram\Models\Telegram::send([
				'title'	=> 'Статус платежа изменен №' . $model->id,
				'text'	=> 'Статус: ' . PaymentStatus::$codeStr[ $model->status ] . "\r\n" . 'Сумма: ' . $model->amount / 100 . "\r\n" . 'Комментарий: ' . $model->comment,
				'group'	=> 'admins'
			]);
		});

		Event::listen( 'art.payment.create', function ( $model ) {
			\Diz\Telegram\Models\Telegram::send([
				'title'	=> 'Создан платеж №' . $model->id,
				'text'	=> 'Сумма: ' . $model->amount / 100 . "\r\n" . 'Комментарий: ' . $model->comment,
				'group'	=> 'admins'
			]);
		});

		Event::listen( 'art.big_brother.event', function ( $model ) {
			\Diz\Telegram\Models\Telegram::send([
				'title'	=> 'Событие BigBrother',
				'text'	=> \Art\BigBrotherSnitch\Classes\EventsTitles::$ids[ $model->event_id ],
				'group'	=> 'admins'
			]);
		});

		Event::listen( 'art.metrica_report', function () {
			$data	= \Diz\Metrika\Models\Settings::getData();

			if ( $data ) {
				$d = '';
				foreach ( $data[ 'detal' ] as $item ) {
					$d .= '<strong>' . $item[ 'name' ] . '</strong>: ' . $item[ 'users' ] . '|' . $item[ 'views' ] . "\n";
				}

				\Diz\Telegram\Models\Telegram::send([
					'title'	=> '📊 Статистика ' . $data[ 'site' ] . ' за ' . date( 'd-m-Y' ),
					'text'	=> 'Посетители: ' . $data[ 'users' ] . "\r\n" . 'Просмотры: ' . $data[ 'views' ] . "\n---\n" . $d,
					'group'	=> 'admins',
				]);
			}
		});
	}
}
?>
