<?php namespace Ed\Helpers;

use Backend;
use System\Classes\PluginBase;
use Ed\Helpers\Classes\Events\TelegramNotif;
use Event;

class Plugin extends PluginBase {
    public function pluginDetails() {
        return [
            'name'        => 'Helpers',
            'description' => 'No description provided yet...',
            'author'      => 'Ed',
            'icon'        => 'icon-leaf'
        ];
    }

	function boot() {
		$this->addEventListeners();
	}

	protected function addEventListeners() {
		Event::subscribe(TelegramNotif::class);
	}

    public function registerMarkupTags() {
        return [
            'functions' => [
                'tagSelect' => function( $str, $tag = 'span', $class = '' ){
                    if( $class != '' )
                        $class = ' class="' . $class . '"';
                    $pattern = '/~(.*)~/i';
                    $replacement = '<' . $tag . $class . '>$1</' . $tag . '>';
                    return preg_replace($pattern, $replacement, $str);
                },
            ],
            'filters' => [
                'vgap' => function( $option ){
                    return 'page_mb-' . $option;
                },
                'price' => function( $number ){
                    return number_format( (int)$number, 0, '.', ' ') . ' ₽';
                },
            ]
        ];
    }
}
