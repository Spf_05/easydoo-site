<?php namespace Ed\Employeeprogram\Classes\Events;

use Ed\Programs\Models\Item as programModel;
use Ed\Programs\Controllers\Items as itemController;
use Ed\Employee\Models\User as userModel;
use Yaml;
use File;

class Program {
    public function subscribe() {
        programModel::extend( function( $model ) {
            $model->belongsToMany[ 'employees' ]	= [
                userModel::class,
                'table'	=> 'ed_employeeprogram_relations',
                'key' => 'user_id',
                'otherKey' => 'program_id'
            ];
        });

        itemController::extend(function($controller) {
            if (!$controller->isClassExtendedWith('Backend.Behaviors.RelationController')) {
                $controller->implement[] = 'Backend.Behaviors.RelationController';
            }

            $myConfigPath = '~/plugins/ed/employeeprogram/config/program/relation.yaml';

            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig', $myConfigPath);
            } else {
                $config = $controller->makeConfig($controller->relationConfig);
                $myConfig = $controller->makeConfig($myConfigPath);
                $controller->relationConfig = (object) array_merge((array) $config, (array) $myConfig);
            }
        });

        itemController::extendFormFields(function($form, $model, $context) {
            if ( !$model instanceof programModel )
                return;

            if ( !$form->isNested ) {
                $configFile = plugins_path('ed/employeeprogram/config/program/field.yaml');
                $config = Yaml::parse(File::get($configFile));
                $form->addTabFields($config);
            }
        });
    }
}
