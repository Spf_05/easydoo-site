<?php namespace Ed\Employeeprogram\Classes\Events;

use Ed\Employee\Models\User as userModel;
use Ed\Employee\Controllers\Users as usersController;
use Ed\Programs\Models\Item as programModel;
use Yaml;
use File;

class Employee {
    public function subscribe() {
        userModel::extend( function( $model ) {
            $model->belongsToMany[ 'programs' ]	= [
                programModel::class,
                'table'	=> 'ed_employeeprogram_relations',
                'key' => 'user_id',
                'otherKey' => 'program_id'
            ];
        });

        usersController::extend(function($controller) {
            if (!$controller->isClassExtendedWith('Backend.Behaviors.RelationController')) {
                $controller->implement[] = 'Backend.Behaviors.RelationController';
            }

            $myConfigPath = '~/plugins/ed/employeeprogram/config/employee/relation.yaml';

            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig', $myConfigPath);
            } else {
                $config = $controller->makeConfig($controller->relationConfig);
                $myConfig = $controller->makeConfig($myConfigPath);
                $controller->relationConfig = (object) array_merge((array) $config, (array) $myConfig);
            }
        });

        usersController::extendFormFields(function($form, $model, $context) {
            if ( !$model instanceof userModel )
                return;

            if ( !$form->isNested ) {
                $configFile = plugins_path('ed/employeeprogram/config/employee/field.yaml');
                $config = Yaml::parse(File::get($configFile));
                $form->addTabFields($config);
            }
        });
    }
}
