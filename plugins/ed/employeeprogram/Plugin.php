<?php namespace Ed\Employeeprogram;

use Event;
use System\Classes\PluginBase;
use Ed\Employeeprogram\Classes\Events\Employee;
use Ed\Employeeprogram\Classes\Events\Program;

class Plugin extends PluginBase {
    public function registerComponents(){}
    public function registerSettings(){}

    public function boot() {
        $this->addEventListeners();
    }

    protected function addEventListeners() {
        Event::subscribe(Employee::class);
        Event::subscribe(Program::class);
    }
}