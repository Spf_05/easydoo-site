<?php namespace Ed\Employeeprogram\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdEmployeeprogramRelations extends Migration
{
    public function up()
    {
        Schema::table('ed_employeeprogram_relations', function($table)
        {
            $table->dropPrimary(['emploee_id','program_id']);
            $table->renameColumn('emploee_id', 'user_id');
            $table->primary(['user_id','program_id']);
        });
    }
    
    public function down()
    {
        Schema::table('ed_employeeprogram_relations', function($table)
        {
            $table->dropPrimary(['user_id','program_id']);
            $table->renameColumn('user_id', 'emploee_id');
            $table->primary(['emploee_id','program_id']);
        });
    }
}
