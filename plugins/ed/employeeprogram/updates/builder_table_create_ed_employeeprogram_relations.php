<?php namespace Ed\Employeeprogram\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdEmployeeprogramRelations extends Migration
{
    public function up()
    {
        Schema::create('ed_employeeprogram_relations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('emploee_id');
            $table->integer('program_id');
            $table->primary(['emploee_id','program_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_employeeprogram_relations');
    }
}
