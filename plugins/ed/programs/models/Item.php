<?php namespace Ed\Programs\Models;

use Model;
use Cms\Classes\Page;

class Item extends Model {
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    protected $slugs = [ 'slug' => [ 'title' ] ];
    public $table = 'ed_programs_items';
    protected $jsonable = [ 'text' ];
    protected $modelPage = 'programs/item';

    public static $directions = [
        '0' => 'Все направления',
        '1' => 'Долголетие',
        '2' => 'Легкое дыхание',
        '3' => 'Прочные суставы',
        '4' => 'Здоровое сердце',
        '5' => 'Ясность ума',
        '6' => 'Обучение',
        '7' => 'Эластичные связки',
        '8' => 'Здоровая спина',
    ];

    public static $types = [
        '0' => 'Любой',
        '1' => 'Базовые',
        '2' => 'Индивидуальные',
        '3' => 'Тренировки',
    ];

    public static $levels = [
        '0' => 'Любой',
        '1' => 'Начальный',
        '2' => 'Средний',
        '3' => 'Продвинутый',
    ];

    public $rules = [
        'title' => 'required',
    ];

    public $implement = [
        'Diz.EditorJSThemeBlocks.Behaviors.ConvertToHtml'
    ];

    public $belongsTo = [
        'category' => [
            \Ed\Programs\Models\Category::class,
        ],
    ];

    public $attachOne	= [
        'icon' => 'System\Models\File',
    ];

    public function getSeoSlugAttribute() {
        return $this->id . '-' . $this->slug;
    }

    public function getTextHtmlAttribute() {
        return $this->convertJsonToHtml($this->text);
    }

    public function getDirectionNameAttribute() {
        return self::$directions[ $this->direction_id ] ?? ' - ';
    }

    public function getTypeNameAttribute() {
        return self::$types[ $this->type_id ] ?? ' - ';
    }

    public function getLevelNameAttribute() {
        return self::$levels[ $this->level_id ] ?? ' - ';
    }

    public function scopePublished( $query ) {
        return $query->where( 'is_published', 1 );
    }

    public function scopeOrder( $query ) {
        return $query->orderBy( 'date', 'DESC' );
    }

    public function getUrl( $slug = null ) {
        return Page::url( $this->modelPage, [ 'slug' => $slug ?? $this->seo_slug ] );
    }

    public function beforeSave() {
        if ( $this->slug == '' ) {
            $this->slug = null;
            $this->slugAttributes();
        }
    }

    private function getArrayWithoutZeroKey( $key ) {
        return $key !== 0;
    }

    public function getDirectionIdOptions() {
        return array_filter( self::$directions, [ $this, 'getArrayWithoutZeroKey' ], ARRAY_FILTER_USE_KEY );
    }

    public function getTypeIdOptions() {
        return array_filter( self::$types, [ $this, 'getArrayWithoutZeroKey' ], ARRAY_FILTER_USE_KEY );
    }

    public function getLevelIdOptions() {
        return array_filter( self::$levels, [ $this, 'getArrayWithoutZeroKey' ], ARRAY_FILTER_USE_KEY );
    }
}