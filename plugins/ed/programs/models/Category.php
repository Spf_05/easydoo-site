<?php namespace Ed\Programs\Models;

use Model;
use Cms\Classes\Page;

class Category extends Model {
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    public $timestamps = false;
    protected $slugs = [ 'slug' => [ 'title' ] ];
    public $table = 'ed_programs_categories';
    public $rules = [
        'title' => 'required',
    ];
    protected $modelPage = 'blog/category';

    public $hasMany = [
        'items' => [
            \Ed\Programs\Models\Item::class,
        ],
    ];

    public function getSlugAttribute() {
        return $this->id . '-' . $this->slug;
    }

    public function scopePublished( $query ) {
        return $query->where( 'is_published', 1 );
    }

    public function scopeOrder( $query ) {
        return $query->orderBy( 'date', 'DESC' );
    }

    public function getUrl( $slug = null ) {
        return Page::url( $this->modelPage, [ 'slug' => $slug ?? $this->seo_slug ] );
    }

    public function beforeSave() {
        if ( $this->slug == '' ) {
            $this->slug = null;
            $this->slugAttributes();
        }
    }
}
