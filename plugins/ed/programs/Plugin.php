<?php namespace Ed\Programs;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
    public function registerComponents() {
        return [
            'Ed\Programs\Components\ProgramsList' => 'ProgramsList',
        ];
    }

    public function registerSettings(){}
}
