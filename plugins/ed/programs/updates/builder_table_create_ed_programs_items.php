<?php namespace Ed\Programs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdProgramsItems extends Migration
{
    public function up()
    {
        Schema::create('ed_programs_items', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->text('anons')->nullable();
            $table->text('text')->nullable();
            $table->string('time')->nullable();
            $table->integer('category_id');
            $table->integer('sort_order')->default(0);
            $table->boolean('is_published')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_programs_items');
    }
}
