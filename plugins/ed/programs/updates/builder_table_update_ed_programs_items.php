<?php namespace Ed\Programs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdProgramsItems extends Migration
{
    public function up()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->integer('direction_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('level_id')->nullable();
            $table->boolean('curator')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->dropColumn('direction_id');
            $table->dropColumn('type_id');
            $table->dropColumn('level_id');
            $table->dropColumn('curator');
        });
    }
}
