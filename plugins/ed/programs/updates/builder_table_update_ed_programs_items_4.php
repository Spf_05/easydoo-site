<?php namespace Ed\Programs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdProgramsItems4 extends Migration
{
    public function up()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->longText('text')->change();
        });
    }
    
    public function down()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->text('text')->change();
        });
    }
}
