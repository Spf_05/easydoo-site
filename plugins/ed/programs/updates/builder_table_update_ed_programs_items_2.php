<?php namespace Ed\Programs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdProgramsItems2 extends Migration
{
    public function up()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->boolean('by_subscription')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->dropColumn('by_subscription');
        });
    }
}
