<?php namespace Ed\Programs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdProgramsItems3 extends Migration
{
    public function up()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->integer('category_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ed_programs_items', function($table)
        {
            $table->integer('category_id')->nullable(false)->change();
        });
    }
}
