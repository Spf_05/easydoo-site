<?php namespace Ed\Programs\Components;

use Cms\Classes\ComponentBase;
use Input;
use ED\Programs\Models\Item as itemModel;
use ED\Programs\Models\Category as categoryModel;

class ProgramsList extends ComponentBase {
    protected $filters = [];
    protected $programs;
    protected $pageNumber = 1;
    protected $perPage = 20;

    public function onRun() {
        $this->pageNumber = (int)Input::get( 'page', 1 );
        $this->prepareVars();
        $this->getPrograms();
    }

    protected $filtersFields = [
        'direction' => 'direction_id',
        'type' => 'type_id',
        'level' => 'level_id',
        'curator' => 'curator',
        'category' => 'category_id',
    ];

    public function onFilter() {
        $this->prepareVars();
        $this->getPrograms();
        
        $this->page[ 'programs' ] = $this->programs;
        $this->page[ 'url' ] = $this->getUrlParams();

        return [
            'url' => $this->getUrlParams(),
        ];
    }

    protected function getUrlParams() {
        $params = $this->filters;

        if( $this->pageNumber > 1 )
            $params[ 'page' ] = $this->pageNumber;

        return http_build_query( $params );
    }

    protected function prepareVars() {
        $filters = [];

        if( Input::get( 'direction', false ) )
            $filters[ 'direction' ] = (int)Input::get( 'direction' );

        if( Input::get( 'type', false ) )
            $filters[ 'type' ] = (int)Input::get( 'type' );

        if( Input::get( 'level', false ) )
            $filters[ 'level' ] = (int)Input::get( 'level' );

        if( Input::get( 'curator', false ) )
            $filters[ 'curator' ] = true;

        if( Input::get( 'category', false ) )
            $filters[ 'category' ] = (int)Input::get( 'category' );

        $this->filters = $filters;
    }

    protected function getPrograms() {
        $q = itemModel::published()
                            ->with([
                                'category'  => function( $q ) {
                                    $q->published();
                                }
                            ]);

        if( count( $this->filters ) ) {
            $filter = [];

            foreach( $this->filters as $filter_key => $filter_val ) {
                $filter[ $this->filtersFields[ $filter_key ] ] = $filter_val;
            }

            $q->where( $filter );
        }


        $this->programs = $q->paginate( $this->perPage, $this->pageNumber );
    }

    public function programs() {
        return $this->programs;
    }

    public function filtersList() {
        return [
            'categories'    => categoryModel::published()->get(),
            'directions'    => itemModel::$directions,
            'types'         => itemModel::$types,
            'levels'        => itemModel::$levels,
        ];
    }

    public function filterValues() {
        $this->prepareVars();
        return $this->filters;
    }

    public function componentDetails() {
        return [
            'name'        => 'ed.programs::lang.components.programs_list.name',
            'description' => 'ed.programs::lang.components.programs_list.description'
        ];
    }

    public function defineProperties() {
        return [
            // 'categorySlug' => [
            //     'title'       => 'octolab.shop::lang.components.products_list.properties.category_slug.title',
            //     'description' => 'octolab.shop::lang.components.products_list.properties.category_slug.description',
            //     'type'        => 'string',
            //     'default'     => '{{ :category }}'
            // ],
            // 'pageNumber' => [
            //     'title'       => 'octolab.shop::lang.components.products_list.properties.page_number.title',
            //     'description' => 'octolab.shop::lang.components.products_list.properties.page_number.description',
            //     'type'        => 'string',
            //     'default'     => '{{ :page }}',
            // ],
            // 'productsPerPage' => [
            //     'title'             => 'octolab.shop::lang.components.products_list.properties.products_per_page.title',
            //     'type'              => 'string',
            //     'validationPattern' => '^[0-9]+$',
            //     'validationMessage' => 'octolab.shop::lang.components.products_list.properties.products_per_page.validation_message',
            //     'default'           => '10',
            // ],
            // 'productPage' => [
            //     'title'   => 'octolab.shop::lang.components.products_list.properties.product_page.title',
            //     'type'    => 'dropdown',
            //     'default' => 'products/item',
            // ],
        ];
    }
}