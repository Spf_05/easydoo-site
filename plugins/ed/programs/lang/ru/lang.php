<?php return [
    'plugin' => [
        'name' => 'Programs',
        'description' => ''
    ],
    'components' => [
        'programs_list' => [
            'name' => 'Programs list',
            'description' => '',
        ]
    ],
    'form' => [
        'fields' => [
            'curator' => 'Куратор',
            'direction' => 'Направление',
            'type_program' => 'Тип программы',
            'level' => 'Уровень сложности',
            'time' => 'Время',
            'time' => 'Время',
            'by_subscription' => 'По подписке',
        ]
    ]
];