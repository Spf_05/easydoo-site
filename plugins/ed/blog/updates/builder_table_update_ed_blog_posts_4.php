<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdBlogPosts4 extends Migration
{
    public function up()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->integer('time_to_read')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->dropColumn('time_to_read');
        });
    }
}
