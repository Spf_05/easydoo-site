<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdBlogCategories extends Migration
{
    public function up()
    {
        Schema::table('ed_blog_categories', function($table)
        {
            $table->text('text')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ed_blog_categories', function($table)
        {
            $table->text('text')->nullable(false)->change();
        });
    }
}
