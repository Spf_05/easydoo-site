<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdBlogPosts2 extends Migration
{
    public function up()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->integer('views')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->dropColumn('views');
        });
    }
}
