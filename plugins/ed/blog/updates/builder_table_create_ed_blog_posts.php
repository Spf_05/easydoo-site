<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdBlogPosts extends Migration
{
    public function up()
    {
        Schema::create('ed_blog_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->boolean('is_published')->default(0);
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->integer('sort_order')->default(0);
            $table->text('anons')->nullable();
            $table->text('text')->nullable();
            $table->dateTime('date');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_blog_posts');
    }
}
