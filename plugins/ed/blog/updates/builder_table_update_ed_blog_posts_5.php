<?php namespace Ed\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdBlogPosts5 extends Migration
{
    public function up()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->text('blocks_data')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ed_blog_posts', function($table)
        {
            $table->dropColumn('blocks_data');
        });
    }
}
