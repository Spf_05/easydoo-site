<?php namespace Ed\Blog\Models;

use Model;
use Cms\Classes\Page;

class Tag extends Model {
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    public $timestamps = false;
    protected $slugs = [ 'slug' => [ 'name' ] ];
    public $table = 'ed_blog_tags';
    protected $jsonable = [ 'text' ];
    protected $modelPage = 'blog/tag';

    public $rules = [
        'name' => 'required',
    ];

    public $belongsToMany = [
        'posts' => [
            \Ed\Blog\Models\Post::class,
            'table'		=> 'ed_blog_posts_tags',
        ],
    ];

    public function getSeoSlugAttribute() {
        return $this->id . '-' . $this->slug;
    }

    public function scopePublished( $query ) {
        return $query->where( 'is_published', 1 );
    }

    public function scopeOrder( $query ) {
        return $query->orderBy( 'date', 'DESC' );
    }

    public function getUrl( $slug = null ) {
        return Page::url( $this->modelPage, [ 'slug' => $slug ?? $this->seo_slug ] );
    }

    public function beforeSave() {
        if ( $this->slug == '' ) {
            $this->slug = null;
            $this->slugAttributes();
        }
    }
}
