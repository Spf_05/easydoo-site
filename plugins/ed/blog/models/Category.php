<?php namespace Ed\Blog\Models;

use Model;
use Cms\Classes\Page;

class Category extends Model {
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    public $timestamps = false;
    protected $slugs = [ 'slug' => [ 'name' ] ];
    public $table = 'ed_blog_categories';
    protected $jsonable = [ 'text' ];
    protected $modelPage = 'blog/category';

    public $rules = [
        'name' => 'required',
    ];

    public $hasMany = [
        'posts' => [
            \Ed\Blog\Models\Post::class,
        ],
    ];

    public $attachOne	= [
        'icon' => 'System\Models\File',
    ];

    public function getSeoSlugAttribute() {
        return $this->id . '-' . $this->slug;
    }

    public function scopePublished( $query ) {
        return $query->where( 'is_published', 1 );
    }

    public function scopeOrder( $query ) {
        return $query->orderBy( 'date', 'DESC' );
    }

    public function getUrl( $slug = null ) {
        return Page::url( $this->modelPage, [ 'slug' => $slug ?? $this->seo_slug ] );
    }

    public function beforeSave() {
        if ( $this->slug == '' ) {
            $this->slug = null;
            $this->slugAttributes();
        }
    }
}
