<?php namespace Ed\Blog\Models;

use Model;
use Carbon\Carbon;
use Cms\Classes\Page;
use Ed\Helpers\Classes\Helpers\Slug as SlugHelpers;

class Post extends Model {
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Sortable;

    public $table = 'ed_blog_posts';
    protected $slugs = [ 'slug' => [ 'title' ] ];
    protected $jsonable = [ 'text', 'blocks_data' ];
    protected $modelPage = 'blog/post';

    public $implement = [
        'Diz.EditorJSThemeBlocks.Behaviors.ConvertToHtml'
    ];

    public $rules = [
        'title' => 'required',
    ];

    public $belongsTo = [
        'category' => \Ed\Blog\Models\Category::class,
        'author' => \Ed\Blog\Models\Author::class
    ];

    public $belongsToMany = [
        'tags' => [
            \Ed\Blog\Models\Tag::class,
            'table'		=> 'ed_blog_posts_tags',
        ],
    ];

    public $attachOne	= [
        'cover' => 'System\Models\File',
    ];

    public $attachMany	= [
        'images' => 'System\Models\File',
    ];

    public function getSeoSlugAttribute() {
        return $this->id . '-' . $this->slug;
    }

    public function getTextHtmlAttribute() {
        return $this->convertJsonToHtml($this->text);
    }

    public function scopePublished( $query ) {
        return $query
                    ->where( 'is_published', 1 )
                    ->where( 'date', '<=', Carbon::now()->toDateTimeString() );
    }

    public function scopeOrder( $query ) {
        return $query->orderBy( 'date', 'DESC' );
    }

    public function getUrl( $slug = null ) {
        return Page::url( $this->modelPage, [ 'slug' => $slug ?? $this->seo_slug ] );
    }

    public function beforeSave() {
        if ( $this->slug == '' ) {
            $this->slug = null;
            $this->slugAttributes();
        }
    }
}
