<?php namespace Ed\Blog\Models;

use Model;

class Author extends Model {
    use \October\Rain\Database\Traits\Validation;
    public $timestamps = false;
    public $table = 'ed_blog_authors';
    public $rules = [];

    public $attachOne	= [
        'avatar' => 'System\Models\File',
    ];
}
