<?php namespace Ed\Auth\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource {
    public function toArray($request) {
        $arViewName = [];

        if( trim( $this->last_name ) == '' ) {
            $arViewName[] = $this->phone;
        } else {
            $arViewName[] = $this->last_name;
            $arInitials = [];

            if( trim( $this->name ) !== '' )
                $arInitials[] = mb_substr( trim( $this->name ),0,1 ) . '.';

            if( trim( $this->middle_name ) !== '' )
                $arInitials[] = mb_substr( trim( $this->middle_name ), 0,1 ) . '.';

            $arViewName[] = join( '', $arInitials);
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'view_name' => join( ' ', $arViewName ),
            'token' => array_key_exists( 'token', $this->additional ) ? $this->additional['token'] : '',
            'avatar' => $this->avatar ? $this->avatar->getPath() : null,
        ];
    }
}