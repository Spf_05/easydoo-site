<?php namespace Ed\Auth\Classes;

use BizMark\SMSAuth\Services\SmsRequestService;
use Lovata\Buddies\Models\User;

class OTP extends SmsRequestService {
    public function send(string $code, ?User $user = null, ?string $phone = null): int
    {
        if ($user === null && $phone === null) {
            return -1;
        }

        if ($user) {
            $phone = $user->phone;
        }

        if (empty($phone)) {
            return -2;
        }

        try {
            $templateSms = trans(config('bizmark.smsauth.sms.template', ':code'), ['code' => $code]);
            if (config('bizmark.smsauth::auth.debug', false)) {
                trace_log($templateSms);
            } else {
                trace_log('=====' . $templateSms);
                // /** @var \October\Rain\Network\Http $response */
                // $response = app('sms')->send(['message' => $templateSms, 'phones' => $phone]);
                // if (!empty($response->body) && strpos($response->body, 'ERROR') !== false) {
                //     trace_log($response);
                //     return -1;
                // }
            }

            return 1;
        } catch (\Exception $e) {
            trace_log($e);
        }

        return -99;
    }
}