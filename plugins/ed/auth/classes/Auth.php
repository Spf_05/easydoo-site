<?php namespace Ed\Auth\Classes;

use Kharanenka\Helper\Result;
use BizMark\SMSAuth\Classes\Enums\SmsCodeType;
use BizMark\SMSAuth\Classes\Exceptions\CodeExpiredException;
use BizMark\SMSAuth\Models\Code;
use Request;
use Lovata\Buddies\Models\User;
use Ed\Auth\Classes\OTP;
use Ed\Auth\Classes\Resources\UserResource;
use PhpParser\Node\Stmt\TryCatch;

class Auth {
    public function getCode(Request $request) {
        $arCredentials = post();

        request()->validate([
            'phone' => 'required',
        ]);

        /** @var OTP $obOTP */
        $obOTP = app(OTP::class);

        try {
            $sPhoneFormatted = phoneFormatE164($arCredentials['phone']);
        } catch (\Throwable $th) {
            return Result::setFalse()->setMessage('Не верный формат телефона')->get();
        }

        $obUser = User::fromPhone($sPhoneFormatted)->first(['id', 'phone']);

        try {
            $obCode = $obOTP->generate($sPhoneFormatted, $obUser, SmsCodeType::LOGIN());

            $telegramPhones = [
                '+79193151350',
                '+79152297247'
            ];

            if( in_array( $sPhoneFormatted, $telegramPhones ) ) {
                \Diz\Telegram\Models\Telegram::send([
                    'title'	=> 'Код входя для: ' . $sPhoneFormatted,
                    'text'	=> $obCode->code,
                    'group'	=> 'admins'
                ]);
            } else {
                $url    = "https://vp.voicepassword.ru/api/voice-password/send/";
                $apiKey = 'd4978270d1f9b2ebfbdb5bcbdc4a4966';

                $data   = [
                    'number' => $sPhoneFormatted,
                    'security' => [ 'apiKey' => $apiKey ],
                    'flashcall' => [ 'code' => $obCode->code ],
                ];
    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    "Content-Type: application/json",
                    "Authorization: $apiKey"
                ]);
                $outData = curl_exec($ch);
                curl_close($ch);
                trace_log($outData);
            }
        } catch (\ApplicationException $ex) {
            return Result::setFalse()->setMessage($ex->getMessage())->get();
        }

        if ($obOTP->send($obCode->code, $obUser, $sPhoneFormatted) === 1) {
            Result::setTrue()->setData( [ 'time' => env('SMSAUTH_USER_CODE_EXPIRED', 60) ] );
        } else {
            Result::setFalse()->setMessage('Произошла ошибка отправки кода');
        }

        return Result::get();
    }

    public function login(Request $request) {
        $arCredentials = post();

        request()->validate([
            'phone' => 'required',
            'code' => 'required',
        ]);

        /** @var OTP $codeManager */
        $obCodeManager = app(OTP::class);

        try {
            $obCode = $obCodeManager->verification($arCredentials['phone'], $arCredentials['code'], SmsCodeType::LOGIN());
        } catch (CodeExpiredException $ex) {
            return Result::setTrue()->setMessage('Код истек')->get();
        }

        if (!$obCode->user) {
            $obCode = \DB::transaction(function () use ($arCredentials, $obCode): Code {
                $obRegisterUser = User::registerByPhone($arCredentials['phone'], request()->ip());
                $obCode->setUser($obRegisterUser);
                return $obCode;
            });
        }

        $sToken = app('JWTGuard')->login($obCode->user);
        $obApiUserData = (new UserResource( $obCode->user ))
                            ->additional(['token' => $sToken]);

        return Result::setTrue( $obApiUserData )->get();
    }
}