<?php

use Ed\Auth\Classes\Auth;

Route::group([ 'prefix' => '/api/auth/' ], function () {
    Route::post('/get-code', [Auth::class, 'getCode']);
    Route::post('/login', [Auth::class, 'login']);
});

Route::post('/api/test', function () {
    return [ 'status' => 'ok' ];
 })->middleware(\ReaZzon\JWTAuth\Http\Middlewares\ResolveUser::class);