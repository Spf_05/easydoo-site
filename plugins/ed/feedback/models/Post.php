<?php namespace Ed\Feedback\Models;

use Model;

class Post extends Model {
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    public $table = 'ed_feedback_posts';
    public $rules = [];
    protected $jsonable = [ 'text' ];

    public $implement = [
        'Diz.EditorJSThemeBlocks.Behaviors.ConvertToHtml'
    ];

    public $attachOne	= [
        'avatar' => 'System\Models\File',
    ];

    public function getTextHtmlAttribute() {
        return $this->convertJsonToHtml($this->text);
    }

    public function scopePublished( $query ) {
        return $query->where( 'is_published', 1 );
    }

    public function scopeOrder( $query ) {
        return $query->orderBy( 'sort_order', 'DESC' );
    }
}
