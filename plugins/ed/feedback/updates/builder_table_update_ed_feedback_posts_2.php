<?php namespace Ed\Feedback\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdFeedbackPosts2 extends Migration
{
    public function up()
    {
        Schema::table('ed_feedback_posts', function($table)
        {
            $table->integer('sort_order')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('ed_feedback_posts', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
