<?php namespace Ed\Feedback\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdFeedbackPost extends Migration
{
    public function up()
    {
        Schema::create('ed_feedback_post', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->boolean('is_published')->default(0);
            $table->string('name');
            $table->smallInteger('stars')->default(5);
            $table->text('text');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_feedback_post');
    }
}
