<?php namespace Ed\Feedback\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdFeedbackPosts extends Migration
{
    public function up()
    {
        Schema::rename('ed_feedback_post', 'ed_feedback_posts');
    }
    
    public function down()
    {
        Schema::rename('ed_feedback_posts', 'ed_feedback_post');
    }
}
