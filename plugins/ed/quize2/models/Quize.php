<?php namespace Ed\Quize2\Models;

use Model;

class Quize extends Model {
    use \October\Rain\Database\Traits\Validation;
    public $timestamps = false;
    public $table = 'ed_quize2_quizes';
    protected $jsonable = [ 'slides', 'redirects' ];

    public $rules = [
        'title' => 'required',
    ];

    public function scopePublished( $query ) {
        return $query->where( 'published', 1 );
    }

    public function beforeSave() {
        $tempSlides = $this->slides;

        if( gettype( $tempSlides ) == 'NULL' ) {
            return;
        }

        foreach( $tempSlides as $idx => $slide ) {
            if ( !array_key_exists( '_id', $slide ) )
                $tempSlides[ $idx ][ '_id' ] = (string)\Str::uuid();
        }
        $this->slides = $tempSlides;
    }
}
