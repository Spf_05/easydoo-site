<?php namespace Ed\Quize2\Models;

use Model;

class Answer extends Model {
    use \October\Rain\Database\Traits\Validation;
    public $table = 'ed_quize2_answers';
    public $rules = [];
    protected $jsonable = ['data'];

    public $belongsTo = [
        'quize' => \Ed\Quize2\Models\Quize::class,
    ];
}
