<?php namespace Ed\Quize2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdQuize2 extends Migration
{
    public function up()
    {
        Schema::create('ed_quize2_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('quize_id');
            $table->longText('data')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_quize2_');
    }
}
