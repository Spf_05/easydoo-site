<?php namespace Ed\Quize2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdQuize2Answers extends Migration
{
    public function up()
    {
        Schema::rename('ed_quize2_', 'ed_quize2_answers');
    }
    
    public function down()
    {
        Schema::rename('ed_quize2_answers', 'ed_quize2_');
    }
}
