<?php namespace Ed\Quize2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdQuize2Quizes3 extends Migration
{
    public function up()
    {
        Schema::table('ed_quize2_quizes', function($table)
        {
            $table->string('general_redirect', 10)->nullable(false)->unsigned(false)->default(null)->comment(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ed_quize2_quizes', function($table)
        {
            $table->smallInteger('general_redirect')->nullable(false)->unsigned(false)->default(null)->comment(null)->change();
        });
    }
}
