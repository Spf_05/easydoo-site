<?php namespace Ed\Quize2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdQuize2Quizes2 extends Migration
{
    public function up()
    {
        Schema::table('ed_quize2_quizes', function($table)
        {
            $table->smallInteger('general_redirect');
        });
    }
    
    public function down()
    {
        Schema::table('ed_quize2_quizes', function($table)
        {
            $table->dropColumn('general_redirect');
        });
    }
}
