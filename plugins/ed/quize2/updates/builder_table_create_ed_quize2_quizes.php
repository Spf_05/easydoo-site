<?php namespace Ed\Quize2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdQuize2Quizes extends Migration
{
    public function up()
    {
        Schema::create('ed_quize2_quizes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->longText('slides')->nullable();
            $table->boolean('published');
            $table->string('title');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ed_quize2_quizes');
    }
}
