<?php namespace Ed\Quize2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdQuize2Answers2 extends Migration
{
    public function up()
    {
        Schema::table('ed_quize2_answers', function($table)
        {
            $table->string('title');
        });
    }
    
    public function down()
    {
        Schema::table('ed_quize2_answers', function($table)
        {
            $table->dropColumn('title');
        });
    }
}
