<?php namespace Ed\Quize2\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Quizes extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ed.Quize2', 'main-menu-item', 'side-menu-item2');
    }
}
