<?php

Route::get( '/api/quize2/{id}', [function($id){
    return \Ed\Quize2\Models\Quize::whereId($id)->published()->first();
}])->middleware( 'web' );