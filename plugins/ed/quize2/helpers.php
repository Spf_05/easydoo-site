<?php
if (!function_exists('phoneFormatE164')) {
    /**
     * Хэлпер для прямого приведения телефона к формату e164
     *
     * @param  string $phone
     * @return string
     */
    function phoneFormatE164(string $phone): string
    {
        return phone($phone, config('ed.quize2::phones.phone_countries_format'))->formatE164();
    }
}

if (!function_exists('plural')) {
    /**
     * @param  int $iNumber
     * @param  array $arTitles
     * @return string
     */
    function plural(int $iNumber, array $arTitles): string
    {
        $arCases = [2, 0, 1, 1, 1, 2];
        return $arTitles[($iNumber % 100 > 4 && $iNumber % 100 < 20) ? 2 : $arCases[min($iNumber % 10, 5)]];
    }
}
