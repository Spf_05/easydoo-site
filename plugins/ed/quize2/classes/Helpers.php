<?php namespace Ed\Quize2\Classes;

class Helpers {
    public static function quizesList() {
        return \Ed\Quize2\Models\Quize::select( 'id', 'title' )
                ->get()
                ->keyBy('id')
                ->pluck( 'title', 'id', )
                ->toArray();
    }

    public static function getContactsFromData( $data ) {
        $contacts = [];

        foreach ( $data as $fieldData ) {
            if( $fieldData[ 'type' ] == 'input' ) {
                foreach ( $fieldData[ 'fields' ] as $field ) {
                    if( $field[ 'fieldType' ] == 'name' )
                        $contacts[ 'name' ] = $field[ 'value' ];
                    if( $field[ 'fieldType' ] == 'tel' )
                        $contacts[ 'phone' ] = phoneFormatE164( $field[ 'value' ] );
                    if( $field[ 'fieldType' ] == 'email' )
                        $contacts[ 'mail' ] = $field[ 'value' ];
                }
            }

            if( $fieldData[ 'type' ] == 'phone' ) {
                $contacts[ 'phone' ] = $fieldData[ 'fields' ][ 'phone' ][ 'value' ];
            }
        }
        return $contacts;
    }
}