<?php namespace Ed\Quize2\Classes;

class Redirects {
    private static function input( $fields, $redirect ) {
        foreach( $fields as $field ) {
            if ( $field[ 'name' ] == $redirect[ 'name' ] && $field[ 'value' ] == $redirect[ 'value' ] ) {
                return true;
            }
        }

        return false;
    }

    private static function radio( $fields, $redirect ) {
        foreach( $fields as $field ) {
            if ( $field[ 'name' ] == $redirect[ 'name' ] && $field[ 'value' ] == $redirect[ 'value' ] && $field[ 'checked' ] === true ) {
                return true;
            }
        }

        return false;
    }

    private static function checkbox( $fields, $redirect ) {
        foreach( $fields as $field ) {
            if ( $field[ 'name' ] == $redirect[ 'name' ] && $field[ 'value' ] == $redirect[ 'value' ] && $field[ 'checked' ] === true ) {
                return true;
            }
        }

        return false;
    }

    public static function check($model) {
        if( $model->quize->redirects === null )
            return;

        $redirect = $model->quize->redirects;
        $answers = $model->data;

        foreach( $model->quize->redirects as $redirect ) {
            foreach( $answers as $answer ) {
                if( $answer['type'] == 'input' ) {
                    if( self::input( $answer['fields'], $redirect ) === true ) {
                        return $redirect['url'];
                    }
                }

                if( $answer['type'] == 'radio' ) {
                    reset( $answer['fields'] );
                    $key = key( $answer['fields'] );
                    $arr = $answer['fields']{$key};

                    if( self::radio( $arr, $redirect ) === true ) {
                        return $redirect['url'];
                    }
                }

                if( $answer['type'] == 'checkbox' ) {
                    reset( $answer['fields'] );
                    $key = key( $answer['fields'] );
                    $arr = $answer['fields']{$key};

                    if( self::checkbox( $arr, $redirect ) === true ) {
                        return $redirect['url'];
                    }
                }
            }
        }

        return false;
    }
}