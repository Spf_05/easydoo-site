<?php namespace Ed\Quize2;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function register()
    {
        require_once __DIR__.'/helpers.php';
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }
}
