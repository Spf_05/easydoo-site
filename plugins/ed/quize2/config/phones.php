<?php

return [
    'phone_countries_format' => [
        \libphonenumber\RegionCode::RU,
        \libphonenumber\RegionCode::BY,
    ]
];
