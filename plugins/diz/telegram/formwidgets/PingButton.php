<?php namespace Diz\Telegram\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Diz\Telegram\Models\Telegram as telegramModel;
class PingButton extends FormWidgetBase {
	protected $defaultAlias	= 'telegram_ping_button';
	public function init(){}

	public function render() {
		$this->prepareVars();
		return $this->makePartial('pingbutton');
	}

	public function onPing() {
		telegramModel::ping( input( 'user_id' ), 'test' );
	}

	public function prepareVars() {
		$this->vars['name']		= $this->formField->getName();
		$this->vars['value']	= $this->getLoadValue();
		$this->vars['model']	= $this->model;
	}

	public function loadAssets() {
		$this->addCss('css/pingbutton.css', 'Diz.Telegram');
		$this->addJs('js/pingbutton.js', 'Diz.Telegram');
	}

	public function getSaveValue($value) {
		return $value;
	}
}
