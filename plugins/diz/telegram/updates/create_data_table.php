<?php namespace Diz\Telegram\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDataTable extends Migration
{
    public function up()
    {
        Schema::create('diz_telegram_data', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('diz_telegram_data');
    }
}
