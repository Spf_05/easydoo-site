<?php namespace Diz\Telegram\Models;

use Model;
use Diz\Telegram\Models\Settings;
use Http;
class Telegram extends Model {
	private static function checkGroup( $name ) {
		$groups	= Settings::get( 'groups' );
		if( !is_array( $groups ) )
			return;

		foreach( $groups as $group ) {
			if ( $group[ 'name' ] == $name ) {
				return $group;
			}
		}

		return false;
	}

	static function ping( $user_id, $text ) {
		$param	= array(
			'chat_id'					=> $user_id,
			'text'						=> $text,
			'parse_mode'				=> 'html',
			'disable_web_page_preview'	=> 1
		);

		$token	= Settings::get( 'token' );
		$url	= 'https://api.telegram.org/bot' . $token . '/sendMessage?' . http_build_query( $param );
		$x		= file_get_contents( $url );
	}

	static function send( $data ) {
		$group_name	= !isset( $data[ 'group' ] ) ? 'admins' : $data[ 'group' ];
		$group		= self::checkGroup( $group_name );
		$token		= Settings::get( 'token' );


		if ( $token != '' and $group ) {
			$users	= $group[ 'users' ];
			$text	= '';

			if ( is_array( $data ) ) {
				if ( isset( $data[ 'title' ] ) and $data[ 'title' ] )
					$text .= '<b>' . $data[ 'title' ] . '</b>' . "\n\n";
	
				if ( isset( $data[ 'text' ] ) and $data[ 'text' ] )
					$text .= $data[ 'text' ];
			} else {
				$text	= $data;
			}

			if ( $text != ''  ) {
                Http::post('https://max74.info/easydoo-telegram/index.php', function($http) use ($text){
                    $http->data([ 'text' => $text ]);
                });

                $users = [];
				foreach( $users as $user ) {
					if ( $user[ 'active' ] ) {

						$param	= array(
							'chat_id'					=> $user[ 'id' ],
							'text'						=> $text,
							'parse_mode'				=> 'html',
							'disable_web_page_preview'	=> 1
                        );

						$url	= 'https://api.telegram.org/bot' . $token . '/sendMessage?' . http_build_query( $param );
						$x		= @file_get_contents( $url );

                        if ( $x === FALSE ) {
							trace_log( '======' );
							trace_log( $x );
							trace_log( $url );
							trace_log( 'Ошибка доставки сообщения ' . $user[ 'id' ], $text );
							trace_log( '======' );
						}
					}
				}
			}
		} else {
			if ( $token == '' )
				trace_log( 'diz.telegram not found token' );

			if ( !$group )
				trace_log( 'diz.telegram not found group "' . $group_name . '"' );
		}
	}
}
