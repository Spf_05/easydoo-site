<?php namespace Diz\Telegram\Models;

use Model;

class Settings extends Model {
	public $implement		= [ 'System.Behaviors.SettingsModel' ];
	public $settingsCode	= 'diz_telegram_settings';
	public $settingsFields	= 'fields.yaml';
}