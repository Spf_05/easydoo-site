<?php namespace Diz\Telegram;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase {
	public function pluginDetails() {
		return [
			'name'			=> 'Telegram',
			'description'	=> 'Telegram notificator',
			'author'		=> 'Diz',
			'icon'			=> 'icon-leaf'
		];
	}

	public function register(){}
	public function boot(){}
	
	public function registerComponents() {
		return [];
	}

	public function registerSettings() {
		return [
			'location' => [
				'label'			=> 'Telegram',
				'description'	=> 'Settings for diz telegram plugin',
				'category'		=> 'Diz',
				'icon'			=> 'icon-cog',
				'class'			=> 'Diz\Telegram\Models\Settings',
				'order'			=> 500,
				'keywords'		=> '',
				'permissions'	=> [ 'diz.telegram.access_settings' ]
			]
		];
	}

	public function registerPermissions() {
		return [
			'diz.telegram.access_settings' => [
				'tab'	=> 'Diz Telegram',
				'label'	=> 'Settings'
			],
		];
	}

	public function registerNavigation() {
		return [];
	}

	public function registerFormWidgets() {
		return [
			'Diz\Telegram\FormWidgets\PingButton' => [
				'label'	=> 'Telegram PingButton',
				'code'	=> 'telegram_ping_button',
				'alias'	=> 'telegram_ping_button',
			],
		];
	}
}
