<?php namespace Diz\EditorJs;

use Backend, App, Event;
use System\Classes\PluginBase;
use Yaml;
use File;

class Plugin extends PluginBase {
	public $blocks		= [];
	public $variants	= [];

	public function boot() {
		if (!App::runningInBackend()) {
			return;
		}

		Event::listen( 'backend.page.beforeDisplay', function($controller, $action, $params) {
			$controller->addJs('/plugins/diz/editorjs/assets/custom-blocks-components.js');
			$controller->addCss('/plugins/diz/editorjs/assets/custom-blocks-components.css');

			$controller->addJs('/plugins/diz/editorjs/assets/media-finder.js');
			$controller->addCss('/plugins/diz/editorjs/assets/media-finder.css');

			$controller->addJs('/plugins/diz/editorjs/assets/relation-uploader/index.min.js');
			$controller->addCss('/plugins/diz/editorjs/assets/relation-uploader/index.min.css');
		});
	}

	public function pluginDetails() {
		return [
			'name'        => 'EditorJs',
			'description' => 'No description provided yet...',
			'author'      => 'Diz',
			'icon'        => 'icon-leaf'
		];
	}

	public function getConfig( $name ) {
		$configFile	= plugins_path('diz/editorjs/config/' . $name . '.yaml');
		return Yaml::parse(File::get($configFile));
	}

	public function addBlock( $name, $props = [] ) {
		$configFile	= plugins_path('diz/editorjs/config/' . $name . '.yaml');
		$settings	= Yaml::parse( File::get( $configFile ) );

		foreach( $props as $field => $value ) {
			array_set( $settings, $field, $value );
		}

		$this->blocks[ $name ] = [
			'settings'		=> $settings,
			'scripts'		=> [ '/plugins/diz/editorjs/assets/editorjs-custom-blocks.js' ],
		];
	}

	public function registerEditorBlocks() {
		$this->addBlock( 'images' );


        $this->blocks[ 'ShopaholicProducts' ] = [
            'settings' => [
                'class' => 'ShopaholicProducts'
            ],
            'validation' => [
                // 'html' => [
                //     'type' => 'string',
                //     'allowedTags' => '*',
                // ]
            ],
            'scripts' => [
                '/plugins/diz/editorjs/assets/shopaholic-products/dist/bundle.js'
            ],
        ];

        $this->blocks[ 'Quiz' ] = [
            'settings' => [
                'class' => 'Quiz'
            ],
            'validation' => [
                // 'html' => [
                //     'type' => 'string',
                //     'allowedTags' => '*',
                // ]
            ],
            'scripts' => [
                '/plugins/diz/editorjs/assets/quiz/dist/bundle.js'
            ],
        ];

        $this->blocks[ 'Attention' ] = [
            'settings' => [
                'class' => 'Attention'
            ],
            'validation' => [
                // 'html' => [
                //     'type' => 'string',
                //     'allowedTags' => '*',
                // ]
            ],
            'scripts' => [
                '/plugins/diz/editorjs/assets/attention/dist/bundle.js'
            ],
        ];

		return $this->blocks;
	}
}
