import lang from './langs/ru.json';

export const makePreloader = ( selector = null ) => {
    return `
        <div ${ selector ? `data-selector="$${ selector }"` : '' } class="preloader-wrapper">
            <div class="preloader">
                <div class="preloader__item preloader__item_one"></div>
                <div class="preloader__item preloader__item_two"></div>
                <div class="preloader__item preloader__item_three"></div>
            </div>
        </div>
    `
}

export const makeQuiz = item => {
    return `
        <div>
            ${ item.title } <button data-action="delete" data-id="${ item.id }" type="button">${ lang.btnDelete }</button>
        </div>
    `
}

export const makeLayout = () => {
    return `
        <div>
            <div class="toolbar cdx-block">
                <button data-selector="$btnAdd" data-action="add" type="button" class="cdx-button">${ lang.btnAdd }</button>
            </div>

            <div data-selector="$quizPlaceholder" class="quiz-placeholder"></div>
        </div>
    `
}

export const makeModalLayout = () => {
    return `
        <div class="quiz-modal">
            <div class="quiz-modal__overlay"></div>
            <div class="quiz-modal__content">
                <div class="quiz-modal__list-wrapper">
                    <ul data-noitems="${ lang.noItems }" data-selector="$list" class="quiz-modal__list custom-scroll"></ul>

                    ${ makePreloader('preloader') }
                </div>

                <div class="quiz-modal__footer">
                    <button data-action="close" class="cdx-button" type="button">${ lang.btnModalClose }</button>
                </div>
            </div>
        </div>
    `
}

export const makeModalItem = item => {
    return `
        <li>
            ${ item.title }
            <button data-id="${ item.id }" data-action="add" class="cdx-button" type="button">${ lang.btnModalAdd }</button>
        </li>
    `
}