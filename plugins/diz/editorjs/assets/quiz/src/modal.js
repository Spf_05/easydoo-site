import { makeElementFromHtml, fetchData, debounce } from './utils';
import { makeModalLayout, makeModalItem } from './templates';
import config from './config.json';

export default function modal() {
    return new Promise( (resolve, reject) => {
        const modalLayoutHtml = makeModalLayout();
        const $modalLayout = makeElementFromHtml( modalLayoutHtml );

        const { $list, $preloader } = $modalLayout.collect();

        const setFetchLoader = state => {
            $preloader.classList[ state ? 'add' : 'remove' ]('preloader-wrapper_show');
        }

        const requestSuccess = data => {
            $list.innerHTML = data.reduce( ( html, productItem ) => html + makeModalItem( productItem ), '' );
        }

        const clickHandle = ({ target : $el }) => {
            const action = $el.dataset.action;

            if( action == 'close' ) {
                closeModal();
            }

            if( action == 'add' ) {
                closeModal();
                resolve( $el.dataset.id );
            }
        }

        function closeModal() {
            $modalLayout.removeEventListener('click', clickHandle );
            $modalLayout.remove();
        }

        $modalLayout.addEventListener('click', clickHandle );

        setFetchLoader( true );

        fetchData( config.listEndpoint )
            .then( requestSuccess )
            .catch( err => console.error( `fetch err ${ err }` ) )
            .finally( () => setFetchLoader( false ) );

        document.body.appendChild( $modalLayout );
    })
}