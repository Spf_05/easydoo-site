import './styles.scss';
import modal from './modal.js';
import { makeElementFromHtml, fetchData } from './utils';
import { makeLayout, makeQuiz } from './templates';
import lang from './langs/ru.json';
import config from './config.json';
import icon from './icon.svg';

export default class Quiz {
    static get toolbox() {
        return {
            title: 'Quize',
            icon: icon
        };
    }

    constructor({data}){
        this.data = data;
        this.quiz = null;
        this.fetchQuiz();
    }

    setFetchLoader( state ) {

    }

    fetchQuiz() {
        if ( !this.data.id ) {
            return;
        }

        this.setFetchLoader( true );

        const urlParts = [
            config.itemEndpoint.replace(/\/$/g, ''),
            this.data.id,
        ];

        fetchData( urlParts.join('/'), )
            .then( data => {
                this.quiz = data;
                this.renderQuiz();
            })
            .catch( err => console.error( `fetch err ${ err }` ) )
            .finally( () => this.setFetchLoader( false ) );
    }

    renderQuiz() {
        if( this.quiz === null ) {
            this.$btnAdd.style.display = 'block';
            this.$quizPlaceholder.innerHTML = '';
        } else {
            this.$btnAdd.style.display = 'none';
            this.$quizPlaceholder.innerHTML = makeQuiz(this.quiz);
        }
    }

    clickHandler = (e) => {
        const $el = e.target;
        const action = $el.dataset.action;

        if( action == 'add' ) {
            modal()
                .then( payload => {
                    this.data.id = payload;
                    this.fetchQuiz();
                })

            return;
        }

        if( action == 'delete' ) {
            if( !confirm( lang.confirmDelete ) ) return;
            this.quiz = null;
            this.renderQuiz();
            return;
        }
    }

    renderLayout() {
        const $root = document.createElement('div');

        $root.addEventListener( 'click', this.clickHandler );

        const layoutHTML = makeLayout();
        const $layout = makeElementFromHtml( layoutHTML );
        $layout.collect({ to : this });

        this.renderQuiz();

        $root.insertAdjacentElement( 'afterbegin', $layout );
        return $root;
    }

    render(){
        return this.renderLayout();
    }

    save(blockContent){
        return {
            id: this.data.id,
        }
    }
}