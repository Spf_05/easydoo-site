(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["Quiz"] = factory();
	else
		root["Quiz"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/styles.scss":
/*!******************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/styles.scss ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("exports = module.exports = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\")(false);\n// Module\nexports.push([module.i, \".custom-scroll::-webkit-scrollbar {\\n  width: 7px;\\n}\\n.custom-scroll::-webkit-scrollbar-track {\\n  background: #f1f1f1;\\n}\\n.custom-scroll::-webkit-scrollbar-thumb {\\n  background: #888;\\n}\\n.custom-scroll::-webkit-scrollbar-thumb:hover {\\n  background: #555;\\n}\\n\\n.toolbar {\\n  padding: 0 5px;\\n}\\n\\n.cdx-button:disabled {\\n  cursor: not-allowed;\\n  color: #fff;\\n  background-color: #ccc;\\n}\\n\\n.quiz-modal {\\n  position: fixed;\\n  top: 0;\\n  left: 0;\\n  width: 100%;\\n  height: 100%;\\n  z-index: 2000000000;\\n  display: flex;\\n  align-items: center;\\n  justify-content: center;\\n}\\n.quiz-modal__overlay {\\n  position: absolute;\\n  left: 0;\\n  top: 0;\\n  width: 100%;\\n  height: 100%;\\n  z-index: 1;\\n  background-color: rgba(0, 0, 0, 0.5);\\n}\\n.quiz-modal__content {\\n  max-width: 70vw;\\n  max-height: 70vh;\\n  background-color: #fff;\\n  border-radius: 20px;\\n  padding: 20px;\\n  position: relative;\\n  z-index: 2;\\n  width: 100%;\\n  height: 100%;\\n  display: flex;\\n  flex-direction: column;\\n  min-height: 1px;\\n}\\n.quiz-modal__list-wrapper {\\n  position: relative;\\n  flex-grow: 1;\\n  margin-top: 15px;\\n  margin-bottom: 15px;\\n  overflow: hidden;\\n  display: flex;\\n  flex-direction: column;\\n}\\n.quiz-modal__list {\\n  overflow-y: auto;\\n  position: relative;\\n  height: 100%;\\n}\\n.quiz-modal__list:empty::after {\\n  content: attr(data-noitems);\\n  display: flex;\\n  align-items: center;\\n  justify-content: center;\\n  font-weight: bold;\\n  position: absolute;\\n  left: 0;\\n  top: 0;\\n  width: 100%;\\n  height: 100%;\\n}\\n.quiz-modal__footer {\\n  display: flex;\\n  border-top: 1px solid #ccc;\\n  padding-top: 5px;\\n  justify-content: flex-end;\\n}\\n.quiz-modal__footer > * + * {\\n  margin-left: 5px;\\n}\\n\\n/* PRELOADER */\\n.preloader-wrapper {\\n  position: absolute;\\n  left: 0;\\n  top: 0;\\n  width: 100%;\\n  height: 100%;\\n  background: rgba(255, 255, 255, 0.85);\\n  z-index: 100000;\\n  display: flex;\\n  align-items: center;\\n  justify-content: center;\\n  opacity: 0;\\n  pointer-events: none;\\n  transition: opacity 0.3s linear;\\n}\\n.preloader-wrapper_show {\\n  pointer-events: initial;\\n  opacity: 1;\\n}\\n\\n.preloader {\\n  text-align: center;\\n  position: relative;\\n  user-select: none;\\n  width: 80px;\\n}\\n.preloader__item {\\n  display: inline-block;\\n  width: 18px;\\n  height: 18px;\\n  border-radius: 100%;\\n  background-color: #ccc;\\n  animation: preloader 1.4s infinite ease-in-out both;\\n}\\n.preloader__item_one {\\n  animation-delay: -0.32s;\\n}\\n.preloader__item_two {\\n  animation-delay: -0.16s;\\n}\\n\\n@keyframes preloader {\\n  0%, 80%, 100% {\\n    transform: scale(0);\\n  }\\n  40% {\\n    transform: scale(1);\\n  }\\n}\", \"\"]);\n\n\n\n//# sourceURL=webpack://Quiz/./src/styles.scss?./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\nmodule.exports = function (useSourceMap) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item, useSourceMap);\n\n      if (item[2]) {\n        return '@media ' + item[2] + '{' + content + '}';\n      } else {\n        return content;\n      }\n    }).join('');\n  }; // import a list of modules into the list\n\n\n  list.i = function (modules, mediaQuery) {\n    if (typeof modules === 'string') {\n      modules = [[null, modules, '']];\n    }\n\n    var alreadyImportedModules = {};\n\n    for (var i = 0; i < this.length; i++) {\n      var id = this[i][0];\n\n      if (id != null) {\n        alreadyImportedModules[id] = true;\n      }\n    }\n\n    for (i = 0; i < modules.length; i++) {\n      var item = modules[i]; // skip already imported module\n      // this implementation is not 100% perfect for weird media query combinations\n      // when a module is imported multiple times with different media queries.\n      // I hope this will never occur (Hey this way we have smaller bundles)\n\n      if (item[0] == null || !alreadyImportedModules[item[0]]) {\n        if (mediaQuery && !item[2]) {\n          item[2] = mediaQuery;\n        } else if (mediaQuery) {\n          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';\n        }\n\n        list.push(item);\n      }\n    }\n  };\n\n  return list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n  var content = item[1] || '';\n  var cssMapping = item[3];\n\n  if (!cssMapping) {\n    return content;\n  }\n\n  if (useSourceMap && typeof btoa === 'function') {\n    var sourceMapping = toComment(cssMapping);\n    var sourceURLs = cssMapping.sources.map(function (source) {\n      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';\n    });\n    return [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n  }\n\n  return [content].join('\\n');\n} // Adapted from convert-source-map (MIT)\n\n\nfunction toComment(sourceMap) {\n  // eslint-disable-next-line no-undef\n  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));\n  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;\n  return '/*# ' + data + ' */';\n}\n\n//# sourceURL=webpack://Quiz/./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/*\n\tMIT License http://www.opensource.org/licenses/mit-license.php\n\tAuthor Tobias Koppers @sokra\n*/\n\nvar stylesInDom = {};\n\nvar\tmemoize = function (fn) {\n\tvar memo;\n\n\treturn function () {\n\t\tif (typeof memo === \"undefined\") memo = fn.apply(this, arguments);\n\t\treturn memo;\n\t};\n};\n\nvar isOldIE = memoize(function () {\n\t// Test for IE <= 9 as proposed by Browserhacks\n\t// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n\t// Tests for existence of standard globals is to allow style-loader\n\t// to operate correctly into non-standard environments\n\t// @see https://github.com/webpack-contrib/style-loader/issues/177\n\treturn window && document && document.all && !window.atob;\n});\n\nvar getTarget = function (target, parent) {\n  if (parent){\n    return parent.querySelector(target);\n  }\n  return document.querySelector(target);\n};\n\nvar getElement = (function (fn) {\n\tvar memo = {};\n\n\treturn function(target, parent) {\n                // If passing function in options, then use it for resolve \"head\" element.\n                // Useful for Shadow Root style i.e\n                // {\n                //   insertInto: function () { return document.querySelector(\"#foo\").shadowRoot }\n                // }\n                if (typeof target === 'function') {\n                        return target();\n                }\n                if (typeof memo[target] === \"undefined\") {\n\t\t\tvar styleTarget = getTarget.call(this, target, parent);\n\t\t\t// Special case to return head of iframe instead of iframe itself\n\t\t\tif (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n\t\t\t\ttry {\n\t\t\t\t\t// This will throw an exception if access to iframe is blocked\n\t\t\t\t\t// due to cross-origin restrictions\n\t\t\t\t\tstyleTarget = styleTarget.contentDocument.head;\n\t\t\t\t} catch(e) {\n\t\t\t\t\tstyleTarget = null;\n\t\t\t\t}\n\t\t\t}\n\t\t\tmemo[target] = styleTarget;\n\t\t}\n\t\treturn memo[target]\n\t};\n})();\n\nvar singleton = null;\nvar\tsingletonCounter = 0;\nvar\tstylesInsertedAtTop = [];\n\nvar\tfixUrls = __webpack_require__(/*! ./urls */ \"./node_modules/style-loader/lib/urls.js\");\n\nmodule.exports = function(list, options) {\n\tif (typeof DEBUG !== \"undefined\" && DEBUG) {\n\t\tif (typeof document !== \"object\") throw new Error(\"The style-loader cannot be used in a non-browser environment\");\n\t}\n\n\toptions = options || {};\n\n\toptions.attrs = typeof options.attrs === \"object\" ? options.attrs : {};\n\n\t// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n\t// tags it will allow on a page\n\tif (!options.singleton && typeof options.singleton !== \"boolean\") options.singleton = isOldIE();\n\n\t// By default, add <style> tags to the <head> element\n        if (!options.insertInto) options.insertInto = \"head\";\n\n\t// By default, add <style> tags to the bottom of the target\n\tif (!options.insertAt) options.insertAt = \"bottom\";\n\n\tvar styles = listToStyles(list, options);\n\n\taddStylesToDom(styles, options);\n\n\treturn function update (newList) {\n\t\tvar mayRemove = [];\n\n\t\tfor (var i = 0; i < styles.length; i++) {\n\t\t\tvar item = styles[i];\n\t\t\tvar domStyle = stylesInDom[item.id];\n\n\t\t\tdomStyle.refs--;\n\t\t\tmayRemove.push(domStyle);\n\t\t}\n\n\t\tif(newList) {\n\t\t\tvar newStyles = listToStyles(newList, options);\n\t\t\taddStylesToDom(newStyles, options);\n\t\t}\n\n\t\tfor (var i = 0; i < mayRemove.length; i++) {\n\t\t\tvar domStyle = mayRemove[i];\n\n\t\t\tif(domStyle.refs === 0) {\n\t\t\t\tfor (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();\n\n\t\t\t\tdelete stylesInDom[domStyle.id];\n\t\t\t}\n\t\t}\n\t};\n};\n\nfunction addStylesToDom (styles, options) {\n\tfor (var i = 0; i < styles.length; i++) {\n\t\tvar item = styles[i];\n\t\tvar domStyle = stylesInDom[item.id];\n\n\t\tif(domStyle) {\n\t\t\tdomStyle.refs++;\n\n\t\t\tfor(var j = 0; j < domStyle.parts.length; j++) {\n\t\t\t\tdomStyle.parts[j](item.parts[j]);\n\t\t\t}\n\n\t\t\tfor(; j < item.parts.length; j++) {\n\t\t\t\tdomStyle.parts.push(addStyle(item.parts[j], options));\n\t\t\t}\n\t\t} else {\n\t\t\tvar parts = [];\n\n\t\t\tfor(var j = 0; j < item.parts.length; j++) {\n\t\t\t\tparts.push(addStyle(item.parts[j], options));\n\t\t\t}\n\n\t\t\tstylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};\n\t\t}\n\t}\n}\n\nfunction listToStyles (list, options) {\n\tvar styles = [];\n\tvar newStyles = {};\n\n\tfor (var i = 0; i < list.length; i++) {\n\t\tvar item = list[i];\n\t\tvar id = options.base ? item[0] + options.base : item[0];\n\t\tvar css = item[1];\n\t\tvar media = item[2];\n\t\tvar sourceMap = item[3];\n\t\tvar part = {css: css, media: media, sourceMap: sourceMap};\n\n\t\tif(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});\n\t\telse newStyles[id].parts.push(part);\n\t}\n\n\treturn styles;\n}\n\nfunction insertStyleElement (options, style) {\n\tvar target = getElement(options.insertInto)\n\n\tif (!target) {\n\t\tthrow new Error(\"Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.\");\n\t}\n\n\tvar lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];\n\n\tif (options.insertAt === \"top\") {\n\t\tif (!lastStyleElementInsertedAtTop) {\n\t\t\ttarget.insertBefore(style, target.firstChild);\n\t\t} else if (lastStyleElementInsertedAtTop.nextSibling) {\n\t\t\ttarget.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);\n\t\t} else {\n\t\t\ttarget.appendChild(style);\n\t\t}\n\t\tstylesInsertedAtTop.push(style);\n\t} else if (options.insertAt === \"bottom\") {\n\t\ttarget.appendChild(style);\n\t} else if (typeof options.insertAt === \"object\" && options.insertAt.before) {\n\t\tvar nextSibling = getElement(options.insertAt.before, target);\n\t\ttarget.insertBefore(style, nextSibling);\n\t} else {\n\t\tthrow new Error(\"[Style Loader]\\n\\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\\n Must be 'top', 'bottom', or Object.\\n (https://github.com/webpack-contrib/style-loader#insertat)\\n\");\n\t}\n}\n\nfunction removeStyleElement (style) {\n\tif (style.parentNode === null) return false;\n\tstyle.parentNode.removeChild(style);\n\n\tvar idx = stylesInsertedAtTop.indexOf(style);\n\tif(idx >= 0) {\n\t\tstylesInsertedAtTop.splice(idx, 1);\n\t}\n}\n\nfunction createStyleElement (options) {\n\tvar style = document.createElement(\"style\");\n\n\tif(options.attrs.type === undefined) {\n\t\toptions.attrs.type = \"text/css\";\n\t}\n\n\tif(options.attrs.nonce === undefined) {\n\t\tvar nonce = getNonce();\n\t\tif (nonce) {\n\t\t\toptions.attrs.nonce = nonce;\n\t\t}\n\t}\n\n\taddAttrs(style, options.attrs);\n\tinsertStyleElement(options, style);\n\n\treturn style;\n}\n\nfunction createLinkElement (options) {\n\tvar link = document.createElement(\"link\");\n\n\tif(options.attrs.type === undefined) {\n\t\toptions.attrs.type = \"text/css\";\n\t}\n\toptions.attrs.rel = \"stylesheet\";\n\n\taddAttrs(link, options.attrs);\n\tinsertStyleElement(options, link);\n\n\treturn link;\n}\n\nfunction addAttrs (el, attrs) {\n\tObject.keys(attrs).forEach(function (key) {\n\t\tel.setAttribute(key, attrs[key]);\n\t});\n}\n\nfunction getNonce() {\n\tif (false) {}\n\n\treturn __webpack_require__.nc;\n}\n\nfunction addStyle (obj, options) {\n\tvar style, update, remove, result;\n\n\t// If a transform function was defined, run it on the css\n\tif (options.transform && obj.css) {\n\t    result = typeof options.transform === 'function'\n\t\t ? options.transform(obj.css) \n\t\t : options.transform.default(obj.css);\n\n\t    if (result) {\n\t    \t// If transform returns a value, use that instead of the original css.\n\t    \t// This allows running runtime transformations on the css.\n\t    \tobj.css = result;\n\t    } else {\n\t    \t// If the transform function returns a falsy value, don't add this css.\n\t    \t// This allows conditional loading of css\n\t    \treturn function() {\n\t    \t\t// noop\n\t    \t};\n\t    }\n\t}\n\n\tif (options.singleton) {\n\t\tvar styleIndex = singletonCounter++;\n\n\t\tstyle = singleton || (singleton = createStyleElement(options));\n\n\t\tupdate = applyToSingletonTag.bind(null, style, styleIndex, false);\n\t\tremove = applyToSingletonTag.bind(null, style, styleIndex, true);\n\n\t} else if (\n\t\tobj.sourceMap &&\n\t\ttypeof URL === \"function\" &&\n\t\ttypeof URL.createObjectURL === \"function\" &&\n\t\ttypeof URL.revokeObjectURL === \"function\" &&\n\t\ttypeof Blob === \"function\" &&\n\t\ttypeof btoa === \"function\"\n\t) {\n\t\tstyle = createLinkElement(options);\n\t\tupdate = updateLink.bind(null, style, options);\n\t\tremove = function () {\n\t\t\tremoveStyleElement(style);\n\n\t\t\tif(style.href) URL.revokeObjectURL(style.href);\n\t\t};\n\t} else {\n\t\tstyle = createStyleElement(options);\n\t\tupdate = applyToTag.bind(null, style);\n\t\tremove = function () {\n\t\t\tremoveStyleElement(style);\n\t\t};\n\t}\n\n\tupdate(obj);\n\n\treturn function updateStyle (newObj) {\n\t\tif (newObj) {\n\t\t\tif (\n\t\t\t\tnewObj.css === obj.css &&\n\t\t\t\tnewObj.media === obj.media &&\n\t\t\t\tnewObj.sourceMap === obj.sourceMap\n\t\t\t) {\n\t\t\t\treturn;\n\t\t\t}\n\n\t\t\tupdate(obj = newObj);\n\t\t} else {\n\t\t\tremove();\n\t\t}\n\t};\n}\n\nvar replaceText = (function () {\n\tvar textStore = [];\n\n\treturn function (index, replacement) {\n\t\ttextStore[index] = replacement;\n\n\t\treturn textStore.filter(Boolean).join('\\n');\n\t};\n})();\n\nfunction applyToSingletonTag (style, index, remove, obj) {\n\tvar css = remove ? \"\" : obj.css;\n\n\tif (style.styleSheet) {\n\t\tstyle.styleSheet.cssText = replaceText(index, css);\n\t} else {\n\t\tvar cssNode = document.createTextNode(css);\n\t\tvar childNodes = style.childNodes;\n\n\t\tif (childNodes[index]) style.removeChild(childNodes[index]);\n\n\t\tif (childNodes.length) {\n\t\t\tstyle.insertBefore(cssNode, childNodes[index]);\n\t\t} else {\n\t\t\tstyle.appendChild(cssNode);\n\t\t}\n\t}\n}\n\nfunction applyToTag (style, obj) {\n\tvar css = obj.css;\n\tvar media = obj.media;\n\n\tif(media) {\n\t\tstyle.setAttribute(\"media\", media)\n\t}\n\n\tif(style.styleSheet) {\n\t\tstyle.styleSheet.cssText = css;\n\t} else {\n\t\twhile(style.firstChild) {\n\t\t\tstyle.removeChild(style.firstChild);\n\t\t}\n\n\t\tstyle.appendChild(document.createTextNode(css));\n\t}\n}\n\nfunction updateLink (link, options, obj) {\n\tvar css = obj.css;\n\tvar sourceMap = obj.sourceMap;\n\n\t/*\n\t\tIf convertToAbsoluteUrls isn't defined, but sourcemaps are enabled\n\t\tand there is no publicPath defined then lets turn convertToAbsoluteUrls\n\t\ton by default.  Otherwise default to the convertToAbsoluteUrls option\n\t\tdirectly\n\t*/\n\tvar autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;\n\n\tif (options.convertToAbsoluteUrls || autoFixUrls) {\n\t\tcss = fixUrls(css);\n\t}\n\n\tif (sourceMap) {\n\t\t// http://stackoverflow.com/a/26603875\n\t\tcss += \"\\n/*# sourceMappingURL=data:application/json;base64,\" + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + \" */\";\n\t}\n\n\tvar blob = new Blob([css], { type: \"text/css\" });\n\n\tvar oldSrc = link.href;\n\n\tlink.href = URL.createObjectURL(blob);\n\n\tif(oldSrc) URL.revokeObjectURL(oldSrc);\n}\n\n\n//# sourceURL=webpack://Quiz/./node_modules/style-loader/lib/addStyles.js?");

/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("\n/**\n * When source maps are enabled, `style-loader` uses a link element with a data-uri to\n * embed the css on the page. This breaks all relative urls because now they are relative to a\n * bundle instead of the current page.\n *\n * One solution is to only use full urls, but that may be impossible.\n *\n * Instead, this function \"fixes\" the relative urls to be absolute according to the current page location.\n *\n * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.\n *\n */\n\nmodule.exports = function (css) {\n  // get current location\n  var location = typeof window !== \"undefined\" && window.location;\n\n  if (!location) {\n    throw new Error(\"fixUrls requires window.location\");\n  }\n\n\t// blank or null?\n\tif (!css || typeof css !== \"string\") {\n\t  return css;\n  }\n\n  var baseUrl = location.protocol + \"//\" + location.host;\n  var currentDir = baseUrl + location.pathname.replace(/\\/[^\\/]*$/, \"/\");\n\n\t// convert each url(...)\n\t/*\n\tThis regular expression is just a way to recursively match brackets within\n\ta string.\n\n\t /url\\s*\\(  = Match on the word \"url\" with any whitespace after it and then a parens\n\t   (  = Start a capturing group\n\t     (?:  = Start a non-capturing group\n\t         [^)(]  = Match anything that isn't a parentheses\n\t         |  = OR\n\t         \\(  = Match a start parentheses\n\t             (?:  = Start another non-capturing groups\n\t                 [^)(]+  = Match anything that isn't a parentheses\n\t                 |  = OR\n\t                 \\(  = Match a start parentheses\n\t                     [^)(]*  = Match anything that isn't a parentheses\n\t                 \\)  = Match a end parentheses\n\t             )  = End Group\n              *\\) = Match anything and then a close parens\n          )  = Close non-capturing group\n          *  = Match anything\n       )  = Close capturing group\n\t \\)  = Match a close parens\n\n\t /gi  = Get all matches, not the first.  Be case insensitive.\n\t */\n\tvar fixedCss = css.replace(/url\\s*\\(((?:[^)(]|\\((?:[^)(]+|\\([^)(]*\\))*\\))*)\\)/gi, function(fullMatch, origUrl) {\n\t\t// strip quotes (if they exist)\n\t\tvar unquotedOrigUrl = origUrl\n\t\t\t.trim()\n\t\t\t.replace(/^\"(.*)\"$/, function(o, $1){ return $1; })\n\t\t\t.replace(/^'(.*)'$/, function(o, $1){ return $1; });\n\n\t\t// already a full url? no change\n\t\tif (/^(#|data:|http:\\/\\/|https:\\/\\/|file:\\/\\/\\/|\\s*$)/i.test(unquotedOrigUrl)) {\n\t\t  return fullMatch;\n\t\t}\n\n\t\t// convert the url to a full url\n\t\tvar newUrl;\n\n\t\tif (unquotedOrigUrl.indexOf(\"//\") === 0) {\n\t\t  \t//TODO: should we add protocol?\n\t\t\tnewUrl = unquotedOrigUrl;\n\t\t} else if (unquotedOrigUrl.indexOf(\"/\") === 0) {\n\t\t\t// path should be relative to the base url\n\t\t\tnewUrl = baseUrl + unquotedOrigUrl; // already starts with '/'\n\t\t} else {\n\t\t\t// path should be relative to current directory\n\t\t\tnewUrl = currentDir + unquotedOrigUrl.replace(/^\\.\\//, \"\"); // Strip leading './'\n\t\t}\n\n\t\t// send back the fixed url(...)\n\t\treturn \"url(\" + JSON.stringify(newUrl) + \")\";\n\t});\n\n\t// send back the fixed css\n\treturn fixedCss;\n};\n\n\n//# sourceURL=webpack://Quiz/./node_modules/style-loader/lib/urls.js?");

/***/ }),

/***/ "./src/config.json":
/*!*************************!*\
  !*** ./src/config.json ***!
  \*************************/
/*! exports provided: listEndpoint, itemEndpoint, default */
/***/ (function(module) {

eval("module.exports = JSON.parse(\"{\\\"listEndpoint\\\":\\\"/api/get-quiz-list\\\",\\\"itemEndpoint\\\":\\\"/api/get-quiz\\\"}\");\n\n//# sourceURL=webpack://Quiz/./src/config.json?");

/***/ }),

/***/ "./src/icon.svg":
/*!**********************!*\
  !*** ./src/icon.svg ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"<svg version=\\\"1.1\\\" id=\\\"Capa_1\\\" xmlns=\\\"http://www.w3.org/2000/svg\\\" xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\" x=\\\"0px\\\" y=\\\"0px\\\" viewBox=\\\"0 0 294.023 294.023\\\" style=\\\"enable-background:new 0 0 294.023 294.023;\\\" xml:space=\\\"preserve\\\"><path color-rendering=\\\"auto\\\" image-rendering=\\\"auto\\\" shape-rendering=\\\"auto\\\" color-interpolation=\\\"sRGB\\\" d=\\\"M124.916,0.002 c-1.649,0.045-3.169,0.9-4.064,2.285l-14.49,21.736h-49.35c-2.761,0-5,2.239-5,5v50c0,2.761,2.239,5,5,5h50c2.761,0,5-2.239,5-5 V39.574l-10,15v19.449h-40v-40h37.682L85.631,55.117l-6.146-12.293c-1.205-2.485-4.196-3.523-6.681-2.318 c-2.485,1.205-3.523,4.196-2.318,6.681c0.018,0.036,0.035,0.072,0.054,0.108l10,20c1.235,2.47,4.238,3.472,6.709,2.237 c0.778-0.389,1.441-0.974,1.924-1.698l40-60c1.565-2.276,0.989-5.389-1.287-6.954C127.013,0.281,125.974-0.027,124.916,0.002 L124.916,0.002z M147.012,44.025c-2.761,0-5,2.239-5,5v10c0,2.761,2.239,5,5,5h90c2.761,0,5-2.239,5-5v-10c0-2.761-2.239-5-5-5 H147.012z M57.012,94.06c-2.761,0-5,2.239-5,5v50c0,2.761,2.239,5,5,5h50c2.761,0,5-2.239,5-5v-50c0-2.761-2.239-5-5-5H57.012z M62.012,104.06h40v40h-40V104.06z M147.012,114.023c-2.761,0-5,2.239-5,5v10c0,2.761,2.239,5,5,5h90c2.761,0,5-2.239,5-5v-10 c0-2.761-2.239-5-5-5H147.012z M57.012,164.023c-2.761,0-5,2.239-5,5v50c0,2.761,2.239,5,5,5h50c2.761,0,5-2.239,5-5v-50 c0-2.761-2.239-5-5-5H57.012z M62.012,174.023h40v40h-40V174.023z M147.012,184.058c-2.761,0-5,2.239-5,5v10c0,2.761,2.239,5,5,5h90 c2.761,0,5-2.239,5-5v-10c0-2.761-2.239-5-5-5H147.012z M57.012,234.023c-2.761,0-5,2.239-5,5v50c0,2.761,2.239,5,5,5h50 c2.761,0,5-2.239,5-5v-50c0-2.761-2.239-5-5-5L57.012,234.023L57.012,234.023z M62.012,244.023h40v40h-40V244.023z M147.012,254.023 c-2.761,0-5,2.239-5,5v10c0,2.761,2.239,5,5,5h90c2.761,0,5-2.239,5-5v-10c0-2.761-2.239-5-5-5H147.012z\\\"></path><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>\"\n\n//# sourceURL=webpack://Quiz/./src/icon.svg?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Quiz; });\n/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./styles.scss */ \"./src/styles.scss\");\n/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styles_scss__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal.js */ \"./src/modal.js\");\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils */ \"./src/utils.js\");\n/* harmony import */ var _templates__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./templates */ \"./src/templates.js\");\n/* harmony import */ var _langs_ru_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./langs/ru.json */ \"./src/langs/ru.json\");\nvar _langs_ru_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./langs/ru.json */ \"./src/langs/ru.json\", 1);\n/* harmony import */ var _config_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./config.json */ \"./src/config.json\");\nvar _config_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./config.json */ \"./src/config.json\", 1);\n/* harmony import */ var _icon_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./icon.svg */ \"./src/icon.svg\");\n/* harmony import */ var _icon_svg__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_icon_svg__WEBPACK_IMPORTED_MODULE_6__);\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, \"prototype\", { writable: false }); return Constructor; }\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n\n\n\nvar Quiz = /*#__PURE__*/function () {\n  function Quiz(_ref) {\n    var _this = this;\n    var data = _ref.data;\n    _classCallCheck(this, Quiz);\n    _defineProperty(this, \"clickHandler\", function (e) {\n      var $el = e.target;\n      var action = $el.dataset.action;\n      if (action == 'add') {\n        Object(_modal_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"])().then(function (payload) {\n          _this.data.id = payload;\n          _this.fetchQuiz();\n        });\n        return;\n      }\n      if (action == 'delete') {\n        if (!confirm(_langs_ru_json__WEBPACK_IMPORTED_MODULE_4__.confirmDelete)) return;\n        _this.quiz = null;\n        _this.renderQuiz();\n        return;\n      }\n    });\n    this.data = data;\n    this.quiz = null;\n    this.fetchQuiz();\n  }\n  _createClass(Quiz, [{\n    key: \"setFetchLoader\",\n    value: function setFetchLoader(state) {}\n  }, {\n    key: \"fetchQuiz\",\n    value: function fetchQuiz() {\n      var _this2 = this;\n      if (!this.data.id) {\n        return;\n      }\n      this.setFetchLoader(true);\n      var urlParts = [_config_json__WEBPACK_IMPORTED_MODULE_5__.itemEndpoint.replace(/\\/$/g, ''), this.data.id];\n      Object(_utils__WEBPACK_IMPORTED_MODULE_2__[\"fetchData\"])(urlParts.join('/')).then(function (data) {\n        _this2.quiz = data;\n        _this2.renderQuiz();\n      })[\"catch\"](function (err) {\n        return console.error(\"fetch err \".concat(err));\n      })[\"finally\"](function () {\n        return _this2.setFetchLoader(false);\n      });\n    }\n  }, {\n    key: \"renderQuiz\",\n    value: function renderQuiz() {\n      if (this.quiz === null) {\n        this.$btnAdd.style.display = 'block';\n        this.$quizPlaceholder.innerHTML = '';\n      } else {\n        this.$btnAdd.style.display = 'none';\n        this.$quizPlaceholder.innerHTML = Object(_templates__WEBPACK_IMPORTED_MODULE_3__[\"makeQuiz\"])(this.quiz);\n      }\n    }\n  }, {\n    key: \"renderLayout\",\n    value: function renderLayout() {\n      var $root = document.createElement('div');\n      $root.addEventListener('click', this.clickHandler);\n      var layoutHTML = Object(_templates__WEBPACK_IMPORTED_MODULE_3__[\"makeLayout\"])();\n      var $layout = Object(_utils__WEBPACK_IMPORTED_MODULE_2__[\"makeElementFromHtml\"])(layoutHTML);\n      $layout.collect({\n        to: this\n      });\n      this.renderQuiz();\n      $root.insertAdjacentElement('afterbegin', $layout);\n      return $root;\n    }\n  }, {\n    key: \"render\",\n    value: function render() {\n      return this.renderLayout();\n    }\n  }, {\n    key: \"save\",\n    value: function save(blockContent) {\n      return {\n        id: this.data.id\n      };\n    }\n  }], [{\n    key: \"toolbox\",\n    get: function get() {\n      return {\n        title: 'Quize',\n        icon: _icon_svg__WEBPACK_IMPORTED_MODULE_6___default.a\n      };\n    }\n  }]);\n  return Quiz;\n}();\n\n\n//# sourceURL=webpack://Quiz/./src/index.js?");

/***/ }),

/***/ "./src/langs/ru.json":
/*!***************************!*\
  !*** ./src/langs/ru.json ***!
  \***************************/
/*! exports provided: btnAdd, btnModalAdd, btnModalClose, noItems, btnDelete, confirmDelete, default */
/***/ (function(module) {

eval("module.exports = JSON.parse(\"{\\\"btnAdd\\\":\\\"Добавить\\\",\\\"btnModalAdd\\\":\\\"Добавить\\\",\\\"btnModalClose\\\":\\\"Закрыть\\\",\\\"noItems\\\":\\\"Нет квизов\\\",\\\"btnDelete\\\":\\\"Удалить\\\",\\\"confirmDelete\\\":\\\"Точно удалить?\\\"}\");\n\n//# sourceURL=webpack://Quiz/./src/langs/ru.json?");

/***/ }),

/***/ "./src/modal.js":
/*!**********************!*\
  !*** ./src/modal.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return modal; });\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils */ \"./src/utils.js\");\n/* harmony import */ var _templates__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./templates */ \"./src/templates.js\");\n/* harmony import */ var _config_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./config.json */ \"./src/config.json\");\nvar _config_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./config.json */ \"./src/config.json\", 1);\n\n\n\nfunction modal() {\n  return new Promise(function (resolve, reject) {\n    var modalLayoutHtml = Object(_templates__WEBPACK_IMPORTED_MODULE_1__[\"makeModalLayout\"])();\n    var $modalLayout = Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"makeElementFromHtml\"])(modalLayoutHtml);\n    var _$modalLayout$collect = $modalLayout.collect(),\n      $list = _$modalLayout$collect.$list,\n      $preloader = _$modalLayout$collect.$preloader;\n    var setFetchLoader = function setFetchLoader(state) {\n      $preloader.classList[state ? 'add' : 'remove']('preloader-wrapper_show');\n    };\n    var requestSuccess = function requestSuccess(data) {\n      $list.innerHTML = data.reduce(function (html, productItem) {\n        return html + Object(_templates__WEBPACK_IMPORTED_MODULE_1__[\"makeModalItem\"])(productItem);\n      }, '');\n    };\n    var clickHandle = function clickHandle(_ref) {\n      var $el = _ref.target;\n      var action = $el.dataset.action;\n      if (action == 'close') {\n        closeModal();\n      }\n      if (action == 'add') {\n        closeModal();\n        resolve($el.dataset.id);\n      }\n    };\n    function closeModal() {\n      $modalLayout.removeEventListener('click', clickHandle);\n      $modalLayout.remove();\n    }\n    $modalLayout.addEventListener('click', clickHandle);\n    setFetchLoader(true);\n    Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"fetchData\"])(_config_json__WEBPACK_IMPORTED_MODULE_2__.listEndpoint).then(requestSuccess)[\"catch\"](function (err) {\n      return console.error(\"fetch err \".concat(err));\n    })[\"finally\"](function () {\n      return setFetchLoader(false);\n    });\n    document.body.appendChild($modalLayout);\n  });\n}\n\n//# sourceURL=webpack://Quiz/./src/modal.js?");

/***/ }),

/***/ "./src/styles.scss":
/*!*************************!*\
  !*** ./src/styles.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("\nvar content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js!../node_modules/sass-loader/dist/cjs.js!./styles.scss */ \"./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/styles.scss\");\n\nif(typeof content === 'string') content = [[module.i, content, '']];\n\nvar transform;\nvar insertInto;\n\n\n\nvar options = {\"hmr\":true}\n\noptions.transform = transform\noptions.insertInto = undefined;\n\nvar update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ \"./node_modules/style-loader/lib/addStyles.js\")(content, options);\n\nif(content.locals) module.exports = content.locals;\n\nif(false) {}\n\n//# sourceURL=webpack://Quiz/./src/styles.scss?");

/***/ }),

/***/ "./src/templates.js":
/*!**************************!*\
  !*** ./src/templates.js ***!
  \**************************/
/*! exports provided: makePreloader, makeQuiz, makeLayout, makeModalLayout, makeModalItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"makePreloader\", function() { return makePreloader; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"makeQuiz\", function() { return makeQuiz; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"makeLayout\", function() { return makeLayout; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"makeModalLayout\", function() { return makeModalLayout; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"makeModalItem\", function() { return makeModalItem; });\n/* harmony import */ var _langs_ru_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./langs/ru.json */ \"./src/langs/ru.json\");\nvar _langs_ru_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./langs/ru.json */ \"./src/langs/ru.json\", 1);\n\nvar makePreloader = function makePreloader() {\n  var selector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return \"\\n        <div \".concat(selector ? \"data-selector=\\\"$\".concat(selector, \"\\\"\") : '', \" class=\\\"preloader-wrapper\\\">\\n            <div class=\\\"preloader\\\">\\n                <div class=\\\"preloader__item preloader__item_one\\\"></div>\\n                <div class=\\\"preloader__item preloader__item_two\\\"></div>\\n                <div class=\\\"preloader__item preloader__item_three\\\"></div>\\n            </div>\\n        </div>\\n    \");\n};\nvar makeQuiz = function makeQuiz(item) {\n  return \"\\n        <div>\\n            \".concat(item.title, \" <button data-action=\\\"delete\\\" data-id=\\\"\").concat(item.id, \"\\\" type=\\\"button\\\">\").concat(_langs_ru_json__WEBPACK_IMPORTED_MODULE_0__.btnDelete, \"</button>\\n        </div>\\n    \");\n};\nvar makeLayout = function makeLayout() {\n  return \"\\n        <div>\\n            <div class=\\\"toolbar cdx-block\\\">\\n                <button data-selector=\\\"$btnAdd\\\" data-action=\\\"add\\\" type=\\\"button\\\" class=\\\"cdx-button\\\">\".concat(_langs_ru_json__WEBPACK_IMPORTED_MODULE_0__.btnAdd, \"</button>\\n            </div>\\n\\n            <div data-selector=\\\"$quizPlaceholder\\\" class=\\\"quiz-placeholder\\\"></div>\\n        </div>\\n    \");\n};\nvar makeModalLayout = function makeModalLayout() {\n  return \"\\n        <div class=\\\"quiz-modal\\\">\\n            <div class=\\\"quiz-modal__overlay\\\"></div>\\n            <div class=\\\"quiz-modal__content\\\">\\n                <div class=\\\"quiz-modal__list-wrapper\\\">\\n                    <ul data-noitems=\\\"\".concat(_langs_ru_json__WEBPACK_IMPORTED_MODULE_0__.noItems, \"\\\" data-selector=\\\"$list\\\" class=\\\"quiz-modal__list custom-scroll\\\"></ul>\\n\\n                    \").concat(makePreloader('preloader'), \"\\n                </div>\\n\\n                <div class=\\\"quiz-modal__footer\\\">\\n                    <button data-action=\\\"close\\\" class=\\\"cdx-button\\\" type=\\\"button\\\">\").concat(_langs_ru_json__WEBPACK_IMPORTED_MODULE_0__.btnModalClose, \"</button>\\n                </div>\\n            </div>\\n        </div>\\n    \");\n};\nvar makeModalItem = function makeModalItem(item) {\n  return \"\\n        <li>\\n            \".concat(item.title, \"\\n            <button data-id=\\\"\").concat(item.id, \"\\\" data-action=\\\"add\\\" class=\\\"cdx-button\\\" type=\\\"button\\\">\").concat(_langs_ru_json__WEBPACK_IMPORTED_MODULE_0__.btnModalAdd, \"</button>\\n        </li>\\n    \");\n};\n\n//# sourceURL=webpack://Quiz/./src/templates.js?");

/***/ }),

/***/ "./src/utils.js":
/*!**********************!*\
  !*** ./src/utils.js ***!
  \**********************/
/*! exports provided: makeElementFromHtml, debounce, fetchData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"makeElementFromHtml\", function() { return makeElementFromHtml; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"debounce\", function() { return debounce; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"fetchData\", function() { return fetchData; });\nfunction _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }\nfunction _nonIterableSpread() { throw new TypeError(\"Invalid attempt to spread non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.\"); }\nfunction _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === \"string\") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === \"Object\" && o.constructor) n = o.constructor.name; if (n === \"Map\" || n === \"Set\") return Array.from(o); if (n === \"Arguments\" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }\nfunction _iterableToArray(iter) { if (typeof Symbol !== \"undefined\" && iter[Symbol.iterator] != null || iter[\"@@iterator\"] != null) return Array.from(iter); }\nfunction _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }\nfunction _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }\nvar makeElementFromHtml = function makeElementFromHtml(html) {\n  var $template = document.createElement('template');\n  $template.innerHTML = html;\n  var $el = $template.content.firstElementChild;\n  $el.collect = function () {\n    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},\n      _ref$attr = _ref.attr,\n      attr = _ref$attr === void 0 ? 'data-selector' : _ref$attr,\n      keepAttribute = _ref.keepAttribute,\n      _ref$to = _ref.to,\n      to = _ref$to === void 0 ? {} : _ref$to;\n    var refElements = $el.querySelectorAll(\"[\".concat(attr, \"]\"));\n    return _toConsumableArray(refElements).reduce(function (acc, $element) {\n      var propName = $element.getAttribute(attr).trim();\n      !keepAttribute && $element.removeAttribute(attr);\n      acc[propName] = acc[propName] ? Array.isArray(acc[propName]) ? [].concat(_toConsumableArray(acc[propName]), [$element]) : [acc[propName], $element] : $element;\n      return acc;\n    }, to);\n  };\n  return $el;\n};\nvar debounce = function debounce(func) {\n  var wait = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 300;\n  var immediate = arguments.length > 2 ? arguments[2] : undefined;\n  var timeout;\n  return function executedFunction() {\n    var context = this,\n      args = arguments;\n    var later = function later() {\n      timeout = null;\n      if (!immediate) func.apply(context, args);\n    };\n    var callNow = immediate && !timeout;\n    clearTimeout(timeout);\n    timeout = setTimeout(later, wait);\n    if (callNow) func.apply(context, args);\n  };\n};\nvar fetchData = function fetchData(endpoint, body) {\n  return new Promise(function (resolve, reject) {\n    var options = {\n      headers: {\n        'Content-Type': 'application/json'\n      }\n    };\n    var resMiddleware = function resMiddleware(res) {\n      var contentType = res.headers.get('content-type');\n      if (contentType && contentType.indexOf('application/json') == -1) {\n        throw new Error(\"Wrong contentType: \".concat(contentType));\n      }\n      if (res.status !== 200) {\n        throw new Error(\"Wrong statusCode: \".concat(res.status));\n      }\n      return res.json();\n    };\n    fetch(endpoint, options).then(resMiddleware).then(function (data) {\n      return resolve(data);\n    })[\"catch\"](function (err) {\n      return reject(err);\n    });\n  });\n};\n\n//# sourceURL=webpack://Quiz/./src/utils.js?");

/***/ })

/******/ })["default"];
});