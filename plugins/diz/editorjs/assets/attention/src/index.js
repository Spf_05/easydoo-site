import './styles.scss';

import lang from './langs/ru.json';
import { makeElementFromHtml } from './utils';
import icon from './icon.svg';
import { typesList } from './config';

export default class Attention {
    static get toolbox() {
        return {
            title: 'Attention',
            icon: icon
        };
    }

    constructor({data, config, api }){
        this.api = api;

        this.typesList = config.typesList || typesList;

        const TYPES = this.typesList.map( item => item.type );
        const DEFAULT_TYPE = TYPES.length > 0 ? TYPES[0] : null;

        this.data = {
            title: data.title || '',
            text: data.text || '',
            author: data.author || '',
            type: TYPES.includes(data.type) && data.type || config.defaultType || DEFAULT_TYPE,
        };
    }

    static get enableLineBreaks() {
        return true;
    }

    render(){
        const $root = makeElementFromHtml(`
            <div class="attention-layout attention-layout_type_danger">
                <div data-selector="$title" class="cdx-input attention-layout__control" contenteditable="true" data-placeholder="${ lang.titlePlaceholder }">${ this.data.title || '' }</div>
                <div data-selector="$text" class="cdx-input attention-layout__control attention-layout__control_text" contenteditable="true" data-placeholder="${ lang.textPlaceholder }">${ this.data.text || '' }</div>
                <div data-selector="$author" class="cdx-input attention-layout__control" contenteditable="true" data-placeholder="${ lang.authorPlaceholder }">${ this.data.author || '' }</div>
            </div>
        `);

        $root.collect({to:this});

        this.$root = $root;
        this._setTypeColor();

        return $root;
    }

    static get sanitize() {
        return {
            text: {
                br: true,
            },
            title: {
                br: false,
            },
            author: {
                br: false,
            },
            type: {},
        };
    }

    _setTypeColor() {
        const currentTune = this.typesList.find( el => el.type == this.data.type );
        if( currentTune === undefined ) return;
        this.$root.style = `--attention-type-color: ${ currentTune.color };`;
    }

    _highlightActiveTune() {
        const cssActiveClass = this.api.styles.settingsButtonActive;
        this.$settingsWrapper.querySelectorAll('[data-type]').forEach( $tune => $tune.classList.remove( cssActiveClass ) );
        const $currentTune = this.$settingsWrapper.querySelector(`[data-type=${ this.data.type }]`);
        if( $currentTune === undefined ) return;
        $currentTune.classList.add( cssActiveClass );
    }

    renderSettings() {
        const capitalize = str => str[0].toUpperCase() + str.substr(1);

        const $wrapper = makeElementFromHtml(`
            <div class="cdx-quote-settings">
            ${
                this.typesList.reduce(( html, tune ) => {
                        return html + `<div
                            data-type="${ tune.type }"
                            class="attention-tune ${ this.api.styles.settingsButton }"
                            title="${ capitalize( tune.title ) }"
                        >${ tune.icon }</div>`
                    }, '')
            }
            </div>
        `);

        $wrapper.onclick = ({ target : $el }) => {
            this.data.type = $el.dataset.type;
            this._highlightActiveTune();
            this._setTypeColor();
        }

        this.$settingsWrapper = $wrapper;
        this._highlightActiveTune();

        return $wrapper;
    };

    save(blockContent){
        return {
            ...this.data,
            ...{
                title: this.$title.innerHTML || '',
                text : this.$text.innerHTML || '',
                author : this.$author.innerHTML || '',
            }
        }
    }
}