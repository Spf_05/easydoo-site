export const makeElementFromHtml = html => {
    const $template = document.createElement('template');
    $template.innerHTML = html;
    const $el = $template.content.firstElementChild;

    $el.collect = ({ attr = 'data-selector', keepAttribute, to = {} } = {}) => {
        const refElements = $el.querySelectorAll(`[${attr}]`);
        return [...refElements].reduce((acc, $element) => {
          const propName = $element.getAttribute(attr).trim();
          !keepAttribute && $element.removeAttribute(attr);
          acc[propName] = acc[propName]
            ? Array.isArray(acc[propName])
              ? [...acc[propName], $element]
              : [acc[propName], $element]
            : $element;
          return acc;
        }, to);
      };

    return $el;
}