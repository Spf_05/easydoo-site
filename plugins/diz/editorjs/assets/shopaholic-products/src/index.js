import './styles.scss';
import modal from './modal.js';
import { makeElementFromHtml, fetchData } from './utils';
import { makeLayout, makeItem } from './templates';
import lang from './langs/ru.json';
import config from './config.json';

export default class ShopaholicProducts {
    static get toolbox() {
        return {
            title: 'Products',
            icon: '<svg width="17" height="17" viewBox="0 0 52 52" xmlns="http://www.w3.org/2000/svg"><path d="m6.778 2.238 39.614.008-.796-1.92c-1.51 1.695-2.907 3.298-2.848 5.55.008.317.684.64.794.919 1.26 3.229 1.557 3.395 1.61 6.09.056 2.89.071 5.784.107 8.676l.214 17.352c.035 2.817.885 6.935-1.051 9.006-2.018 2.158-6.067 1.315-8.988 1.329l-16.662.076c-2.834.013-6 .391-8.821.04-4.154-.516-3.473-2.758-3.517-6.426L6.222 25.16c-.06-5.101-2.166-16.638 3.269-19.232.697-.332.664-1.265.228-1.766C8.466 2.724 7.269 1.707 5.694.675L5.126 2.77l1.15-.142c1.418-.175 1.435-2.427 0-2.25l-1.15.142c-1.08.133-1.565 1.443-.568 2.096a11.612 11.612 0 0 1 3.57 3.135l.227-1.767C3.484 6.31 3.783 9.258 3.843 14.295L4.125 38l.07 5.926c.018 1.419-.396 4.594.071 5.927.283.805.278 1.137.987 1.624 1.635 1.124 6.574.129 8.618.12l11.762-.054 11.761-.054c2.418-.011 6.642.926 8.856-.204 3.156-1.61 1.506-9.73 1.473-12.372-.09-7.39-.182-14.782-.274-22.173-.033-2.698.292-5.746-.453-8.389-.906-3.216-2.802-3.075.191-6.435.633-.71.22-1.92-.795-1.92L6.778-.012c-1.447 0-1.45 2.25 0 2.25z"/><path class="st0" d="M4.903 8.866h39.5c1.448 0 1.45-2.25 0-2.25h-39.5c-1.447 0-1.45 2.25 0 2.25z"/><path class="st0" d="M31.861 8.62c.274 2.612.43 5.255.243 7.88-.166 2.316-.885 3.679-2.81 5.117-1.306.977-3.16 1.98-4.851 1.52-2.719-.737-4.518-4.656-4.718-7.135-.199-2.46.257-4.914.032-7.382-.13-1.43-2.381-1.444-2.25 0 .344 3.78-.795 7.797.954 11.35 1.251 2.542 3.594 5.431 6.672 5.532 2.744.09 6.38-2.008 7.973-4.18 2.363-3.222 1.387-9.056 1.005-12.702-.149-1.426-2.4-1.441-2.25 0z"/></svg>'
        };
    }

    constructor({data}){
        this.data = data;
        this.products = [];
        this.fetchProducts();
    }

    setFetchLoader( state ) {

    }

    fetchProducts() {
        if ( this.data.products && this.data.products.length ) {
            this.setFetchLoader( true );

            fetchData( config.getEndpoint, this.data.products )
                .then( data => {
                    this.products = data;
                    this.renderItems();
                })
                .catch( err => console.error( `fetch err ${ err }` ) )
                .finally( () => this.setFetchLoader( false ) );
        }
    }

    renderItems() {
        if( this.products.length == 0 ) {
            this.$list.innerHTML = `<div>${ lang.noItems }</div>`;
        } else {
            this.$list.innerHTML = this.products.reduce( ( html, item ) => html + makeItem(item), '' );
        }
    }

    clickHandler = (e) => {
        const $el = e.target;
        const action = $el.dataset.action;

        if( action == 'add' ) {
            modal()
                .then( data => {
                    this.products = [...this.products, ...data];
                    this.renderItems();
                })

            return;
        }

        if( action == 'delete' ) {
            if( !confirm( lang.confirmDelete ) ) return;
            this.products = this.products.filter( item => item.id != $el.dataset.id );
            this.renderItems();
            return;
        }
    }

    renderLayout() {
        const $root = document.createElement('div');

        $root.addEventListener( 'click', this.clickHandler );

        const layoutHTML = makeLayout();
        const $layout = makeElementFromHtml( layoutHTML );
        $layout.collect({ to : this });

        this.renderItems();

        $root.insertAdjacentElement( 'afterbegin', $layout );
        return $root;
    }

    render(){
        return this.renderLayout();
    }

    save(blockContent){
        console.log( {
            products: this.products.map( item => item.id ),
        } );
        return {
            products: this.products.map( item => item.id ),
        }
    }
}