export const makeElementFromHtml = html => {
    const $template = document.createElement('template');
    $template.innerHTML = html;
    const $el = $template.content.firstElementChild;

    $el.collect = ({ attr = 'data-selector', keepAttribute, to = {} } = {}) => {
        const refElements = $el.querySelectorAll(`[${attr}]`);
        return [...refElements].reduce((acc, $element) => {
          const propName = $element.getAttribute(attr).trim();
          !keepAttribute && $element.removeAttribute(attr);
          acc[propName] = acc[propName]
            ? Array.isArray(acc[propName])
              ? [...acc[propName], $element]
              : [acc[propName], $element]
            : $element;
          return acc;
        }, to);
      };

    return $el;
}

export const debounce = (func, wait = 300, immediate) => {
	let timeout;

	return function executedFunction() {
		const context = this, args = arguments;
		const later = () => {
			timeout = null;
			if ( !immediate ) func.apply( context, args );
		};
		const callNow = immediate && !timeout;
		clearTimeout( timeout );
		timeout = setTimeout( later, wait );
		if (callNow) func.apply(context, args);
	};
};

export const fetchData = ( endpoint, body ) => {
    return new Promise( (resolve, reject ) => {
        const options = {
            method: 'POST',
            body : JSON.stringify(body ),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const resMiddleware = res => {
            const contentType = res.headers.get( 'content-type' );

            if ( contentType && contentType.indexOf( 'application/json' ) == -1) {
                throw new Error(`Wrong contentType: ${ contentType }`);
            }

            if( res.status !== 200 ) {
                throw new Error(`Wrong statusCode: ${ res.status }`);
            }

            return res.json();
        }

        fetch(endpoint, options )
            .then( resMiddleware )
            .then( data => resolve( data ) )
            .catch( err => reject(err) );
    })
}