import { makeElementFromHtml, fetchData, debounce } from './utils';
import { makeModalLayout, makeModalItem } from './templates';
import config from './config.json';

export default function modal() {
    return new Promise( (resolve, reject) => {
        const modalLayoutHtml = makeModalLayout();
        const $modalLayout = makeElementFromHtml( modalLayoutHtml );
        let productItems = [];

        const { $list, $preloader, $btnAdd } = $modalLayout.collect();

        const setFetchLoader = state => {
            $preloader.classList[ state ? 'add' : 'remove' ]('preloader-wrapper_show');
        }

        function setProductItems( data ) {
            productItems = data.map( item => ({ ...item, selected : false }) );
            updateAddButtonDisabled();
        }

        const requestSuccess = data => {
            setProductItems( data );
            $list.innerHTML = productItems.reduce( ( html, productItem ) => html + makeModalItem( productItem ), '' );
        }

        function updateAddButtonDisabled() {
            $btnAdd.disabled = productItems
                                    .filter( item => item.selected === true )
                                    .length == 0;
        }

        const inputHandle = debounce(e => {
            const query = e.target.value;

            if( query == '' ) {
                $list.innerHTML = '';
                setProductItems([]);
                return;
            }

            if( query.length <= 3 ) {
                return;
            }

            setFetchLoader( true );

            fetchData( config.queryEndpoint, { query } )
                .then( requestSuccess )
                .catch( err => console.error( `fetch err ${ err }` ) )
                .finally( () => setFetchLoader( false ) );
        });

        const clickHandle = e => {
            const $el = e.target;
            const action = $el.dataset.action;

            if( action == 'close' ) {
                closeModal();
            }

            if( action == 'add' ) {
                closeModal();
                resolve( productItems.filter( item => item.selected === true ) );
            }
        }

        const changeHandle = e => {
            const $el = e.target;
            if( $el.dataset.action != 'select' ) return;
            const item = productItems.find( item => item.id == $el.value );
            if( item === undefined ) return;

            item.selected = $el.checked;
            updateAddButtonDisabled();
        }

        function closeModal() {
            $modalLayout.removeEventListener('change', changeHandle );
            $modalLayout.removeEventListener('input', inputHandle );
            $modalLayout.removeEventListener('click', clickHandle );
            $modalLayout.remove();
        }

        $modalLayout.addEventListener('change', changeHandle );
        $modalLayout.addEventListener('input', inputHandle );
        $modalLayout.addEventListener('click', clickHandle );

        document.body.appendChild( $modalLayout );
    })
}