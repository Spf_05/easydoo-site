import lang from './langs/ru.json';

export const makePreloader = ( selector = null ) => {
    return `
        <div ${ selector ? `data-selector="$${ selector }"` : '' } class="preloader-wrapper">
            <div class="preloader">
                <div class="preloader__item preloader__item_one"></div>
                <div class="preloader__item preloader__item_two"></div>
                <div class="preloader__item preloader__item_three"></div>
            </div>
        </div>
    `
}

export const makeItem = item => {
    return `
        <div>
            ${ item.title } <button data-action="delete" data-id="${ item.id }" type="button">${ lang.btnDelete }</button>
        </div>
    `
}

export const makeLayout = () => {
    return `
        <div>
            <div class="toolbar cdx-block">
                <button data-action="add" type="button" class="cdx-button">${ lang.btnAdd }</button>
            </div>
        
            <div data-selector="$list" class="list"></div>
        </div>
    `
}

export const makeModalLayout = () => {
    return `
        <div class="shopaholic-products-modal">
            <div class="shopaholic-products-modal__overlay"></div>
            <div class="shopaholic-products-modal__content">
                <div>
                    <input class="cdx-input" type="text" name="query" placeholder="${ lang.inputQueryPlaceholder }">
                </div>

                <div class="shopaholic-products-modal__list-wrapper">
                    <ul data-noitems="${ lang.noItems }" data-selector="$list" class="shopaholic-products-modal__list custom-scroll"></ul>

                    ${ makePreloader('preloader') }
                </div>

                <div class="shopaholic-products-modal__footer">
                    <button disabled data-selector="$btnAdd" data-action="add" class="cdx-button" type="button">${ lang.btnModalAdd }</button>
                    <button data-action="close" class="cdx-button" type="button">${ lang.btnModalClose }</button>
                </div>
            </div>
        </div>
    `
}

export const makeModalItem = item => {
    return `
        <li>
            <input data-action="select" type="checkbox" name="selected[]" value="${ item.id }">
            ${ item.title }
        </li>
    `
}