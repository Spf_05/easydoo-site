<?php
Route::group(['prefix' => 'api'], function () {
    Route::group([ 'middleware' => ['web'] ], function () {
        Route::any('image/{type}', \Diz\EditorJs\Classes\Image\Plugin::class);
    });
});

function getTemplatePartialsOptions() {
	$arr		= [];
	$theme		= \Cms\Classes\Theme::getEditTheme();
	$list		= \Cms\Classes\Partial::listInTheme($theme, true);
	$folderName	= 'editorjs';
	foreach ( $list as $item ) {
		$fileName	= $item->getBaseFileName();

		if ( substr( $fileName, 0, strlen( $folderName ) ) == $folderName ) {
			$blockName = substr( $fileName, strlen( $folderName ) + 1, strlen( $fileName ) );
			$arr[ $blockName ] = $fileName;
		}
	}
	return $arr;
}

function renderFromPartial( $partial, $data ) {
	try {
		$controller	= new \Cms\Classes\Controller;
		return	$controller->renderPartial( $partial, $data );
	} catch (\Exception $e) {
		trace_log( $e );
		return false;
	};
}

Route::post('/diz-editor/get-template', function () {
	if ( !BackendAuth::getUser() ) {
		return response('Forbidden', 401);
	}

	$data = input();
	$blockType = $data[ 'type' ];
	$blocksPartials = getTemplatePartialsOptions();

	if( array_key_exists( $blockType, $blocksPartials ) ) {
		return renderFromPartial( $blocksPartials[ $blockType ], [ 'block' => $data ] );
	} else {
		return 'Шаблон блока не найден :(';
	}
})->middleware('web');

Route::get('/api/get-quiz-list', function () {
    return \Ed\Quize\Models\Quize::get();
});

Route::get('/api/get-quiz/{id}', function ($id) {
    return \Ed\Quize\Models\Quize::find($id);
});

Route::post('/api/search-products', function () {
	$query = post('query', '123');
    $ret = [];

    if( $query == '123' ) {
        $ret[] = [ 'id' => 1, 'title' => 'Item 1', 'category' => 'cat1', 'image' => 'img1' ];
    } else {
        $ret[] = [ 'id' => 1, 'title' => 'Item 1', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 2, 'title' => 'Item 2', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 3, 'title' => 'Item 3', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 4, 'title' => 'Item 4', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 5, 'title' => 'Item 5', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 6, 'title' => 'Item 6', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 7, 'title' => 'Item 7', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 8, 'title' => 'Item 8', 'category' => 'cat1', 'image' => 'img1' ];
        $ret[] = [ 'id' => 9, 'title' => 'Item 9', 'category' => 'cat1', 'image' => 'img1' ];
    }

    return $ret;
})->middleware('web');

Route::post('/api/get-products', function () {
    $ret[] = [ 'id' => 9, 'title' => 'Item 9', 'category' => 'cat1', 'image' => 'img1' ];

    return $ret;
})->middleware('web');