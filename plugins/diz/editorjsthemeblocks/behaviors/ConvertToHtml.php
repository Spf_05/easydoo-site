<?php namespace Diz\EditorJSThemeBlocks\Behaviors;

/**
 * Class ConvertableBehavior
 * @package ReaZzon\Editor\Classes\Behaviors
 * @author Nick Khaetsky, nick@reazzon.ru
 */
class ConvertToHtml extends ConvertableBehavior
{
    /**
     * @var array Settings for validation
     */
    protected $validationSettings;

    /**
     * @var array Views for blocks
     */
    protected $blocksViews;

    /**
     * Converts json string to blocks
     * @param string $jsonField
     * @return string
     */
    public function convertJsonToHtml($jsonField)
    {
		$this->getTemplatePartialsOptions();
        return $this->startConverter($jsonField);
    }

	public static function getTemplatePartialsOptions() {
		$arr		= [];
		$theme		= \Cms\Classes\Theme::getEditTheme();
		$list		= \Cms\Classes\Partial::listInTheme($theme, true);
		$folderName	= 'editorjs';
		foreach ( $list as $item ) {
			$fileName	= $item->getBaseFileName();

			if ( substr( $fileName, 0, strlen( $folderName ) ) == $folderName ) {
				$blockName = substr( $fileName, strlen( $folderName ) + 1, strlen( $fileName ) );
				$arr[ $blockName ] = $fileName;
			}
		}
		return $arr;
	}

	public static function renderFromPartial( $partial, $data ) {
		try {
			$controller	= new \Cms\Classes\Controller;
			return	$controller->renderPartial( $partial, $data );
		} catch (\Exception $e) {
			trace_log( $e );
			return false;
		};
	}

    /**
     * @param \EditorJS\EditorJS $editor
     * @param array $blocks
     * @return mixed|string
     */
    public function processBlocks($editor, $blocks)
    {
		$blocksPartials = $this->getTemplatePartialsOptions();
		$htmlOutput = '';
        foreach ($blocks as $block) {
			$blockType = strtolower( $block['type'] );
			if( array_key_exists( $blockType, $blocksPartials ) ) {
				$htmlOutput .= $this->renderFromPartial( $blocksPartials[ $blockType ], [ 'block' => $block ] );
			} else {
				// trace_log( 'Diz.EditorJSThemeBlocks: Partial блока "' . $block['type'] . '" не найден' );
			}
        }

        return html_entity_decode($htmlOutput);
    }
}