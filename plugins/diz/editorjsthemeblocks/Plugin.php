<?php namespace Diz\EditorJSThemeBlocks;

use Backend, Event;
use System\Classes\PluginBase;
use Diz\EditorJSThemeBlocks\Classes\Event\ExtendRainLabStaticPages;
use Diz\EditorJSThemeBlocks\Classes\Xx;

/**
 * EditorJSThemeBlocks Plugin Information File
 */
class Plugin extends PluginBase
{
	public $require = ['ReaZzon.Editor'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'EditorJSThemeBlocks',
            'description' => 'No description provided yet...',
            'author'      => 'Diz',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        parent::boot();
        Event::subscribe(ExtendRainLabStaticPages::class);
        Event::subscribe(Xx::class);
    }

    public function register()
    {
        $this->registerConsoleCommand( 'diz.editorjs-staticpage-update', 'Diz\EditorJSThemeBlocks\Console\StaticPageUpdate' );
    }
}
