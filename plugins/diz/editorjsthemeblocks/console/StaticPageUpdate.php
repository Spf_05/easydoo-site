<?php namespace Diz\EditorJSThemeBlocks\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Diz\EditorJSThemeBlocks\Classes\Helpers\StaticPages;

class StaticPageUpdate extends Command {
    protected $name = 'diz:editorjs-staticpage-update';
    protected $description = 'Update EditorJS StaticPage blocks';

    public function handle() {
        StaticPages::updatateBlocks();
        $this->output->writeln('Ready!');
    }
}