<?php namespace Diz\EditorJSThemeBlocks\Classes\Helpers;

use Rainlab\Pages\Classes\Page;
use \Cms\Classes\Theme;

class StaticPages {
	public static function updatateBlocks() {
		$pages = Page::sortBy('title')->lists('title', 'baseFileName');

		foreach( $pages as $fileName => $fileTitle ) {
			$page = Page::loadCached( Theme::getActiveTheme(), $fileName );
			$page->save();
		}
	}
}