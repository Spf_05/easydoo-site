<?php namespace Diz\EditorJSThemeBlocks\Classes\Event;

use RainLab\Translate\Classes\MLStaticPage;
use ReaZzon\Editor\Models\Settings;
use System\Classes\PluginManager;

/**
 * Class ExtendRainLabStaticPages
 * @package ReaZzon\Editor\Classes\Event
 * @author Nick Khaetsky, nick@reazzon.ru
 */
class ExtendRainLabStaticPages
{
    /**
     * Add listeners
     * @param \Illuminate\Events\Dispatcher $event
     */
    public function subscribe($event)
    {
        if (Settings::get('integration_static_pages', false) &&
            PluginManager::instance()->hasPlugin('RainLab.Pages')) {

            \RainLab\Pages\Classes\Page::extend(function ($model) {
                $model->implement[] = 'Diz.EditorJSThemeBlocks.Behaviors.ConvertToHtml';

				if (($key = array_search( 'ReaZzon.Editor.Behaviors.ConvertToHtml' , $model->implement)) !== false) {
					unset($model->implement[$key]);
				}
            });

            // if (PluginManager::instance()->hasPlugin('RainLab.Translate')
            //     && !PluginManager::instance()->isDisabled('RainLab.Translate')) {

            //     MLStaticPage::extend(function (MLStaticPage $model) {
            //         /** @var \October\Rain\Database\Model $model */
            //         $model->implement[] = 'ReaZzon.Editor.Behaviors.ConvertToHtml';

            //         $model->bindEvent('model.beforeSave', function () use ($model) {
            //             if (isset($model->viewBag['editor']) && !empty($model->viewBag['editor'])) {
            //                 $model->markup = $model->convertJsonToHtml($model->viewBag['editor']);
            //             }
            //         });
            //     });
            // }
        }
    }
}