<?php namespace Diz\ThemeBlocks;

use Backend;
use System\Classes\PluginBase;
use Diz\ThemeBlocks\Classes\Events\ExtendThemeForm;
use Diz\ThemeBlocks\Classes\Twig\Filters;
use Event;

class Plugin extends PluginBase {
    public function pluginDetails() {
        return [
            'name'        => 'ThemeBlocks',
            'description' => 'No description provided yet...',
            'author'      => 'Diz',
            'icon'        => 'icon-leaf'
        ];
    }

    public function boot() {
        Event::subscribe(ExtendThemeForm::class);
    }

    public function registerMarkupTags() {
        return [
            'functions' => [
                'get_theme_block' => [ Filters::class, 'getThemeBlock' ],
            ]
        ];
    }
}
