<?php return [
	'form' => [
		'fields' => [
			'is_published'		=> 'Published',
			'meta_title'		=> 'Meta title',
			'meta_description'	=> 'Meta description',
			'meta_keywords'		=> 'Meta keywords',
			'slug'				=> 'Slug',
			'description'		=> 'Description',
			'anons'				=> 'Anons',
			'cover'				=> 'Cover',
			'documents'			=> 'Documents',
			'files'				=> 'Files',
			'price'				=> 'Price',
			'text'				=> 'Text',
			'category'			=> 'Category',
			'tags'				=> 'Tags',
			'title'				=> 'Title',
			'date'				=> 'Date',
			'empty'				=> '- Empty -',
			'color'				=> 'Color',
			'icon'				=> 'Icon',
			'avatar'			=> 'Avatar',
			'name'				=> 'Name',
		],
		'tabs' => [
			'meta'	=> 'Meta',
			'card'	=> 'Карточка',
		]
	],
	'columns' => [
		'fields' => [
			'is_published'		=> 'Publ.',
			'sort_order'		=> 'Sort.',
			'slug'				=> 'Slug',
			'title'				=> 'Title',
			'date'				=> 'Date',
			'category'			=> 'Category',
			'tags_count'		=> 'Tags count',
			'color'				=> 'Color',
		]
	]
];