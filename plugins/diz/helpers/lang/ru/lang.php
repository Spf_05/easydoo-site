<?php return [
	'form' => [
		'fields' => [
			'is_published'		=> 'Опубликовать',
			'meta_title'		=> 'Meta title',
			'meta_description'	=> 'Meta description',
			'meta_keywords'		=> 'Meta keywords',
			'slug'				=> 'Слаг',
			'description'		=> 'Описание',
			'anons'				=> 'Анонс',
			'cover'				=> 'Обложка',
			'documents'			=> 'Документы',
			'files'				=> 'Файлы',
			'price'				=> 'Цена',
			'text'				=> 'Текст',
			'category'			=> 'Категория',
			'tags'				=> 'Теги',
			'title'				=> 'Заголовок',
			'date'				=> 'Дата',
			'empty'				=> '- Не выбрано -',
			'color'				=> 'Цвет',
			'icon'				=> 'Иконка',
			'avatar'			=> 'Аватар',
			'name'				=> 'Имя',
		],
		'tabs' => [
			'meta'	=> 'Meta',
			'card'	=> 'Сard',
		]
	],
	'columns' => [
		'fields' => [
			'is_published'		=> 'Опубл.',
			'sort_order'		=> 'Сорт.',
			'slug'				=> 'Слаг',
			'title'				=> 'Заголовок',
			'date'				=> 'Дата',
			'category'			=> 'Категория',
			'tags_count'		=> 'Кол-во тегов',
			'color'				=> 'Цвет',
		]
	]
];