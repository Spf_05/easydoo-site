<?php namespace Diz\Helpers;

use Backend;
use System\Classes\PluginBase;

/**
 * Helpers Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Helpers',
            'description' => 'No description provided yet...',
            'author'      => 'Diz',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Diz\Helpers\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'diz.helpers.some_permission' => [
                'tab' => 'Helpers',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'helpers' => [
                'label'       => 'Helpers',
                'url'         => Backend::url('diz/helpers/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['diz.helpers.*'],
                'order'       => 500,
            ],
        ];
    }
}
